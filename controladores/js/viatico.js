function registrarSolicitud(){

	var lugardes       = $("#lugardes").val();
	var fedesde        = $.trim($("#fedesde").val());
	var fehasta        = $("#fehasta").val();
	var motivo         = $("#motivo").val();
	var otro           = $("#otro").val();
	var otrovu         = $("#otrovu").val();
	var hidempleado    = $("#hidempleado").val();
	var ciu_des        = $("#ciu_des").val();
	var ciu_ori        = $("#ciu_ori").val();

	var trans=3;
	if($("#transa").is(':checked')){
    	trans="1";
    }else if($("#transt").is(':checked')){
    	trans="2";
    }

    var transvu=3;
	if($("#transavu").is(':checked')){
    	transvu="1";
    }else if($("#transtvu").is(':checked')){
    	transvu="2";
    }

    //validarFechasDesHas(fedes,fehas);

	if ($.trim(lugardes) == '' || $.trim(fedesde) == '' || $.trim(fehasta) == '' || ciu_des == 0 || ciu_ori == 0 || 
			$.trim(motivo) == '' || $.trim(hidempleado) == '' || (trans == 3 && $.trim(otro)=='')  || (transvu == 3 && $.trim(otrovu)=='')) {
			alert("Existen Campos vacios o tienen espacios");
			return;
	}
	$.ajax( {
		type : 'POST',
		url : "../controladores/trans/tViatico.php",
		data :	{ accion: "registrarSolicitud", lugardes: lugardes, fedesde:fedesde, fehasta:fehasta, motivo:motivo,
				 otro:otro,otrovu:otrovu, hidempleado:hidempleado,trans:trans,transvu:transvu, ciu_des:ciu_des,ciu_ori:ciu_ori},
		error : function(xhr, ajaxOptions, thrownError) {
			alert("Ups... Algo esta mal :(");
		},
		beforeSend: function(data){
           // $("#prueba").html("<p align='center'><img src='img/Preloader_10.gif'></img></p>");
        },
		success : function(data) {
			var data = eval("(" + data + ")");
			if(data){
				alert("Solicitud de viatico registrada exitosamente");
				//mostrarPlanilla(data);
				limpiarFormSolicitud();
				$('#tabs_soli a:first').tab('show');
				mostrarPlanilla1(data);
				enviarCorreo(data,0);
			}else{
				alert("ERROR, No se pudo registrar la solicitud de viáticos");
			}
		}
	});
}

function limpiarFormSolicitud(){
     $("#lugardes").val('');
	 $("#fedesde").val('');
	 $("#fehasta").val('');
	 $("#motivo").val('');
	 $("#otro").val('');
	 $("#otrovu").val('');
	 $("#hidempleado").val('');
	 $("#ciu_des").val('');
	 $("#ciu_ori").val('');
	 $("#ced_em").val('');
	 $("#nom_em").val('');
	 $("#ape_em").val('');
	 $("#cargo_em").val('');
	 $("#esta").val('');
	 $("#ciu").val('');
	 $("#direc").val('');
	 $("#esta_ori").val('');
	 $("#esta_des").val('');

}

function validarFechasDesHas(fedes,fehas){

	fedes.split('-');
	fehas.split('-');

	if (!fehas[0] >= fedes[0] || !fehas[1] >= fedes[1] || !fehas[2] >= fedes[2]) {
		alert("ERROR, fecha invalida, la fecha hasta debe ser mayor a la fecha desde");
		return;
	}

}

function listarSolicitudes(){
	$.ajax( {
		type : 'POST',
		url : "../controladores/trans/tViatico.php",
		data :	{ accion: "listarSolicitudes"},
		error : function(xhr, ajaxOptions, thrownError) {
			alert("Ups... Algo esta mal :(");
		},
		success : function(data) {
			var data = eval("(" + data + ")");
			if(data){
				$('#tbl-sol').html("");
				var fila="<tr>"+
								"<td><strong>#</strong></td>"+
								"<td><strong>Lugar D.</strong></td>"+
								"<td><strong>Fecha Desde</strong></td>"+
								"<td><strong>Fecha Hasta</strong></td>"+
								"<td><strong>Motivo</strong></td>"+
								"<td><strong>T. Transporte</strong></td>"+
								"<td><strong>Estado S.</strong></td>"+
							"</tr>";
				$('#tbl-sol').append(fila);

				for(var i=0; i<data.length; i++){

					var fila="<tr id='user_"+data[i]['id_soli']+"'>"+
								"<td>"+(i+1)+"</td>"+
								"<td>"+data[i]['lugardes_soli']+"</td>"+
								"<td>"+data[i]['fechades_soli']+"</td>"+
								"<td>"+data[i]['fechahas_soli']+"</td>"+
								"<td>"+data[i]['motivovia_soli']+"</td>"+
								"<td>"+data[i]['tipotrans_soli']+"</td>"+
								"<td>"+data[i]['estado_soli']+"</td>"+
							"</tr>";
					$('#tbl-sol').append(fila);

					//$('#user_'+data[i]['id_usu']).click({usuario:data[i]['usuario']},function(evento){
						//buscarUsuario(evento.data.usuario);
						//$("#myModal1").modal('hide');
					//});
				}
			}
		}
	});
}

function validarPrecioViatico(){

	var via     = $("#via").val();
	var fecha   = $("#fecha").val();
	var monto   = $("#monto").val();
	var montomax   = $("#montomax").val();

	if ($.trim(via) == 0 || $.trim(fecha) == '' || $.trim(monto) == '' || $.trim(montomax) == '') {
		alert("ERROR, Existen campos vacios");
		return;
	}
	if (parseFloat($.trim(monto)) == 0 || parseFloat($.trim(montomax)) == 0) {
		alert("ERROR, los montos del viátco (Máx/Mín) deben ser mayores a 0");
		return;
	}

	if (parseFloat($.trim(monto)) > parseFloat($.trim(montomax))) {
		alert("El monto mínimo no puede superar al monto máximo");
		return;
	}

	$.ajax( {
		type : 'POST',
		url : "../controladores/trans/tViatico.php",
		data :	{ accion: "validarPrecioViatico", via:via,fecha:fecha},
		error : function(xhr, ajaxOptions, thrownError) {
			alert("Ups... Algo esta mal :(");
		},
		beforeSend: function(data){
           // $("#prueba").html("<p align='center'><img src='img/Preloader_10.gif'></img></p>");
        },
		success : function(data) {
			var data = eval("(" + data + ")");
			if(data == 0){
				registrarPrecios();
			}
			else if(data == 1){
				alert("ERROR, ya existe un precio para ese viático en la fecha indicada");
			}
			else if(data == 2){
				alert("ERROR, la fecha introducida debe ser superior a la de hoy");
			}
			else{
				alert("ERROR, no se puede registrar este precio de viático para la fecha indicada, \n puesto que ya existen solicitudes generadas posteriores a esa fecha");
			}
		}
	});
}

function registrarPrecios(){

	var via     = $("#via").val();
	var fecha   = $("#fecha").val();
	var monto   = $("#monto").val();
	var montomax   = $("#montomax").val();

		if (confirm("Esta seguro de registrar este precio al viático?")) {
			$.ajax( {
				type : 'POST',
				url : "../controladores/trans/tViatico.php",
				data :	{ accion: "registrarPrecios", via:via,fecha:fecha,monto:monto,montomax:montomax},
				error : function(xhr, ajaxOptions, thrownError) {
					alert("Ups... Algo esta mal :(");
				},
				beforeSend: function(data){
		           // $("#prueba").html("<p align='center'><img src='img/Preloader_10.gif'></img></p>");
		        },
				success : function(data) {
					var data = eval("(" + data + ")");
					if(data){
						alert("Registro del precio de viático realizado exitosamente");
						location.reload();
					}else{
						alert("ERROR, No se pudo registrar el precio del viático");
					}
				}
			});
		}
}

function cargarViatico(){
	$.ajax( {
		type : 'POST',
		url : "../controladores/trans/tViatico.php",
		data :	{ accion: "cargarViatico"},
		error : function(xhr, ajaxOptions, thrownError) {
			alert("Ups... Algo esta mal :(");
		},
		beforeSend: function(data){
           // $("#prueba").html("<p align='center'><img src='img/Preloader_10.gif'></img></p>");
        },
		success : function(data) {
			var data = eval("(" + data + ")");
			if(data){
				$('#via').html("<option value='0'>Seleccione</option>");
				for(var i=0; i<data.length; i++){
					var option="<option value='"+data[i]['id_via']+"'>"+data[i]['nombre_via']+"</option>";
					$('#via').append(option);
				}
			}else{
				alert("ERROR, No se pudieron cargar los viáticos");
			}
		}
	});
}

function buscarSolicitud(nSoli){
	//var nSoli = $("#nSoli").val();

	if ($.trim(nSoli) == '') {
		alert('ERROR, Debe introducir un numero de solicitud');
		return;
	}

	$.ajax( {
		type : 'POST',
		url : "../controladores/trans/tViatico.php",
		data :	{ accion: "buscarSolicitud", nSoli:nSoli},
		error : function(xhr, ajaxOptions, thrownError) {
			alert("Ups... Algo esta mal :(");
		},
		beforeSend: function(data){
           // $("#prueba").html("<p align='center'><img src='img/Preloader_10.gif'></img></p>");
        },
		success : function(data) {
			var data = eval("(" + data + ")");
			if(data){
				if (data=="E001"){
					alert("La solicitud especificada no esta aprobada");
				}else if (data=="E002"){
					alert("La solicitud especificada ya fue generada pero no aprobada");
				}else if (data=="E003"){
					alert("La solicitud especificada ya fue generada y aprobada");
				}else if (data=="E004"){
					alert("La solicitud especificada ya fue generada, aprobada y pagada");
				}
				else{
					$("#idHidden").val(data[0]['id_soli']);
					fecha_desde=data[0]['fechades_soli'];

					$("#ced_em").val(data[0]['cedula_per']);
					$("#nom_em").val(data[0]['nombre_per']);
					$("#ape_em").val(data[0]['apellido_per']);
					$("#cargo_em").val(data[0]['cargo']);
					$("#esta").val(data[0]['nombre_es']);
					$("#ciu").val(data[0]['nombre_ciu']);
					$("#direc").val(data[0]['direccion_per']);
					$("#fedesde").val(data[0]['fechades_soli']);
					$("#fehasta").val(data[0]['fechahas_soli']);
					$("#lugardes").val(data[0]['lugardes_soli']);
					$("#motivo").val(data[0]['motivovia_soli']);
					$("#otro").val(data[0]['otro']);

					$("#esta_ori").val(data[0]['essoliori']);
					$("#esta_des").val(data[0]['essolides']);
					traerCiudades(data[0]['essoliori'], 'ciu_ori', data[0]['id_ciu_ori']);
					traerCiudades(data[0]['essolides'], 'ciu_des', data[0]['id_ciu_des']);

					//inputs tipo radio
					if(data[0]['sexo_per']=="M"){
					    	$("#sexoM").attr('checked',true);
					    }else{
							$("#sexoF").attr('checked',true);
					    }

					//inputs tipo radio
					if(data[0]['tipotrans_soli']=="1"){
					    	$("#transa").attr('checked',true);
					}
					    else if(data[0]['tipotrans_soli']=="2"){
							$("#transt").attr('checked',true);
					    }
					    else{
					    	$("#transo").attr('checked',true);
					    }
					//se debe llenar los campos de la solicitud y del empleado --- pendiente con la fecha
					//
					//ahora buscams los precios con esa fecha 'desde'
					buscarPrecios(fecha_desde);
				}
			}else{
				alert("ERROR, No se pudo conseguir la solicitud");
			}
		}
	});
}

function generarInputsPrecios(){
	$.ajax( {
		type : 'POST',
		url : "../controladores/trans/tViatico.php",
		data :	{ accion: "cargarViatico"},
		error : function(xhr, ajaxOptions, thrownError) {
			alert("Ups... Algo esta mal :( ");
		},
		beforeSend: function(data){
           // $("#prueba").html("<p align='center'><img src='img/Preloader_10.gif'></img></p>");
        },
		success : function(data) {
			var data = eval("(" + data + ")");
			if(data){
				var html="";

				for(var i=0; i<data.length; i++){

					var id  = data[i]['id_via'];
					var nom = data[i]['nombre_via'];
					html+='<hr>'+
	        	    '<div class="form-group">'+nom+'&nbsp;<input type="text" readonly="readonly" class="form-control" id="preciovia_'+id+'"></div>'+
	        	    '<div class="form-group">Cantidad&nbsp;<input type="text" onkeyup="calcularPrecioViatico(this.id);" onkeypress="return soloNum(event);" id="cantvia_'+id+'" class="form-control"></div>'+
	        	    '<div class="form-group">Total&nbsp;<input type="text" readonly class="form-control" id="totalvia_'+id+'"></div>'+
	        	    '<div class="col-lg-12">&nbsp;</div>';
	        	}
	        	$("#form_precios_via").html(html);
			}else{
				alert("ERROR, No se pudo conseguir la solicitud");
			}
		}
	});
}

function buscarPrecios(fecha){
	if ($.trim(fecha) == '') {
		alert('ERROR, No existe una fecha');
		return;
	}
	$.ajax( {
		type : 'POST',
		url : "../controladores/trans/tViatico.php",
		data :	{ accion: "buscarPrecios", fecha:fecha},
		error : function(xhr, ajaxOptions, thrownError) {
			alert("Ups... Algo esta mal :(");
		},
		beforeSend: function(data){
           // $("#prueba").html("<p align='center'><img src='img/Preloader_10.gif'></img></p>");
        },
		success : function(data) {
			var data = eval("(" + data + ")");
			if(data){
				for(var i=0; i<data.length; i++){
					if (data[i]['id_via'] == 1 || data[i]['id_via'] == 5 || data[i]['id_via'] == 6) {
						$("option[id^='preciovia_"+data[i]['id_via']+"_']").attr('value',data[i]['monto_pre']+'-'+data[i]['monto_max']);
					}else{
						$("#preciovia_"+data[i]['id_via']).val(data[i]['monto_pre']+'-'+data[i]['monto_max']);
					}
				}
				$("#selprecio_ida").val('');
				$("#preciovia_ida").val('');
				$("#selprecio_vu").val('');
				$("#preciovia_vu").val('');
			}else{
				alert("ERROR, No se pudieron obtener los precios");
			}
		}
	});
}

function guardarGastos(){

	var ids     = new Array();
	//var precios = new Array();
	var montos   = new Array();
	var cants   = new Array();
	var totals  = new Array();
	var idProyecto = $("#proyec_presu").val();
	var idSoli = $("#idHidden").val();

	var c = 0;
	$('input[id^="montovia_"]').each(function(index){

		var parts=$(this).attr("id").split("_");
		var id_via=parts[1];
		if ($("#totalvia_"+id_via).val() != '') {  // if($("#checkSoli_"+id_soli).is(":checked")){
			c = 1;
			var arr;
			//precios.push($("#preciovia_"+id_via).val());
			montos.push($("#montovia_"+id_via).val());
			cants.push($("#cantvia_"+id_via).val());
			totals.push($("#totalvia_"+id_via).val());
			if (parseInt(id_via,10)==0 || parseInt(id_via,10)==1){
				arr=$("#preciovia_"+id_via).val().split("_");
				id_via=arr[1];
			}
			ids.push(id_via);
		}
	});

	ids     = ids.join();
	//precios = precios.join();
	cants   = cants.join();
	montos  = montos.join();
	totals  = totals.join();

	if (c == 0 || idProyecto == 0 || idSoli == '') {
		if(idSoli == ''){
			alert("Debe buscar una solicitud para calcularle sus viaticos");
			return;
		}else if(idProyecto == 0 ){
			alert("Debe especificar un proyecto");
			return;
		}else if (c == 0) {
			alert("Debe especificar al menos una cantidad para uno de los viaticos");
			return;
		}
	}

	$.ajax( {
		type : 'POST',
		url : "../controladores/trans/tViatico.php",
		data :	{ accion: "guardarGastos", ids:ids, montos:montos, cants:cants, totals:totals, idSoli:idSoli, idProyecto:idProyecto},
		error : function(xhr, ajaxOptions, thrownError) {
			alert("Ups... Algo esta mal :( ...");
		},
		beforeSend: function(data){
           // $("#prueba").html("<p align='center'><img src='img/Preloader_10.gif'></img></p>");
        },
		success : function(data) {
			var data = eval("(" + data + ")");
			if(data){
				alert("Los gastos fueron guardados con exito");
				 window.open('reportes/rptRelacionGastos.php?idSoli='+idSoli);
				//MostrarReporteDispon();
				location.reload();
			}else{
				alert("ERROR, No se pudieron guardar los gastos");
			}
		}
	});
}

function listarViaticos(){
	$.ajax( {
		type : 'POST',
		url : "../controladores/trans/tViatico.php",
		data :	{ accion: "listarViaticos"},
		error : function(xhr, ajaxOptions, thrownError) {
			alert("Ups... Algo esta mal :(");
		},
		success : function(data) {
			var data = eval("(" + data + ")");
			if(data){
				$('#tbl-sol').html("");
				var fila="<tr>"+
							"<td><strong>#</strong></td>"+
							"<td><strong>N° Solicitud</strong></td>"+
			        		"<td><strong>Lugar D.</strong></td>"+
			        		"<td><strong>Fecha Desde</strong></td>"+
			        		"<td><strong>Fecha Hasta</strong></td>"+
			        		"<td><strong>Motivo</strong></td>"+
			        		"<td><strong>T. Transporte</strong></td>"+
			        		"<td><strong>Estado S.</strong></td>"+
						"</tr>";
				$('#tbl-sol').append(fila);

				for(var i=0; i<data.length; i++){

					var tipo = data[i]['tipotrans_soli'];
					var estado_soli = data[i]['estado_soli'];

					if (estado_soli == 0) {
						estado_soli = 'No Aprobada';
					}else
						estado_soli = 'Aprobada';

					if (tipo == 1) {
						tipo = 'Aereo';
					}
					else if (tipo == 2) {
						tipo = 'Terrestre';
					}
					else {
						tipo = 'Otros';
					}

					var fila="<tr id='soli_"+data[i]['id_soli']+"'>"+
								"<td>"+(i+1)+"</td>"+
								"<td>"+data[i]['num_soli']+"</td>"+
								"<td>"+data[i]['lugardes_soli']+"</td>"+
								"<td>"+data[i]['fechades_soli']+"</td>"+
								"<td>"+data[i]['fechahas_soli']+"</td>"+
								"<td>"+data[i]['motivovia_soli']+"</td>"+
								"<td>"+tipo+"</td>"+
								"<td>"+estado_soli+"</td>"+
							"</tr>";
					$('#tbl-sol').append(fila);

					$('#soli_'+data[i]['id_soli']).click({nSoli:data[i]['id_soli']},function(evento){
						buscarSolicitud(evento.data.nSoli);
						$("#myModal22").modal('hide');
					});

				}
			}
		}
	});
}

function calcularPrecioViatico(id){
	var input = id.split('_');

	if ($("#montovia_"+input[1]).val() != '' && $("#"+id).val() != '') {
		var precio = parseFloat($("#montovia_"+input[1]).val());
		var cant   = parseFloat($("#"+id).val());
		var total = precio * cant;
		$("#totalvia_"+input[1]).val(total);
	}else
		$("#totalvia_"+input[1]).val('');

	var suma = 0;
	$('input[id^="montovia_"]').each(function(index){
		var parts=$(this).attr("id").split("_");
		var id_via=parts[1];
		if ($("#totalvia_"+id_via).val() != '') {
			suma = parseFloat(suma)+parseFloat($("#totalvia_"+id_via).val());
		}
	});

	$("#preciTotal").val(suma);
}

function verificarAnioMes(gen){

	var mes = $("#mes").val();
	var anio = $("#anio").val();

	if ($.trim(mes) == 0 || $.trim(anio) == '') {
		alert("ERROR, debe ingresar un mes y un año para poder realizar la busqueda de solicitudes");
		return;
	}else{
		if (gen==2){
			listatSolicitudesGerenadas();
		}else{
			listatSolicitudesMesAnio(gen);
		}
	}
}

function listatSolicitudesMesAnio(gen){

	var mes = $("#mes").val();
	var anio = $("#anio").val();

	$('#tbl-soli').html("");

	$.ajax( {
		type : 'POST',
		url : "../controladores/trans/tViatico.php",
		data :	{ accion: "listatSolicitudesNivelMesAnio", mes:mes, anio:anio, gen:gen},
		error : function(xhr, ajaxOptions, thrownError) {
			alert("Ups... Algo esta mal :(");
		},
		success : function(data) {
			var data = eval("(" + data + ")");
			if(data){
				var mi_nivel=parseInt($("#hid_nivel").val(),10)
				for(var i=0; i<data.length; i++){
					var checked='';
					var clase='';

					var name     = data[i]['nombre_per'].split(' ');
					var lastname = data[i]['apellido_per'].split(' ');

					var realname = name[0]+' '+lastname[0];

					if (data[i]['total'] == null) {
						var valor = 'Sin monto establecido';
					}else{
						valor = data[i]['total'];
					}

					var fila="<tr "+clase+">"+
								"<td>"+(i+1)+"</td>"+
								"<td>"+completarCodigoCeros(data[i]['num_soli'],10)+"</td>"+
								"<td>"+data[i]['cedula_per']+"</td>"+
								"<td>"+realname+"</td>"+
								"<td>"+data[i]['motivovia_soli']+"</td>"+
								"<td>"+data[i]['lugardes_soli']+"</td>"+
								"<td>"+data[i]['fechades_soli']+"</td>"+
								"<td>"+data[i]['fechahas_soli']+"</td>"+
								"<td><input type='checkbox' id='chksol-"+data[i]['id_soli']+"' "+checked+">"+
									"<input id='status-"+data[i]['id_soli']+"' type='hidden' value='"+data[i]['in_status']+"'></td>"+
							"</tr>";
				$('#tbl-soli').append(fila);

				}
			}else{
				var fila="<tr "+clase+">"+
								"<td colspan='10' class='text-center text-danger'><strong>No existen solicitudes para el mes/año especificado</strong></td>"+
							"</tr>";
				$('#tbl-soli').append(fila);
			}
		}
	});
}

function listatSolicitudesGerenadas(){

	var mes = $("#mes").val();
	var anio = $("#anio").val();

	$('#tbl-soli').html("");

	$.ajax( {
		type : 'POST',
		url : "../controladores/trans/tViatico.php",
		data :	{ accion: "listatSolicitudesGeneradas", mes:mes, anio:anio},
		error : function(xhr, ajaxOptions, thrownError) {
			alert("Ups... Algo esta mal :(");
		},
		success : function(data) {
			var data = eval("(" + data + ")");
			if(data){
				for(var i=0; i<data.length; i++){
					var p = parseInt(i);
					var clase='';

					var name     = data[i]['nombre_per'].split(' ');
					var lastname = data[i]['apellido_per'].split(' ');

					var realname = name[0]+' '+lastname[0];
					var fila="<tr "+clase+">"+
								"<td>"+(i+1)+"</td>"+
								"<td>"+completarCodigoCeros(data[i]['num_soli'],10)+"</td>"+
								"<td>"+data[i]['cedula_per']+"</td>"+
								"<td>"+realname+"</td>"+
								"<td>"+data[i]['motivovia_soli']+"</td>"+
								"<td>"+data[i]['lugardes_soli']+"</td>"+
								"<td>"+data[i]['fechades_soli']+"</td>"+
								"<td>"+data[i]['fechahas_soli']+"</td>"+
								"<td><input type='radio'  name='radio_soli' id='chksol-"+data[i]['id_soli']+"'>"+
							"</tr>";
				$('#tbl-soli').append(fila);

				}
			}else{
				var fila="<tr "+clase+">"+
								"<td colspan='10' class='text-center text-danger'><strong>No existen solicitudes para el mes/año especificado</strong></td>"+
							"</tr>";
				$('#tbl-soli').append(fila);
			}
		}
	});
}

function aprobarViaticos(){

    var ids=new Array();
    var status=new Array();
    $("input[id^='chksol-']").each(function(){
        if ($(this).is(":checked") && !$(this).is(":disabled")) {
        	var p = $(this).attr('id').split('-');
        	var id=p[1];
            ids.push(id);
            status.push($("#status-"+id).val());
        }
    });

    if (ids.length==0)
    {
        alert("Debe marcar al menos una solicitud para aprobar");
        return;
    }
    ids=ids.join();
    status=status.join();

    $.ajax( {
        type : 'POST',
        url : "../controladores/trans/tViatico.php",
        data :  { accion: "aprobarViaticos", ids:ids, status:status},
        error : function(xhr, ajaxOptions, thrownError) {
            alert("ERROR aprobarViaticos");
        },
        success : function(data) {
            var data = eval("(" + data + ")");
            if(data)
            {
                alert('Los viaticos han sido aprobados exitosamente');
                location.reload();
            }
            else{
                alert('Ha ocurrido un error al aprobar los viaticos');
            }
        }
    });
}

function cargarCiudad(){
	var ciudad_nueva = $("#ciu_nue").val();

	if (!ciudad_nueva) {
		$("#error_vaci").show().fadeOut(4000);
		return;
	}

	$.ajax( {
        type : 'POST',
        url : "../controladores/trans/tViatico.php",  
        data :  { accion: "cargarCiudad", ciudad:ciudad_nueva},
        error : function(xhr, ajaxOptions, thrownError) {
            alert("ERROR aprobarViaticos");
        },
        success : function(data) {
            var data = eval("(" + data + ")");
            if(data){
                $("#sin_error_ciu").show().fadeOut(4000);
                $("#ciu_nue").val('');
                //listarCiudadesSoli();
            }
            else{
                $("#error_ciu").show().fadeOut(4000);
                $("#ciu_nue").val('');
            }
        }
    });
}

function traerCiudades(est, opt, ciu){

	$.ajax( {
        type : 'POST',
        url : "../controladores/trans/tViatico.php",
        data :  { accion: "listarCiudades",estado:est,activa:1},
        error : function(xhr, ajaxOptions, thrownError) {
            alert("ERROR");
        },
        success : function(data) {
            var data = eval("(" + data + ")");
            if(data){

	               $('#'+opt).html("<option value='0'>Seleccione</option>");
	               //$('#ciu_des').html("<option value='0'>Seleccione</option>");
	               var sel;
					for(var i=0; i<data.length; i++){
						if (ciu == data[i]['id_ciu']) {
							sel="selected";
						}
						var option="<option "+sel+" value='"+data[i]['id_ciu']+"'>"+data[i]['nombre_ciu']+"</option>";
						$('#'+opt).append(option);
						//$('#ciu_des').append(option);
					}
            }
            else{
                //$("#error_ciu").show().fadeOut(4000);
                //$("#ciu_nue").val('');
            }
        }
    });
}

function listarEstados(){

	$.ajax( {
        type : 'POST',
        url : "../controladores/trans/tViatico.php",
        data :  { accion: "listarEstados"},
        error : function(xhr, ajaxOptions, thrownError) {
            alert("ERROR listarEstados");
        },
        success : function(data) {
            var data = eval("(" + data + ")");
            if(data){
               $('#esta').html("<option value='0'>Seleccione</option>");
               $('#esta_ori').html("<option value='0'>Seleccione</option>");
               $('#esta_des').html("<option value='0'>Seleccione</option>");
				for(var i=0; i<data.length; i++){
					var option="<option value='"+data[i]['id_es']+"'>"+data[i]['nombre_es']+"</option>";
					$('#esta').append(option);
					$('#esta_ori').append(option);
					$('#esta_des').append(option);
				}
            }
            else{
                //$("#error_ciu").show().fadeOut(4000);
                //$("#ciu_nue").val('');
            }
        }
    });

}

function listarCiudades(estado){
	//alert(estado);
	if (estado == 0) {
		alert("ERROR.. debe seleccionar un estado.");
		return;
	}
	//$("#box").append();
	$.ajax( {
        type : 'POST',
        url : "../controladores/trans/tViatico.php",
        data :  { accion: "listarCiudades",estado:estado,activa:0},
        error : function(xhr, ajaxOptions, thrownError) {
            alert("ERROR");
        },
        success : function(data) {
            $("#t_body").empty();
            var data = eval("(" + data + ")");
            if(data){
   					//alert('data[i]['status_ciu']')
				for(var i=0; i<data.length; i++){

					var iden = i+parseInt(1);
					var msj = data[i]['status_ciu'];
					var clase;
					if (msj == 0) {
						msj_aux = 'Activa';
						clase = 'text-success';
					}else{
						msj_aux = 'Desactivada';
						clase= 'text-danger';
					}

					var fila="<tr id='estatus-"+data[i]['id_ciu']+"'>"+
								"<td>"+(i+1)+"</td>"+
								"<td>"+data[i]['nombre_ciu']+"</td>"+
								"<td class='"+clase+"'>"+msj_aux+"</td>"+
								"<td><input type='checkbox' id='chkciu-"+data[i]['id_ciu']+"'></td>"+
							"</tr>";
							$('#t_body').append(fila);
				}
            }
            else{
                //$("#error_ciu").show().fadeOut(4000);
                //$("#ciu_nue").val('');
            }
        }
    });
}

function msjAlerta(){
	alert("POR FAVOR SEA RESPONSABLE AL CARGAR UNA CIUDAD, MANTENGA UNA BUENA ORTOGRAFIA Y ESCRIBA NOMBRES REALES DE CIUDADES");
}

function pagoDeViaticos(){
	var fecha = $("#fecha").val();
	var banco = $("#banco").val();
	var modo = 0;

		if ($("#inlineRadio2").is(':checked')) {
			modo = 1;
		}else if($("#inlineRadio3").is(':checked')){
			modo = 2;
		}

		var referencia = $("#referencia").val();

		var ids=new Array();
	    $("input[id^='chksol-']").each(function(){
	        if ($(this).is(":checked")) {
	        	var p = $(this).attr('id').split('-');
	        	var id=p[1];
	            ids.push(id);
	        }
	    });

	    var id_soli = '';
	    if (ids.length==0)
	    {
	        alert("Debe marcar la solicitud a pagar");
	        return;
	    }
	    id_soli=ids.join();

		 if(id_soli == '' || $.trim(referencia) == '' || $.trim(fecha)=='' || banco == 0){
		 	alert("Debe llenar todos los campos obligatorios");
		 	return;
		 }
		$.ajax( {
        type : 'POST',
        url : "../controladores/trans/tViatico.php",
        data :  { accion: "pagoDeViaticos", id_soli:id_soli, referencia:referencia, modo:modo,fecha:fecha,banco:banco},
        error : function(xhr, ajaxOptions, thrownError) {
            alert("ERROR pagoDeViaticos");
        },
        success : function(data) {
            var data = eval("(" + data + ")");
            if(data){
               alert('El pago del viatico se ha registrado exitosamente');
                listatSolicitudesGerenadas();
               limpiarFormPago();
               enviarCorreos(id_soli,4);
            }
            else{
                //$("#error_ciu").show().fadeOut(4000);
                //$("#ciu_nue").val('');
            }
        }
    });

}

function limpiarFormPago(){
	$("#referencia").val('');
	$("#fecha").val('');
	$("#banco").val('0');
}

function cancelarViaticos(){
	 var ids=new Array();
    $("input[id^='chksol-']").each(function(){ 
        if ($(this).is(":checked") && !$(this).is(":disabled")) {
        	var p = $(this).attr('id').split('-');
        	var id=p[1];
            ids.push(id);
        } 
    });

    if (ids.length==0)
    {
        alert("Debe marcar al menos una solicitud para cancelar");
        return;
    }
    ids=ids.join();

    $.ajax( {
        type : 'POST',
        url : "../controladores/trans/tViatico.php", 
        data :  { accion: "cancelarViaticos", ids:ids},
        error : function(xhr, ajaxOptions, thrownError) {
            alert("ERROR cancelarViaticos");
        },
        success : function(data) {
            var data = eval("(" + data + ")");
            if(data)
            {
                //alert('Los viaticos han sido cancelados exitosamente');
                //enviarCorreo(id,opc);
                listatSolicitudesMesAnio(0);
                enviarCorreos(ids,5);
            }
            else{
                alert('Ha ocurrido un error al cancelar los viaticos');
            }
        }
    });
}

function enviarCorreos(ids,tipo){
	 $.ajax( {
        type : 'POST',
        url : "../controladores/trans/tViatico.php", 
        data :  { accion: "enviarCorreos", ids:ids,opc:tipo},
        error : function(xhr, ajaxOptions, thrownError) {
            alert("ERROR enviarCorreos");
        },
        success : function(data) {
            var data = eval("(" + data + ")");
            if(data)
            {
            }
            else{
               // alert('Ha ocurrido un error al cancelar los viaticos');
            }
        }
    });
}

function desactivarCiudades(){
	var ids=new Array();
    $("input[id^='chkciu-']").each(function(){ 
        if ($(this).is(":checked") && !$(this).is(":disabled")) {
        	var p = $(this).attr('id').split('-');
        	var id=p[1];
            ids.push(id);
        } 
    });

    if (ids.length==0)
    {
        alert("Debe marcar al menos una ciudad para desactivarla");
        return;
    }
    ids=ids.join();

    $.ajax( {
        type : 'POST',
        url : "../controladores/trans/tViatico.php", 
        data :  { accion: "desactivarCiudades", ids:ids},
        error : function(xhr, ajaxOptions, thrownError) {
            alert("ERROR cancelarViaticos");
        },
        success : function(data) {
            var data = eval("(" + data + ")");
            if(data)
            {
                alert('Las ciudades se han desactivado exitosamente');
                listarCiudades($("#esta").val());
            }
            else{
                alert('Ha ocurrido un error al desactivar las ciudades');
            }
        }
    });
}
function activarCiudades(){
	var ids=new Array();
    $("input[id^='chkciu-']").each(function(){ 
        if ($(this).is(":checked") && !$(this).is(":disabled")) {
        	var p = $(this).attr('id').split('-');
        	var id=p[1];
            ids.push(id);
        } 
    });

    if (ids.length==0)
    {
        alert("Debe marcar al menos una ciudad para activarla");
        return;
    }
    ids=ids.join();

    $.ajax( {
        type : 'POST',
        url : "../controladores/trans/tViatico.php", 

        data :  { accion: "activarCiudades", ids:ids},
        error : function(xhr, ajaxOptions, thrownError) {
            alert("ERROR cancelarViaticos");
        },
        success : function(data) {
            var data = eval("(" + data + ")");
            if(data)
            {
                alert('Las ciudades se han activado exitosamente');
                listarCiudades($("#esta").val());
            }
            else{
                alert('Ha ocurrido un error al activar las ciudades');
            }
        }
    });
}

function enviarCorreo(id,opc){

    $.ajax( {
        type : 'POST',
        url : "../controladores/trans/tViatico.php",
        data :  { accion: "enviarCorreo", id:id,opc:opc},
        error : function(xhr, ajaxOptions, thrownError) {
            alert("ERROR enviarCorreo");
        },
        success : function(data) {
            var data = eval("(" + data + ")");
            if(data){
            	console.log(data);
                alert('Correo enviado');
            }
            else{
                alert('Ha ocurrido un error al enviar el correo');
            }
        }
    });
}

function busquedaAvanzasaVia(){

	var mes       = $("#mes").val();
	var anio      = $("#anio").val();
	var filtro    = $("#filtro").val();
	var cedula_em = $("#cedu").val();
	var fedesde   = $("#fedesde").val();
	var fehasta   = $("#fehasta").val();
	var esta_ori  = $("#esta_ori").val();
	var ciu_ori   = $("#ciu_ori").val();
	var esta_des  = $("#esta_des").val();
	var ciu_des   = $("#ciu_des").val();

	if ($.trim(mes) == 0 || $.trim(anio) == '') {
		alert("ERROR, Existen campos vacios");
		return;
	}

	$.ajax( {
        type : 'POST',
        url : "../controladores/trans/tViatico.php",
        data :  { accion: "busquedaAvanzasaVia", mes:mes,anio:anio, filtro:filtro,cedula_em:cedula_em,
        fedesde:fedesde,fehasta:fehasta,esta_ori:esta_ori,ciu_ori:ciu_ori,esta_des:esta_des,ciu_des:ciu_des},
        error : function(xhr, ajaxOptions, thrownError) {
            alert("ERROR busquedaAvanzasaVia");
        },
        success : function(data) {
            var data = eval("(" + data + ")");
            $('#tbl-soli').html("");
            if(data){
            	console.log(data);
            	var state = '';
                for(var i=0; i<data.length; i++){

                	alert(data[i]['estado_soli']);

                	if (data[i]['estado_soli'] == 0) {
                		state = 'Solicitud Recien Creada';
                	}else if(data[i]['estado_soli'] == 1){
                		state = 'Solicitudes aprobadas sin cálculo de gastos';
                	}else if(data[i]['estado_soli'] == 2){
                		state = 'Solicitudes con gastos aún no aprobados';
                	}else if(data[i]['estado_soli'] == 3){
                		state = 'Solicitudes con gastos aprobados';
                	}else if(data[i]['estado_soli'] == 3){
                		state = 'Solicitudes pagadas';
                	}else{
                		state = 'Solicitudes canceladas';
                	}

					var fila="<tr>"+
								"<td>"+(i+1)+"</td>"+
								"<td>"+completarCodigoCeros(data[i]['num_soli'],10)+"</td>"+
								"<td>"+data[i]['cedula_per']+"</td>"+
								"<td>"+data[i]['nombre_per']+"</td>"+
								"<td>"+data[i]['motivovia_soli']+"</td>"+
								"<td>"+data[i]['lugardes_soli']+"</td>"+
								"<td>"+data[i]['fechades_soli']+"</td>"+
								"<td>"+data[i]['fechahas_soli']+"</td>"+
								"<td>"+state+"</td>"+
							"</tr>";
				$('#tbl-soli').append(fila);

				}
            }
            else{
                var fila="<tr>"+
								"<td colspan='9' class='text-center text-danger'><strong>No existen resultado para el mes, año y filtro especificado</strong></td>"+
							"</tr>";
				$('#tbl-soli').append(fila);
            }
        }
    });
}

function mostrarReporte(){

	var mes       = $("#mes").val();
	var anio      = $("#anio").val();
	var filtro    = $("#filtro").val();
	var cedula_em = $("#cedu").val();
	var fedesde   = $("#fedesde").val();
	var fehasta   = $("#fehasta").val();
	var esta_ori  = $("#esta_ori").val();
	var ciu_ori   = $("#ciu_ori").val();
	var esta_des  = $("#esta_des").val();
	var ciu_des   = $("#ciu_des").val();

	if ($.trim(mes) == 0 || $.trim(anio) == '') {
		alert("ERROR, Existen campos vacios");
		return;
	}

	window.open('../../vistas/reportes/rpt_list_soli.php?mes='+mes+'&anio='+anio+'&filtro='+filtro+'&cedula_em='+cedula_em+'&fedesde='+fedesde+'&fehasta='+fehasta+'&esta_ori='+esta_ori+'&ciu_ori='+ciu_ori+'&esta_des='+esta_des+'&ciu_des='+ciu_des);
}

function mostrarPlanilla(id_soli){

	window.open('../../vistas/reportes/rptPlanillaSoli.php?id_soli='+id_soli);
}

function mostrarPlanillaGastos(id_soli){

	window.open('../../vistas/reportes/rptRelacionGastos1.php?id_soli='+id_soli);
}

function mostrarPlanilla1(id_soli){

	window.open('reportes/rptPlanillaSoli.php?id_soli='+id_soli);
}

function historialDeViaticos(){

	var tipo = $('#consul').val();
	var cedu = $('#cedu').val();
	var cod  = $('#cod').val();

	if (tipo == 0) {
		alert("ERROR... Debe seleccionar el tip de consulta que desea realizar");
		return;
	}

	if (tipo == 1 && !cedu) {
		alert("ERROR... Debe introducir un número de cédula");
		return;
	}

	if (tipo == 2 && !cod || $.trim(cedu) == '') {
		alert("ERROR... Debe introducir el número de solicitud y un número de cédula");
		return;
	}

		$.ajax( {
        type : 'POST',
        url : "../controladores/trans/tViatico.php",
        data :  { accion: "historialDeViaticos", tipo:tipo,cedu:cedu,cod:cod},
        error : function(xhr, ajaxOptions, thrownError) {
            alert("ERROR historialDeViaticos");
        },
        success : function(data) {
            var data = eval("(" + data + ")");
            $('#tbl-his').html("");
            if(data){
            	console.log(data);
            	var state = '';
                for(var i=0; i<data.length; i++){

                	if (data[i]['estado_soli'] == 0) {
                		state = 'Solicitud Recien Creada';
                	}else if(data[i]['estado_soli'] == 1){
                		state = 'Solicitudes aprobadas sin cálculo de gastos';
                	}else if(data[i]['estado_soli'] == 2){
                		state = 'Solicitudes con gastos aún no aprobados';
                	}else if(data[i]['estado_soli'] == 3){
                		state = 'Solicitudes con gastos aprobados';
                	}else if(data[i]['estado_soli'] == 3){
                		state = 'Solicitudes pagadas';
                	}else{
                		state = 'Solicitudes canceladas';
                	}

					var fila="<tr>"+
								"<td>"+(i+1)+"</td>"+
								"<td>"+completarCodigoCeros(data[i]['num_soli'],10)+"</td>"+
								"<td>"+data[i]['cedula_per']+"</td>"+
								"<td>"+data[i]['nombre_per']+"</td>"+
								"<td>"+data[i]['motivovia_soli']+"</td>"+
								"<td>"+data[i]['lugardes_soli']+"</td>"+
								"<td>"+data[i]['fe_creado']+"</td>"+
								"<td>"+data[i]['fechades_soli']+"</td>"+
								"<td>"+data[i]['fechahas_soli']+"</td>"+
								"<td>"+state+"</td>"+
							"</tr>";
				$('#tbl-his').append(fila);

				}
            }
            else{
                var fila="<tr>"+
								"<td colspan='10' class='text-center text-danger'><strong>No existen solicitudes</strong></td>"+
							"</tr>";
				$('#tbl-his').append(fila);
            }
        }
    });
}

function buscarStatusSoli(){

	var soli = $("#soli").val();

	if ($.trim(soli) == '') {
		alert("ERROR... Debe ingresar un númeno de solicitud");
		return;
	}

	$("#tbl_status").empty();

	$.ajax( {
        type : 'POST',
        url : "../controladores/trans/tViatico.php",
        data :  { accion: "buscarStatusSoli", soli:soli},
        error : function(xhr, ajaxOptions, thrownError) {
            alert("ERROR buscarStatusSoli");
        },
        success : function(data) {
            var data = eval("(" + data + ")");
            if(data){
            	console.log(data);
            	//alert(data[0]['in_status']);
            	var cancelado = data[0]['estado_soli'];
            	var soli = data[0]["num_soli"];
            	if (cancelado == 5) {
            		var icon = "glyphicon glyphicon-remove";
            		var msj = 'SOLICITUD CANCELADA';
            		//alert("La solicitud "+soli+" Ha sido cancelada");
            		filas= filas+'<tr>'+
					//'<td width="10%" style="text-align: center">'+(i+1)+'</td>'+
					'<td width="10%" style="text-align: center"><span class="'+icon+'"></span></td>'+
					'<td>'+msj+'</td>'+
				'</tr>';
				$("#tbl_status").html(filas);
            		return;
            	}
            	var status   = new Array();
            	status.push("Solicitud recién creada");
            	status.push("Solicitud aprobada por Administración");
            	status.push("Solicitud aprobada por Director de Zona");
            	status.push("Solicitud aprobada por Planificación");
            	status.push("Solicitud aprobada por Verificación y Control");
            	status.push("Solicitud aprobada por Administración");
            	status.push("Solicitud aprobada por Contraloría Interna");
				var filas='';
            	for (var i = 0; i < status.length; i++) {
            		var ok = '';
            		if (data[0]['in_status'] >= i) {
            			ok = 'glyphicon glyphicon-ok';
            		}
            		filas= filas+'<tr>'+
					'<td width="10%" style="text-align: center">'+(i+1)+'</td>'+
					'<td width="10%" style="text-align: center"><span class="'+ok+'"></span></td>'+
					'<td>'+status[i]+'</td>'+
				'</tr>';
            	}
            	$("#tbl_status").html(filas);
            }
            else{
                alert("Disculpe la solicitud no fue encontrada");
                return;
            }
        }
    });
}

/*function listarSoliStatus(){
	var filtro  = $("#filtro").val();
	var fedesde = $("#fedesde").val();
	var fehasta = $("#fehasta").val();

	if ($.trim(filtro) == '-' || $.trim(fedesde) == '' || $.trim(fehasta) == '') {
		alert("ERROR... Existen campos vacios");
		return;
	}

	/*fedesde.split('-');
	fehasta.split('-');

	if (fehasta[0] < fedesde[0] || fehasta[1] <= fedesde[1] || fehasta[2] <= fedesde[2]) {
		alert("ERROR... Valores incorrectos en la fecha hasta, los valores deben ser mayor que la fecha desde");
		return;
	}

	$.ajax( {
        type : 'POST',
        url : "../controladores/trans/tViatico.php",
        data :  { accion: "listarSoliStatus", filtro:filtro,fedesde:fedesde,fehasta:fehasta},
        error : function(xhr, ajaxOptions, thrownError) {
            alert("ERROR listarSoliStatus");
        },
        success : function(data) {
            var data = eval("(" + data + ")");
            $('#tbl-his').html("");
            if(data){
            	console.log(data);
            	var state = '';
                for(var i=0; i<data.length; i++){

                	if (data[i]['estado_soli'] == 0) {
                		state = 'Solicitud Recien Creada';
                	}else if(data[i]['estado_soli'] == 1){
                		state = 'Solicitudes aprobadas sin cálculo de gastos';
                	}else if(data[i]['estado_soli'] == 2){
                		state = 'Solicitudes con gastos aún no aprobados';
                	}else if(data[i]['estado_soli'] == 3){
                		state = 'Solicitudes con gastos aprobados';
                	}else if(data[i]['estado_soli'] == 3){
                		state = 'Solicitudes pagadas';
                	}else{
                		state = 'Solicitudes canceladas';
                	}

					var fila="<tr>"+
								"<td>"+(i+1)+"</td>"+
								"<td>"+completarCodigoCeros(data[i]['num_soli'],10)+"</td>"+
								"<td>"+data[i]['cedula_per']+"</td>"+
								"<td>"+data[i]['nombre_per']+"</td>"+
								"<td>"+data[i]['motivovia_soli']+"</td>"+
								"<td>"+data[i]['lugardes_soli']+"</td>"+
								"<td>"+data[i]['fe_creado']+"</td>"+
								"<td>"+data[i]['fechades_soli']+"</td>"+
								"<td>"+data[i]['fechahas_soli']+"</td>"+
								"<td>"+state+"</td>"+
							"</tr>";
				$('#tbl-his').append(fila);

				}
            }
            else{
                var fila="<tr>"+
								"<td colspan='10' class='text-center text-danger'><strong>No existen solicites</strong></td>"+
							"</tr>";
				$('#tbl-his').append(fila);
            }
        }
    });
}*/
/*
var arreglo = $("#selprecio_ida").val().split('-');
alert(arreglo[0]); //va a mostrar 1000 p. ej
alert(arreglo[0]); //va a mostrar 2000 p. ej

*/