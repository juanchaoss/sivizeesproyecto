function limpiarFormPartida(){
	$("#codi").val('');
	$("#descri").val('');
}
function limpiarFormProyecto(){
	$("#descri_pro").val('');
	$("#impu_presu").val('');
}
function crearPartida(){
	var codigo = $('#codi').val();
	var descripcion = $('#descri').val();

		if ($.trim(codigo) == 0 || $.trim(descripcion) == 0) {
			alert("ERROR, Existe campos vacios");
			return false;
		}
		if (confirm('¿Esta seguro de guardar la nueva partida?')) {
				$.ajax( {
					type : 'POST',
					url : "../controladores/trans/tPresupuesto.php",
					data :	{ accion: "cargarPartida", codigo: codigo, descripcion: descripcion},
					error : function(xhr, ajaxOptions, thrownError) {
						alert("Ups... Algo esta mal :(");
					},
					beforeSend: function(data){
		                //$("#img").html("<p align='center'><img src='img/Preloader_3.gif'></p>");
		            },
		            complete: function(data){
		            	//$("#img").html("");
		            },
					success : function(data) {
						var data = eval("(" + data + ")");
						if(data){
							//moverDirectorioSesion();
							alert("Patida registrada existosamente");
							limpiarFormPartida();
						}else{
							alert("ERROR, No se pudo registrar la partida "+codigo);
							//$("#error-usu").show();
							//$('#prueba').html("<br><br><b><p align='center' style='font-size:1.5em; color: red;'>Datos incorrectos</p></b>");
						}
					}
		    });
		}else{
			return false;
		}
}

function formatearPartida(){
	var cadena = $("#codi").val().split('.');

	if ($.trim(cadena[0]).length < 1 || $.trim(cadena[0]).length > 2) {
		alert("La partida introducida no cumple con el formato");
		return;
	}

	if ($.trim(cadena[1]).length < 1 || $.trim(cadena[1]).length > 2) {
		alert("La partida introducida no cumple con el formato");
		return;
	}

	if ($.trim(cadena[2]).length < 1 || $.trim(cadena[2]).length > 2) {
		alert("La partida introducida no cumple con el formato");
		return;
	}

	if ($.trim(cadena[3]).length < 1 || $.trim(cadena[3]).length > 2) {
		alert("La partida introducida no cumple con el formato");
		return;
	}

	if ($.trim(cadena[4]).length < 1 || $.trim(cadena[4]).length > 2) {
		alert("La partida introducida no cumple con el formato");
		return;
	}
}

function cargarParti(ok){
	
	if (ok) {
		listarParitdas();
		return;
	}

	$.ajax( {
		type : 'POST',
		url : "../controladores/trans/tPresupuesto.php",
		data :	{ accion: "cargarParti"},
		error : function(xhr, ajaxOptions, thrownError) {
			alert("Ups... Algo esta mal :(");
		},
		success : function(data) {
			var data = eval("(" + data + ")");
			if(data){
				//console.log(data);
				$('#impu_presu').html("<option value='0'>Seleccione</option>");
				$('#impu_afect').html("<option value='0'>Seleccione</option>");
				$('#impu_presu_aux').html("<option value='0'>Seleccione</option>");
					for(var i=0; i<data.length; i++){
						var option="<option value='"+data[i]['id_partida']+"&@"+data[i]['descripcion_par']+"&@"+data[i]['dispon']+"'>"+data[i]['cuenta_par'] +' - '+data[i]['descripcion_par']+"</option>";
						$('#impu_presu').append(option);
						//$('#impu_afect').append(option);
						$('#impu_presu_aux').append(option);
					}
			}
		}
	});
}

function cargarPartiExcept(except){

	$.ajax( {
		type : 'POST',
		url : "../controladores/trans/tPresupuesto.php",
		data :	{ accion: "cargarParti", except:except},
		error : function(xhr, ajaxOptions, thrownError) {
			alert("Ups... Algo esta mal :(");
		},
		success : function(data) {
			var data = eval("(" + data + ")");
			if(data){
				$('#impu_afect').html("<option value='0'>Seleccione</option>");
					for(var i=0; i<data.length; i++){
						var option="<option value='"+data[i]['id_partida']+"&@"+data[i]['descripcion_par']+"&@"+data[i]['dispon']+"'>"+data[i]['cuenta_par'] +' - '+data[i]['descripcion_par']+"</option>";
						$('#impu_afect').append(option);
					}
			}
		}
	});
}

function listarParitdas(){

	$.ajax( {
		type : 'POST',
		url : "../controladores/trans/tPresupuesto.php",
		data :	{ accion: "cargarParti"},
		error : function(xhr, ajaxOptions, thrownError) {
			alert("Ups... Algo esta mal :(");
		},
		success : function(data) {
			var data = eval("(" + data + ")");
			$('#tbl_par').empty();
			if(data){
				console.log(data);
				for(var i=0; i<data.length; i++){
					var iden = i+parseInt(1);
					var fila="<tr>"+
								"<td>"+iden+"</td>"+
								"<td>"+data[i]['cuenta_par']+"</td>"+
								"<td>"+data[i]['descripcion_par']+"</td>"+
							"</tr>";
							$('#tbl_par').append(fila);
				   }
			}
		}
	});

}

function buscarCuentasProyecto(id_pro){
	$('#impu_presu').val('');
	$('#cuenta_presu').val('');
	if(id_pro!=0)
	{
		$.ajax( {
			type : 'POST',
			url : "../controladores/trans/tPresupuesto.php",
			data :	{ accion: "buscarCuentasProyecto", id_pro:id_pro},
			error : function(xhr, ajaxOptions, thrownError) {
				alert("Ups... Algo esta mal :(  222");
			},
			success : function(data) {
				var data = eval("(" + data + ")");
				if(data){
					$('#impu_presu').val(data[0]['cuenta_par']);
					$('#cuenta_presu').val(data[0]['descripcion_par']);
				}
			}
		});
	}
}

function traerCuenta(){
	var id_partida=$('#impu_presu').val();
	$('#cuenta').val('');
    if(id_partida!=0){
		var partes = id_partida.split("&@")
		$('#cuenta').val(partes[1]);
    }
	/*$.ajax( {
			type  : 'POST',
			async: false,
			url   : "../controladores/trans/tPresupuesto.php",
			data  :	{ accion: "traerCuenta",id_partida:id_partida},
			error : function(xhr, ajaxOptions, thrownError) {
				alert("ERROR");
			},
			success : function(data) {
				var data = eval("(" + data + ")");
				if(data){
					$('#cuenta').val(data[0]['descripcion_par']);
				}
			}ata[i]['id_pro']
		});*/
}

function registrarProyecto(){
	var impu   = $("#impu_presu").val();
	var descrip = $("#descri_pro").val();

	if ($.trim(impu) == 0 || $.trim(descrip) == '') {
		alert("ERROR... Existen campos vacios, debe llenar todos los campos");
		return;
	}

	var partes = impu.split("&@")
	impu=partes[0];

	if (confirm('¿Esta seguro de guardar el proyecto?')) {
		$.ajax( {
			type : 'POST',
			url : "../controladores/trans/tPresupuesto.php",
			data :	{ accion: "registrarProyecto", impu: impu, descrip: descrip},
			error : function(xhr, ajaxOptions, thrownError) {
				alert("Ups... Algo esta mal :(");
			},
			success : function(data) {
				var data = eval("(" + data + ")");
				if(data){
					//moverDirectorioSesion();
					alert("Proyecto registrado existosamente");
					limpiarFormProyecto();
				}else{
					alert("ERROR, No se pudo registrar el proyecto "+descrip);
					//$("#error-usu").show();
					//$('#prueba').html("<br><br><b><p align='center' style='font-size:1.5em; color: red;'>Datos incorrectos</p></b>");
				}
			}
    	});
	}
}
function limpiarFormMovimiento(){
	$("#impu_presu").val('');
	$("#monto").val('');
	$("#descrip").val('');
	$("#fecha").val('');//ata[i]['id_pro']
	$("#num_refe").val('');
}
function guardarMovimiento(){
	var impu     = $("#impu_presu").val();
	var num_refe = $("#num_refe").val();
	var monto    = parseFloat($("#monto").val());
	var descrip = $("#descrip").val();
	var fecha   = $("#fecha").val();
	var traspa  = $("#impu_afect").val();
	var banco   = $("#banco").val();
	var modo    = 0;
	var dispon = parseFloat(str_replace(str_replace($("#dispon").val(), "", "."),".",",")); 

	if ($("#inlineRadio2").is(':checked')) {
		modo = 1;
	}else if($("#inlineRadio3").is(':checked')){
		modo = 2;
	}

	var partes = impu.split("&@");
	impu=partes[0];

	if (!$("#traspaso").is(':checked')){
		traspa = 0;
	}

	if ($("#traspaso").is(':checked')) {
		if ($.trim(impu) == 0  || $.trim(monto) == '' || $.trim(descrip) == '' || $.trim(fecha) == '' || traspa == 0) {

			if (monto > dispon) {
				alert("ERROR... El monto introducido es mayor al monto disponible de la partida seleccionada");
				$("#monto").focus();
				return;
			}
			alert("ERROR, Existen datos vacios, debe llenar todos los campos");
			return;

		}

		var partes = traspa.split("&@");
		traspa=partes[0];

		if (monto > dispon) {
			alert("ERROR, El monto del movimiento supera el monto disponible en la partida");
			return;
		};
	}else{
		if ($.trim(impu) == 0  || $.trim(monto) == '' || $.trim(descrip) == '' || $.trim(fecha) == ''
			|| $.trim(num_refe) == '' || banco == 0) {
			alert("ERROR, Existen datos vacios, debe llenar todos los campos");
			return;
		}
	}

	if (monto == 0) {
		alert("ERROR, El monto debe ser mayor a 0");
		return;
	}

	if (confirm('¿Esta seguro de registrar el actual movimiento?')) {
		$.ajax( {
			type : 'POST',
			url : "../controladores/trans/tPresupuesto.php",
			data :	{ accion: "guardarMovimiento", impu: impu, descrip: descrip, num_refe:num_refe, monto:monto, fecha:fecha,
						modo:modo,traspa:traspa,banco:banco},
			error : function(xhr, ajaxOptions, thrownError) {
				alert("Ups... Algo esta mal :(");
			},
			success : function(data) {
				var data = eval("(" + data + ")");
				if(data){
					//moverDirectorioSesion();
					alert("Movimiento registrado existosamente");
					limpiarFormMovimiento();
					cargarParti();
				}else{
					alert("ERROR, No se pudo registrar el movimiento ");
					//$("#error-usu").show();
					//$('#prueba').html("<br><br><b><p align='center' style='font-size:1.5em; color: red;'>Datos incorrectos</p></b>");
				}
			}
    	});
	}
}

function buscarProyectos(){
		$.ajax( {
			type : 'POST',
			url : "../controladores/trans/tPresupuesto.php",
			data :	{ accion: "buscarProyectos"},
			error : function(xhr, ajaxOptions, thrownError) {
				alert("Ups... Algo esta mal :( 333");
			},
			success : function(data) {
				var data = eval("(" + data + ")");
				if(data){
					$('#proyec_presu').html("<option value='0'>Seleccione</option>");
					for(var i=0; i<data.length; i++){
						var option="<option value='"+data[i]['id_pro']+"'>"+data[i]['nombre_pro'] +"</option>";
						$('#proyec_presu').append(option);
					}
				}else{
					//alert("ERROR, No se pudo registrar el proyecto "+descrip);
					//$("#error-usu").show();
					//$('#prueba').html("<br><br><b><p align='center' style='font-size:1.5em; color: red;'>Datos incorrectos</p></b>");
				}
			}
    	});
}

function listarMovimientos(){
	$.ajax( {
			type : 'POST',
			url : "../controladores/trans/tPresupuesto.php",
			data :	{ accion: "listarMovimientos"},
			error : function(xhr, ajaxOptions, thrownError) {
				alert("Ups... Algo esta mal :(");
			},
			success : function(data) {
				var data = eval("(" + data + ")");
				$('#head_movi').empty();
				var mont_final = 0;

				if(data){
					for(var i=0; i<data.length; i++){
						var modo = data[i]['modo_movi'];
						var monto = parseFloat(data[i]['monto_movi']);
						//console.log(monto);
						var msj='';
						if (modo == 0) {
							msj = 'Cheque';
						}else if(modo == 1){
							msj = 'Transferencia';
						}else if(modo == 3){
							msj = 'Traspaso';
						}else{msj = 'Deposito';}

					var iden = i+parseInt(1);
					var fila="<tr>"+
								"<td>"+iden+"</td>"+
								"<td>"+data[i]['descripcion_movi']+"</td>"+
								"<td>"+data[i]['monto_movi']+"</td>"+
								"<td>"+data[i]['fecha_movi']+"</td>"+
								"<td>"+msj+"</td>"+
								"<td>"+data[i]['cuenta_par']+"</td>"+
								"<td>"+data[i]['num_referencia']+"</td>"+
							"</tr>";
							$('#head_movi').append(fila);

							mont_final = mont_final+monto;
				   }
				   $("#mont_final").val('Bs. '+mont_final);
				}
			}
    	});
}

function listarBanco(){
	$.ajax( {
			type : 'POST',
			url : "../controladores/trans/tPresupuesto.php",
			data :	{ accion: "listarBanco"},
			error : function(xhr, ajaxOptions, thrownError) {
				alert("Ups... Algo esta mal :(");
			},
			success : function(data) {
				var data = eval("(" + data + ")");
				if(data){
					$('#banco').html("<option value='0'>Seleccione</option>");
					for(var i=0; i<data.length; i++){
						var option="<option value='"+data[i]['id_banco']+"'>"+data[i]['nom_banco'] +"</option>";
						$('#banco').append(option);
					}
				}
			}
    	});
}

function filtrarSumaPartida(){
	var mes = $("#mes").val();
	var anio = $("#anio").val();
	var partida = $("#impu_presu_aux").val();

	/*alert(mes);
	alert(anio);
	alert(partida);*/
	//return;

	if ($.trim(mes) == 0 || $.trim(anio) == '' || $.trim(partida) == 0) {
		alert("ERROR... Existen campos vacios");
		return;
	}

	$.ajax( {
			type : 'POST',
			url : "../controladores/trans/tPresupuesto.php",
			data :	{ accion: "filtrarSumaPartida",mes:mes,anio:anio,partida:partida},
			error : function(xhr, ajaxOptions, thrownError) {
				alert("Ups... Algo esta mal :(");
			},
			success : function(data) {
				var data = eval("(" + data + ")");
				console.log(data);
				$('#head_movi').html("");
				var mont_final = 0;
				if(data){

					for(var i=0; i<data.length; i++){
						var modo = data[i]['modo_movi'];
						var monto = parseFloat(data[i]['monto_movi']);
						//console.log(monto);
						var msj='';
						if (modo == 0) {
							msj = 'Cheque';
						}else if(modo == 1){
							msj = 'Transferencia';
						}else if(modo == 2){
							msj = 'Deposito';
						}else{msj = 'Traspaso';}

					var iden = i+parseInt(1);
					var fila="<tr>"+
								"<td>"+iden+"</td>"+
								"<td>"+data[i]['descripcion_movi']+"</td>"+
								"<td>"+data[i]['monto_movi']+"</td>"+
								"<td>"+data[i]['fecha_movi']+"</td>"+
								"<td>"+msj+"</td>"+
								"<td>"+data[i]['cuenta_par']+"</td>"+
								"<td>"+data[i]['num_referencia']+"</td>"+
							"</tr>";
							$('#head_movi').append(fila);

							mont_final = mont_final+monto;
				   }
				   $("#mont_final").val('Bs. '+mont_final);
				}else{
					var fila="<tr>"+
								"<td colspan='7' class='text-center text-danger'><strong>No existen movimientos con la partida y el mes/año especificado</strong></td>"+
							"</tr>";
				     $('#head_movi').append(fila);
				}
			}
    	});
}
function mostrarReporteMovi(){
	var mes  = $("#mes").val();
	var anio = $("#anio").val();
	var impu = $("#impu_presu_aux").val();

	if ($.trim(mes)==0 || $.trim(anio)=='') {
		alert("ERROR...Existen campos vacios");
		return;
	}
	window.open('../vistas/reportes/rpt_list_movi.php?mes='+mes+'&anio='+anio+'&impu='+impu);
}
function listarProyectos(){
	$.ajax( {
			type : 'POST',
			url : "../controladores/trans/tPresupuesto.php",
			data :	{ accion: "listarProyectos"},
			error : function(xhr, ajaxOptions, thrownError) {
				alert("Ups... Algo esta mal :(");
			},
			success : function(data) {
				console.log(data);
				var data = eval("(" + data + ")");
				$('#tbl_body').empty();
				if(data){
					for(var i=0; i<data.length; i++){
					var iden = i+parseInt(1);
					var fila="<tr>"+
								"<td>"+iden+"</td>"+
								"<td>"+data[i]['nombre_pro']+"</td>"+
								"<td>"+data[i]['cuenta_par']+"</td>"+
							"</tr>";
							$('#tbl_body').append(fila);
				   }
				}
			}
    	});
}

function buscarPrecioFecha(){

	var mes = $("#mes").val();
	var anio = $("#anio").val();

	if ($.trim(mes) == 0 || $.trim(anio) == '') {
		alert("ERROR... Debe Especificar una fecha");
		return;
	}

	$.ajax( {
			type : 'POST',
			url : "../controladores/trans/tPresupuesto.php",
			data :	{ accion: "buscarPrecioFecha",mes:mes,anio:anio},
			error : function(xhr, ajaxOptions, thrownError) {
				alert("Ups... Algo esta mal :(");
			},
			success : function(data) {
				console.log(data);
				var data = eval("(" + data + ")");
				$('#tbl-pre').empty();
				if(data){
					for(var i=0; i<data.length; i++){
					var iden = i+parseInt(1);
					var fila="<tr>"+
								"<td>"+iden+"</td>"+
								"<td>"+data[i]['nombre_via']+"</td>"+
								"<td>"+data[i]['monto_pre']+"</td>"+
								"<td>"+data[i]['fecha_pre']+"</td>"+
							"</tr>";
							$('#tbl-pre').append(fila);
				   }
				}else{
				   	 var fila="<tr>"+
								"<td colspan='4' class='text-center text-danger'><strong>No existen resultado para el mes, año y filtro especificado</strong></td>"+
							"</tr>";
					$('#tbl-pre').append(fila);
				   }
			}
    	});
}

function mostrarDispon(){
	var dispon = '';

	if ($("#impu_presu").val() != 0) {
		var parte = $("#impu_presu").val().split('&@');
		dispon = parte[2];
		cargarPartiExcept(parte[0]);
	}else{
		$('#impu_afect').html("<option value='0'>Seleccione</option>");
	}
	$("#dispon").val(dispon);
	toComa('dispon',2);
}