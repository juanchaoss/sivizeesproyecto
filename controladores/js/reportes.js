function listarEstados(){

	$.ajax( {
        type : 'POST',
        url : "../../controladores/trans/tViatico.php",
        data :  { accion: "listarEstados"},
        error : function(xhr, ajaxOptions, thrownError) {
            alert("ERROR listarEstados");
        },
        success : function(data) {
            var data = eval("(" + data + ")");
            if(data){
               $('#esta').html("<option value='0'>Seleccione</option>");
               $('#esta_ori').html("<option value='0'>Seleccione</option>");
               $('#esta_des').html("<option value='0'>Seleccione</option>");
				for(var i=0; i<data.length; i++){
					var option="<option value='"+data[i]['id_es']+"'>"+data[i]['nombre_es']+"</option>";
					$('#esta').append(option);
					$('#esta_ori').append(option);
					$('#esta_des').append(option);
				}
            }
            else{
                //$("#error_ciu").show().fadeOut(4000);
                //$("#ciu_nue").val('');
            }
        }
    });

}

function busquedaZona(){

    var filtro = $("#filtro").val();
    var fedesde = $("#fedesde").val();
    var fehasta = $("#fehasta").val();
    var esta_des = $("#esta_des").val();
    var ciu_des = $("#ciu_des").val();
    var cons = $("#tip").val();

    if ($.trim(cons) == '-') {
        alert("ERROR... Debe seleccionar un tipo de busqueda");
        return;
    }

    $.ajax( {
        type : 'POST',
        url : "../../controladores/trans/tViatico.php",
        data :  { accion: "busquedaZona",  filtro:filtro,fedesde:fedesde,fehasta:fehasta,esta_des:esta_des,
        ciu_des:ciu_des, cons:cons},
        error : function(xhr, ajaxOptions, thrownError) {
            alert("ERROR busquedaZona");
        },
        success : function(data) {
            var data = eval("(" + data + ")");
            $('#tbl-soli').html("");
            if(data){

                var total = parseFloat(data[0]['total']);
                var es=data[0]['id_edo'];
                var enc=0;
                var total_es=0;
                var k=1;
                for(var i=0; i<data.length; i++){
                    var cant = parseFloat(data[i]['cont']);
                    var porc = parseFloat(cant*100/total);
                    porc = number_format( porc, 2, ',', '.' );
                    if (cons==0)
                    {
                        if(enc==0)
                        {
                            total_es=parseFloat(data[i]['cont']);
                            var ciudad="-";
                            if(data[i]['nombre_ciu'] && data[i]['nombre_ciu']!=""){
                                ciudad=data[i]['nombre_ciu'];
                            }
                            var fila="<tr class='info'>"+
                                    "<td>"+k+"</td>"+
                                    "<td><strong>"+data[i]['nombre_edo']+"</strong></td>"+
                                    "<td id='totales-"+data[i]['id_edo']+"'>&nbsp;</td>"+
                                    "<td id='porces-"+data[i]['id_edo']+"'>&nbsp;</td>"+
                                "</tr>"+
                                "<tr>"+
                                    "<td>&nbsp;</td>"+
                                    "<td>"+ciudad+"</td>"+
                                    "<td>"+data[i]['cont']+"</td>"+
                                    "<td>"+porc+"%</td>"+
                                "</tr>";
                            $('#tbl-soli').append(fila);
                            enc=1;
                            k++;
                        }else{

                            if(data[i]['id_edo']==es){
                                total_es=parseFloat(total_es)+parseFloat(data[i]['cont']);
                                var fila="<tr>"+
                                        "<td>&nbsp;</td>"+
                                        "<td>"+data[i]['nombre_ciu']+"</td>"+
                                        "<td>"+data[i]['cont']+"</td>"+
                                        "<td>"+porc+"%</td>"+
                                    "</tr>";
                                $('#tbl-soli').append(fila);
                            }else{
                                var porc_es = parseFloat(total_es*100/total);
                                porc_es = number_format( porc_es, 2, ',', '.' );
                                $("#totales-"+es).html("<strong>"+total_es+"</strong>");
                                $("#porces-"+es).html("<strong>"+porc_es+"</strong>");

                                es=data[i]['id_edo'];
                                i--;
                                enc=0;
                            }
                        }
                    }else{
                        var fila="<tr>"+
                                    "<td>"+(i+1)+"</td>"+
                                    "<td>"+data[i]['nombre_edo']+"</td>"+
                                    "<td>"+data[i]['cont']+"</td>"+
                                    "<td>"+porc+"%</td>"+
                                "</tr>";
                        $('#tbl-soli').append(fila);
                    }
                }

                if (cons==0)
                {
                    var porc_es = parseFloat(total_es*100/total);
                    porc_es = number_format( porc_es, 2, ',', '.' );
                    $("#totales-"+es).html("<strong>"+total_es+"</strong>");
                    $("#porces-"+es).html("<strong>"+porc_es+"</strong>");
                }
                var fila="<tr class='danger'>"+
                            "<td>&nbsp;</td>"+
                            "<td>TOTAL</td>"+
                            "<td>"+total+"</td>"+
                            "<td>100,00%</td>"+
                        "</tr>";
                $('#tbl-soli').append(fila);
            }
            else{
                var fila="<tr>"+
                                "<td colspan='12' class='text-center text-danger'><strong>No existen resultado para el mes, año y filtro especificado</strong></td>"+
                            "</tr>";
                $('#tbl-soli').append(fila);
            }
        }
    });
}

function traerCiudades(est, opt){

    $.ajax( {
        type : 'POST',
        url : "../../controladores/trans/tViatico.php",
        data :  { accion: "listarCiudades",estado:est,activa:1},
        error : function(xhr, ajaxOptions, thrownError) {
            alert("ERROR");
        },
        success : function(data) {
            var data = eval("(" + data + ")");
            if(data){

                   $('#'+opt).html("<option value='0'>Seleccione</option>");
                   //$('#ciu_des').html("<option value='0'>Seleccione</option>");
                    for(var i=0; i<data.length; i++){
                        var option="<option value='"+data[i]['id_ciu']+"'>"+data[i]['nombre_ciu']+"</option>";
                        $('#'+opt).append(option);
                        //$('#ciu_des').append(option);
                    }
            }
            else{
                //$("#error_ciu").show().fadeOut(4000);
                //$("#ciu_nue").val('');
            }
        }
    });
}

function busquedaAvanzasaVia(){

    var mes = $("#mes").val();
    var anio = $("#anio").val();
    var filtro = $("#filtro").val();
    var cedula_em = $("#cedu").val();
    var fedesde = $("#fedesde").val();
    var fehasta = $("#fehasta").val();
    var esta_ori = $("#esta_ori").val();
    var ciu_ori = $("#ciu_ori").val();
    var esta_des = $("#esta_des").val();
    var ciu_des = $("#ciu_des").val();

    if ($.trim(mes) == 0 || $.trim(anio) == '') {
        alert("ERROR, Existen campos vacios");
        return;
    }

    $.ajax( {
        type : 'POST',
        url : "../../controladores/trans/tViatico.php",
        data :  { accion: "busquedaAvanzasaVia", mes:mes,anio:anio, filtro:filtro,cedula_em:cedula_em,
        fedesde:fedesde,fehasta:fehasta,esta_ori:esta_ori,ciu_ori:ciu_ori,esta_des:esta_des,ciu_des:ciu_des},
        error : function(xhr, ajaxOptions, thrownError) {
            alert("ERROR busquedaAvanzasaVia");
        },
        success : function(data) {
            var data = eval("(" + data + ")");
            $('#tbl-soli').html("");
             var state = '';
            if(data){
                console.log(data);
                for(var i=0; i<data.length; i++){

                    if (data[i]['estado_soli'] == 0) {
                        state = 'Solicitud Recien Creada';
                    }else if(data[i]['estado_soli'] == 1){
                        state = 'Solicitudes aprobadas sin cálculo de gastos';
                    }else if(data[i]['estado_soli'] == 2){
                        state = 'Solicitudes con gastos aún no aprobados';
                    }else if(data[i]['estado_soli'] == 3){
                        state = 'Solicitudes con gastos aprobados';
                    }else if(data[i]['estado_soli'] == 3){
                        state = 'Solicitudes pagadas';
                    }else{
                        state = 'Solicitudes canceladas';
                    }
                    var fila="<tr ondblclick='mostrarPlanilla("+data[i]['id_soli']+")'>"+
                                "<td>"+(i+1)+"</td>"+
                                "<td>"+completarCodigoCeros(data[i]['num_soli'],10)+"</td>"+
                                "<td>"+data[i]['cedula_per']+"</td>"+
                                "<td>"+data[i]['nombre_per']+"</td>"+
                                "<td>"+data[i]['motivovia_soli']+"</td>"+
                                "<td>"+data[i]['lugardes_soli']+"</td>"+
                                "<td>"+data[i]['fe_creado']+"</td>"+
                                "<td>"+data[i]['fechades_soli']+"</td>"+
                                "<td>"+data[i]['fechahas_soli']+"</td>"+
                                 "<td>"+state+"</td>"+
                                 "<td onclick='mostrarPlanilla("+data[i]['id_soli']+")'>"+"<span class='glyphicon glyphicon-file'></span>"+"</td>"+
                                 "<td onclick='mostrarPlanillaGastos("+data[i]['id_soli']+")'>"+'<span class="glyphicon glyphicon-th-list"></span>'+"</td>"+
                            "</tr>";
                $('#tbl-soli').append(fila);

                }
            }
            else{
                var fila="<tr>"+
                                "<td colspan='10' class='text-center text-danger'><strong>No existen resultado para el mes, año y filtro especificado</strong></td>"+
                            "</tr>";
                $('#tbl-soli').append(fila);
            }
        }
    });
}

function mostrarReporteZona(){

    var filtro = $("#filtro").val();
    var fedesde = $("#fedesde").val();
    var fehasta = $("#fehasta").val();
    var estades = $("#esta_des").val();
    var ciudes = $("#ciu_des").val();
    var cons   = $("#tip").val();

    if ($.trim(cons) == '-') {
       alert("ERROR... Debe elegir un tipo de busqueda para realizar un reporte");
       return;
    }

    window.open('rpt_list_zona.php?filtro='+filtro+'&fedesde='+fedesde+'&fehasta='+fehasta+'&estades='+estades+'&ciudes='+ciudes+'&cons='+cons,'_blank');

}

function cargarParti(select, tipo){

    $.ajax( {
        type : 'POST',
        url : "../../controladores/trans/tPresupuesto.php",
        data :  { accion: "cargarParti"},
        error : function(xhr, ajaxOptions, thrownError) {
            alert("Ups... Algo esta mal :(");
        },
        success : function(data) {
            var data = JSON.parse(data); /*eval("(" + data + ")");*/
            if(data){
                if (tipo == 1) {
                    $('#'+select).html("<option value='0'>Todas las Partidas</option>");
                }else{
                    $('#'+select).html("<option value='0'>Seleccione</option>");
                }
                
                for(var i=0; i<data.length; i++){
                    var option="<option value='"+data[i]['id_partida']+"'>"+data[i]['cuenta_par'] +' - '+data[i]['descripcion_par']+"</option>";
                    $('#'+select).append(option);
                }
            }
        }
    });
}

function listarDispon(){

    var partida = $("#impu_presu").val();

    $.ajax( {
        type : 'POST',
        url : "../../controladores/trans/tPresupuesto.php",
        data :  { accion: "listarDispon", partida:partida},
        error : function(xhr, ajaxOptions, thrownError) {
            alert("ERROR listarDispon");
        },
        success : function(data) {
            var data = eval("(" + data + ")");
            $('#tbl_dispon').html("");
             var state = '';
            if(data){
                console.log(data);
                for(var i=0; i<data.length; i++){

                    var fila="<tr>"+
                                "<td>"+(i+1)+"</td>"+
                                 "<td>"+data[i]['cuenta_par']+"</td>"+
                                "<td>"+data[i]['descripcion_par']+"</td>"+
                                "<td style='text-align:right;'>"+number_format(data[i]['disponible'],2,',','.')+"</td>"+
                            "</tr>";
                $('#tbl_dispon').append(fila);

                }
            }
            else{
                var fila="<tr>"+
                                "<td colspan='10' class='text-center text-danger'><strong>No existen resultado para el mes, año y filtro especificado</strong></td>"+
                            "</tr>";
                $('#tbl_dispon').append(fila);
            }
        }
    });
}

function listarMovimientoPartida(){

    var partida  = $("#impu_presu").val();
    var fechades = $("#fedesde").val();
    var fechahas = $("#fehasta").val();

    if (partida == 0) {
        alert("ERROR, debe elegir una partida");
        return;
    }

    $.ajax( {
        type : 'POST',
        url : "../../controladores/trans/tPresupuesto.php",
        data :  { accion: "listarMovimientoPartida", partida:partida,fechades:fechades,fechahas:fechahas},
        error : function(xhr, ajaxOptions, thrownError) {
            alert("ERROR listarMovimientoPartida");
        },
        success : function(data) {
            var data = eval("(" + data + ")");
            $('#tbl_movi_part').html("");
            if(data){
                console.log(data);
                for(var i=0; i<data.length; i++){

                    var modo = data[i]['modo_movi'];

                    if (modo == 0) {
                        modo = 'Cheque';
                    }else if(modo == 1){
                        modo = 'Transferencia';
                    }else if(modo == 2){
                        modo = 'Depósito';
                    }else
                        modo = 'Traspaso';

                    var fila="<tr>"+
                                "<td>"+(i+1)+"</td>"+
                                "<td>"+data[i]['fecha_movi']+"</td>"+
                                "<td>"+data[i]['descripcion_movi']+"</td>"+
                                "<td>"+modo+"</td>"+
                                "<td>"+data[i]['num_referencia']+"</td>"+
                                "<td style='text-align:right;'>"+number_format(data[i]['monto_movi'],2,',','.')+"</td>"+
                            "</tr>";
                $('#tbl_movi_part').append(fila);

                }
            }
            else{
                var fila="<tr>"+
                                "<td colspan='10' class='text-center text-danger'><strong>No existen resultado para el mes, año y filtro especificado</strong></td>"+
                            "</tr>";
                $('#tbl_movi_part').append(fila);
            }
        }
    });
}

function listarSoliStatus(){
    
    var filtro  = $("#filtro").val();
    var fedesde = $("#fedesde").val();
    var fehasta = $("#fehasta").val();

    /*if ($.trim(filtro) == '-' || $.trim(fedesde) == '' || $.trim(fehasta) == '') {
        alert("ERROR... Existen campos vacios");
        return;
    }*/

    /*fedesde.split('-');
    fehasta.split('-');

    if (fehasta[0] < fedesde[0] || fehasta[1] <= fedesde[1] || fehasta[2] <= fedesde[2]) {
        alert("ERROR... Valores incorrectos en la fecha hasta, los valores deben ser mayor que la fecha desde");
        return;
    }*/

    $.ajax( {
        type : 'POST',
        url : "../../controladores/trans/tViatico.php",
        data :  { accion: "listarSoliStatus", filtro:filtro,fedesde:fedesde,fehasta:fehasta},
        error : function(xhr, ajaxOptions, thrownError) {
            alert("ERROR listarSoliStatus");
        },
        success : function(data) {
            var data = eval("(" + data + ")");
            $('#tbl_sta').html("");
            if(data){
                console.log(data);
                var state = '';
                for(var i=0; i<data.length; i++){

                    if (data[i]['estado_soli'] == 0) {
                        state = 'Solicitud Recien Creada';
                    }else if(data[i]['estado_soli'] == 1){
                        state = 'Solicitudes aprobadas sin cálculo de gastos';
                    }else if(data[i]['estado_soli'] == 2){
                        state = 'Solicitudes con gastos aún no aprobados';
                    }else if(data[i]['estado_soli'] == 3){
                        state = 'Solicitudes con gastos aprobados';
                    }else if(data[i]['estado_soli'] == 4){
                        state = 'Solicitudes pagadas';
                    }else{
                        state = 'Solicitudes canceladas';
                    }

                  var nom = data[i]['nombre_per'].split(' ');
                  var ape = data[i]['apellido_per'].split(' ');

                  var fullname = nom[0]+' '+ape[0];

                    var fila="<tr>"+
                                "<td>"+(i+1)+"</td>"+
                                "<td>"+completarCodigoCeros(data[i]['num_soli'],10)+"</td>"+
                                "<td>"+data[i]['cedula_per']+"</td>"+
                                "<td>"+fullname+"</td>"+
                                "<td>"+data[i]['motivovia_soli']+"</td>"+
                                "<td>"+data[i]['lugardes_soli']+"</td>"+
                                "<td>"+data[i]['fe_creado']+"</td>"+
                                "<td>"+data[i]['fechades_soli']+"</td>"+
                                "<td>"+data[i]['fechahas_soli']+"</td>"+
                                "<td>"+state+"</td>"+
                            "</tr>";
                $('#tbl_sta').append(fila);

                }
            }
            else{
                var fila="<tr>"+
                                "<td colspan='10' class='text-center text-danger'><strong>No existen solicitudes</strong></td>"+
                            "</tr>";
                $('#tbl_sta').append(fila);
            }
        }
    });
}

function ConsolidadoSoliPerso(){

    var filtro  = $("#filtro").val();
    var fedesde = $("#fedesde").val();
    var fehasta = $("#fehasta").val();

    $.ajax( {
        type : 'POST',
        url : "../../controladores/trans/tViatico.php",
        data :  { accion: "ConsolidadoSoliPerso", filtro:filtro,fedesde:fedesde,fehasta:fehasta},
        error : function(xhr, ajaxOptions, thrownError) {
            alert("ERROR ConsolidadoSoliPerso");
        },
        success : function(data) {
            var data = eval("(" + data + ")");
            $('#tbl_emp').html("");
            if(data){
                console.log(data);

                for(var i=0; i<data.length; i++){

                  var nom = data[i]['nombre_per'].split(' ');
                  var ape = data[i]['apellido_per'].split(' ');

                  var fullname = nom[0]+' '+ape[0];

                    var fila="<tr>"+
                                "<td>"+(i+1)+"</td>"+
                                "<td>"+fullname+"</td>"+
                                "<td>"+data[i]['cedula_per']+"</td>"+
                                "<td>"+data[i]['cont']+"</td>"+
                            "</tr>";
                $('#tbl_emp').append(fila);

                }
            }
            else{
                var fila="<tr>"+
                                "<td colspan='5' class='text-center text-danger'><strong>No existen solicitudes</strong></td>"+
                            "</tr>";
                $('#tbl_emp').append(fila);
            }
        }
    });

}

function consolidadoSoliStatus(){

    var filtro  = $("#filtro").val();
    var fedesde = $("#fedesde").val();
    var fehasta = $("#fehasta").val();

    $.ajax( {
        type : 'POST',
        url : "../../controladores/trans/tViatico.php",
        data :  { accion: "consolidadoSoliStatus", filtro:filtro,fedesde:fedesde,fehasta:fehasta},
        error : function(xhr, ajaxOptions, thrownError) {
            alert("ERROR consolidadoSoliStatus");
        },
        success : function(data) {
            var data = eval("(" + data + ")");
            $('#tbl_emp').html("");
            var state = "";
            if(data){
                console.log(data);

                for(var i=0; i<data.length; i++){

                     if (data[i]['estado_soli'] == 0) {
                        state = 'Solicitud Recien Creada';
                    }else if(data[i]['estado_soli'] == 1){
                        state = 'Solicitudes aprobadas sin cálculo de gastos';
                    }else if(data[i]['estado_soli'] == 2){
                        state = 'Solicitudes con gastos aún no aprobados';
                    }else if(data[i]['estado_soli'] == 3){
                        state = 'Solicitudes con gastos aprobados';
                    }else if(data[i]['estado_soli'] == 4){
                        state = 'Solicitudes pagadas';
                    }else{
                        state = 'Solicitudes canceladas';
                    }

                    var fila="<tr>"+
                                "<td>"+(i+1)+"</td>"+
                                "<td>"+state+"</td>"+
                                "<td>"+data[i]['cont']+"</td>"+
                            "</tr>";
                $('#tbl_emp').append(fila);

                }
            }
            else{
                var fila="<tr>"+
                                "<td colspan='3' class='text-center text-danger'><strong>No existen solicitudes</strong></td>"+
                            "</tr>";
                $('#tbl_emp').append(fila);
            }
        }
    });

}

function MostrarReporteDispon(){

    var partida = $("#impu_presu").val();
    window.open('rpt_Dispon.php?partida='+partida);
}

function mostrarReporteMoviPar(){

    var partida = $("#impu_presu").val();
    var fedesde = $("#fedesde").val();
    var fehasta = $("#fehasta").val();

     if ($.trim(partida) == 0) {
        alert("ERROR... Debe seleccionar una partida");
        return;
    }

    window.open('rpt_MoviPar.php?partida='+partida+'&fedesde='+fedesde+'&fehasta='+fehasta);
}

function mostrarReporSoliStatus(){
    
    var filtro  = $("#filtro").val();
    var fedesde = $("#fedesde").val();
    var fehasta = $("#fehasta").val();

    window.open('rpt_ListStatus.php?filtro='+filtro+'&fedesde='+fedesde+'&fehasta='+fehasta);
}

function mostrarReporteEm(){

    var filtro  = $("#filtro").val();
    var fedesde = $("#fedesde").val();
    var fehasta = $("#fehasta").val();

    window.open('rpt_Report_Em.php?filtro='+filtro+'&fedesde='+fedesde+'&fehasta='+fehasta);
}

function mostrarReporteConsoliStatus(){

    var filtro  = $("#filtro").val();
    var fedesde = $("#fedesde").val();
    var fehasta = $("#fehasta").val();

    window.open('rpt_consoliStatus.php?filtro='+filtro+'&fedesde='+fedesde+'&fehasta='+fehasta);
}

/*function reporteDeGastos(){

    window.open('rptRelacionGastos.php.php?id_via='+id_via);
}*/