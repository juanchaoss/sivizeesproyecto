function verificarUsuario() {
	//var aux = $("#usuario_aux").val();
	var cedula     = $("#ci").val();
	var usuario    = $("#usuario").val();
	if ($.trim(cedula) == '') {
			alert("Ingrese un numero de cédula");
			return;
	}

	if ($.trim(usuario) == '') {
		alert("Ingrese un usuario");
	   return;
	}

	var hidusu = $("#hidusu").val();

		if (!hidusu) {
			hidusu = 0;
		}

	$.ajax( {
			type : 'POST',
			url : "../controladores/trans/tUsuario.php",
			data :	{ accion: "verificarUsuario", cedula: cedula, usuario:usuario, hidusu:hidusu },
			error : function(xhr, ajaxOptions, thrownError) {
				alert("Ups... Algo esta mal :(");
			},
			beforeSend: function(data){
               // $("#prueba").html("<p align='center'><img src='img/Preloader_10.gif'></img></p>");
            },
			success : function(data) {
				console.log(data);
				var data = eval("(" + data + ")");
				if(data==1){
					registrarUsuario();
				}else{
					alert(data);
				}
			}
	});

}
//funcion temporal
function empleadoExis(){
	var ced = $("#ci").val();
	/*alert(ced);
	alert("Llego a la funcion a traves del evento onBlur "+ced);*/
	if ($.trim(ced) == '') {
		$("#ced").val('');
		$("#nom").val('');
		$("#ap").val('');
		return;
	}
	$.ajax( {
			type : 'POST',
			url : "../controladores/trans/tUsuario.php",
			data :	{ accion: "empleadoExis", ced: ced},
			error : function(xhr, ajaxOptions, thrownError) {
				alert("Ups... Algo esta mal :(");
			},
			beforeSend: function(data){
               // $("#prueba").html("<p align='center'><img src='img/Preloader_10.gif'></img></p>");
            },
			success : function(data) {
				console.log(data);
				var data = eval("(" + data + ")");
				if(data){
					$("#ced").val(data[0]['cedula_per']);
					$("#nom").val(data[0]['nombre_per']);
					$('#ap').val(data[0]['apellido_per']);
				}else{
					alert("ERROR.. No se puede crear el usuario, asegurese que el usuario que vaya a crear se un empleado");
					$("#ci").val('');
					$("#ced").val('');
					$("#nom").val('');
					$('#ap').val('');
					return;
				}
			}
	});
}

function holaId(){
	var hidusu = $("#hidusu").val();
	alert(hidusu);
}

function registrarUsuario() {
	var hidusu = $("#hidusu").val();
	var aux = $("#usuario_aux").val();

	if (hidusu == 0){

		var usuario   = $.trim($("#usuario").val());
		var clave     = $("#clave").val();
		var tipo      = $("#tipo").val();
		var clave_aux = $("#aux_clave").val();
		var aprobador = $("#apro").val();
		var cedula    = $("#ci").val();


		if (clave != clave_aux) {
			alert("Las contraseñas introducidas no coinciden");
			return;
		}

		if (usuario == '' || $.trim(clave) == '' || $.trim(tipo) == '0' ||
			$.trim(aprobador) == '0' ) {
			alert("Existen Campos vacios");
			return;
		}
		if (confirm("¿Estas seguro de registar el asuario: "+usuario+"?") == 1) {
				$.ajax( {
					type : 'POST',
					url : "../controladores/trans/tUsuario.php",
					data :	{ accion: "registrarUsuario", usuario: usuario,
					                                        clave: clave,
					                                        tipo:tipo,
					                                        aprobador:aprobador,
					                                        cedula:cedula
					                                        },
					error : function(xhr, ajaxOptions, thrownError) {
						alert("Ups... Algo esta mal :(");
					},
					beforeSend: function(data){
		               // $("#prueba").html("<p align='center'><img src='img/Preloader_10.gif'></img></p>");
		            },
					success : function(data) {
						console.log(data);
						var data = eval("(" + data + ")");
						//console.log(data);
						if(data){
							alert("El usuario fue registrado exitosamente");
							limpiarFormUsu();
						}else{
							alert("ERROR, ha ocurrido un error al registrar el usuario");
						}
					}
		        });
		    }

	}if(hidusu > 0){
		actualizarDatosUsuario(aux);
	}
}
function bloquearBotonesUsu(){
	$('#btn-eli').attr("disabled", true);
   	$('#btn-modi').attr("disabled", true);
   	//$('#btn-con').attr("disabled", true);
}
function activarBotonesUsu(){
	$("#btn-eli").removeAttr('disabled');
	$("#btn-modi").removeAttr('disabled');
	$("#btn-con").removeAttr('disabled');
}
function bloquearFormUsu(){
   $('#ci').attr("disabled", true);
   $('#usuario').attr("disabled", true);
   $('#clave').attr("disabled", true);
   $('#tipo').attr("disabled", true);
   $('#apro').attr("disabled", true);
   $('#aux_clave').attr("disabled", true);
   $("#btn_ac").attr("disabled", true);
   $("#btn_eli").attr("disabled", true);
   $("#btn_bo").attr("disabled", true);
}
function desbloquearFormUsu(){
	//$("#ci").removeAttr('disabled');
	$("#usuario").removeAttr('disabled');
	//$("#clave").removeAttr('disabled');
	$("#tipo").removeAttr('disabled');
	$("#apro").removeAttr('disabled');
	//$("#aux_clave").removeAttr('disabled');
	$("#btn_ac").removeAttr('disabled');
	$("#btn_eli").removeAttr('disabled');
	$("#btn_bo").removeAttr('disabled');
}
function limpiarFormUsu(){
	$("#ci").val('');
	$("#usuario").val('');
	$("#clave").val('');
	$("#tipo").val('0');
	$("#apro").val('0');
	$("#aux_clave").val('');
	$("#nom").val('0');
	$("#ap").val('');
}


function buscarUsuario(usu_aux){
	//var usu_aux = $("#usuario_aux").val();
	//alert(usu_aux);
		if ($.trim(usu_aux) == '') {
			alert("ERROR, debe introducir un usuario");
			return
		}

		//limpiar y bloquear formulario
		limpiarFormUsu();
		bloquearFormUsu();

		$.ajax( {
			type : 'POST',
			url : "../controladores/trans/tUsuario.php",
			data :	{ accion: "buscarUsuario", usu_aux:usu_aux},
			error : function(xhr, ajaxOptions, thrownError) {
				alert("Ups... Algo esta mal :(");
			},
			success : function(data) {
				console.log(data);
				var data = eval("(" + data + ")");
				if(data){
					$("#ci").val(data[0]['cedula_per']);
					$("#nom").val(data[0]['nombre_per']);
					$("#ap").val(data[0]['apellido_per']);
					$("#usuario").val(data[0]['usuario']);
					$("#hidusu").val(data[0]['id_usu']);
					$("#hidusuario").val(data[0]['id_usu']);
					$('#tipo').val(data[0]['tipo']);
	                $('#apro').val(data[0]['aprob']);

	                $('#clave').attr("disabled", true);
   					$('#aux_clave').attr("disabled", true);
				  	bloquearFormUsu();
				  	activarBotonesUsu();
				} else{
					//$('#msg').html("<b><p align='center' style='font-size:1em; color: red;'>No se pudo encontrar al usuario</p><b>");
					alert("El usuario: "+usu_aux+", no pudo ser encontrado");
					location.reload();
					//alert(data);var usuario   = $.trim($("#usuario").val());
				}
			}
	});

}
function actualizarDatosUsuario(user){
	var aux = user;
	var usuario_aux = $("usuario_aux").val();
	var usuario   = $.trim($("#usuario").val());
	var tipo      = $("#tipo").val();
	var aprobador = $("#apro").val();
	var hidusu = $("#hidusu").val();

	if ($.trim(aux) == '') {
		aux = usuario;
	}

	//alert(hidusu);

	if ($.trim(usuario) == '' || tipo == 0 || aprobador == 0) {
		alert("ERROR, debe llenar todos los campos");
		return;
	}

	if (confirm("¿Estas seguro de actualizar los datos del usuario: "+aux+"?") == 1) {
			$.ajax( {
				type : 'POST',
				url : "../controladores/trans/tUsuario.php",
				data :	{ accion: "actualizarDatosUsuario", usuario: usuario,
					                                        tipo:tipo,
					                                        aprobador:aprobador,
					                                        hidusu:hidusu
				                                           },
				error : function(xhr, ajaxOptions, thrownError) {
					alert("Ups... Algo esta mal :(");
				},
				beforeSend: function(data){
	               // $("#prueba").html("<p align='center'><img src='img/Preloader_10.gif'></img></p>");
	            },
				success : function(data) {
					console.log(data);
					var data = eval("(" + data + ")");
					if(data != false){
						alert("El usuario fue actualizado exitosamente");
						location.reload();
						if (data == 'u001') {
							location.reload();
						}
					}
				}
		});
	}
}

function cambiarClave(){
	var usu      = $("#usu").val();
	var passvie  = $("#passvie").val();
	var passnue  = $("#passnue").val();
	var passnure = $("#passnure").val();

	if ($.trim(usu) == '' || $.trim(passvie) == '' || $.trim(passnue) == '' || $.trim(passnure) == '') {
		alert("ERROR, Existen campos vacios");
		return;
	}

	if (passnue != passnure) {
		alert("ERROR, Las contraseñas introducidas no coinciden");
		return
	}

	if (confirm("¿Estas seguro de cambiar la contraseña del usuario: "+usu+"?") == 1) {
			$.ajax( {
				type : 'POST',
				url : "../controladores/trans/tUsuario.php",
				data :	{ accion: "cambiarClave", usu: usu,
		                                        passnue:passnue,
		                                        passvie:passvie,
				                                                 },
				error : function(xhr, ajaxOptions, thrownError) {
					alert("Ups... Algo esta mal :(");
				},
				beforeSend: function(data){
	               // $("#prueba").html("<p align='center'><img src='img/Preloader_10.gif'></img></p>");
	            },
				success : function(data) {
					console.log(data);
					var data = eval("(" + data + ")");
					if (data) {
							//location.reload();
							alert("Se ha realizado el cambio de contraseña al usuario: "+usu);
							location.reload();
					}else{
						alert("ERROR, No se pudo realizar el cambio de contraseña, es posible que el usuario no exista o la clave no sea la correcta");
					}
				}
		});
	}
}

function eliminarUsuario(){
	var usuario_aux = $("#usuario_aux").val();
	var hidusu  = $("#hidusu").val();

	if (confirm("¿Esta seguro de eliminar al usuario: "+usuario_aux+"?")) {
		$.ajax( {
				type : 'POST',
				url : "../controladores/trans/tUsuario.php",
				data :	{ accion: "eliminarUsuario", hidusu:hidusu},
				error : function(xhr, ajaxOptions, thrownError) {
					alert("Ups... Algo esta mal :(");
				},
				beforeSend: function(data){
	               // $("#prueba").html("<p align='center'><img src='img/Preloader_10.gif'></img></p>");
	            },
				success : function(data) {
					console.log(data);
					var data = eval("(" + data + ")");
					//console.log(data);
					if(data){
						alert("El usuario fue eliminado exitosamente!");
						location.reload();
						//$("#usuSesion").html(data);
					}
				}
		});
	}
}
function listarUsuarios(){
	$.ajax( {
		type : 'POST',
		url : "../controladores/trans/tUsuario.php",
		data :	{ accion: "listarUsuarios"},
		error : function(xhr, ajaxOptions, thrownError) {
			alert("Ups... Algo esta mal :(");
		},
		success : function(data) {
			var data = eval("(" + data + ")");
			if(data){
				$('#tbl-usu').html("");
				var fila="<tr>"+
								"<th>#</th>"+
								"<th>Usuario</th>"+
								"<th>Tipo</th>"+
								"<th>Aprobador</th>"+
								"<th>Nivel de Aprobación</th>"+
							"</tr>";
				$('#tbl-usu').append(fila);

				for(var i=0; i<data.length; i++){

					var tipo=data[i]['tipo'];
					var aprob=data[i]['aprob'];

					if (tipo == 1) {
						tipo = 'Administador-Sistema';
					}
					else if(tipo == 2){
						tipo = 'Administrador';
					}else{
						tipo = 'Administrativo';
					}

					if (aprob > 1) {
						aprob = 'Sí';
					}
					else
						aprob = 'No';

					if (data[i]['aprob'] == 2) {
						var nivel = 'Administrador - Div. Administración y Servicio';
					}else if(data[i]['aprob'] == 3){
						nivel = 'Jefe de Zona';
					}else if(data[i]['aprob'] == 4){
						nivel = 'Aprob. Div. Planificación y P.';
					}else if(data[i]['aprob'] == 5){
						nivel = 'Aprob. Coord. Verificación y Control';
					}else if(data[i]['aprob'] == 6){
						nivel = 'Aprob. Contraloria Interna';
					}else if(data[i]['aprob'] == 1){
						nivel = 'Usuario sin privilegio de aprobación';
					}

					var fila="<tr id='user_"+data[i]['id_usu']+"'>"+
								"<td>"+(i+1)+"</td>"+
								"<td>"+data[i]['usuario']+"</td>"+
								"<td>"+tipo+"</td>"+
								"<td>"+aprob+"</td>"+
								"<td>"+nivel+"</td>"+
							"</tr>";
					$('#tbl-usu').append(fila);

					$('#user_'+data[i]['id_usu']).click({usuario:data[i]['usuario']},function(evento){
						buscarUsuario(evento.data.usuario);
						$("#myModal1").modal('hide');
					});


				}
			}
		}
	});
}