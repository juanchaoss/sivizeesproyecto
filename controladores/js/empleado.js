function cargarEstados() {

	$.ajax( {
			type  : 'POST',
			url   : "../controladores/trans/tEmpleado.php",
			data  :	{ accion: "cargarEstados"},
			error : function(xhr, ajaxOptions, thrownError) {
				alert("ERROR");
			},
			success : function(data) {
				var data = eval("(" + data + ")");
				if(data)
				{
					$('#selEstado').html("<option value='0'>Seleccione</option>");
					for(var i=0; i<data.length; i++){
						var option="<option value='"+data[i]['id_es']+"'>"+data[i]['nombre_es']+"</option>";
						$('#selEstado').append(option);
					}
				}
			}
		});
	}

function cargarMunicipios(mi_mun) {

	var id_estado=$('#selEstado').val();
	if(!mi_mun){
		mi_mun=0;
	}
	if(id_estado!=0){
	$.ajax( {
			type  : 'POST',
			async: false,
			url   : "../controladores/trans/tEmpleado.php",
			data  :	{ accion: "cargarMunicipios",id_es:id_estado},
			error : function(xhr, ajaxOptions, thrownError) {
				alert("ERROR");
			},
			success : function(data) {
				var data = eval("(" + data + ")");
				if(data)
				{
					$('#selMunicipio').html("<option value='0'>Seleccione</option>");
					for(var i=0; i<data.length; i++){
						var opcion="";
						if(mi_mun==data[i]['id_mun']){
							opcion="selected";
						}
						var option="<option "+opcion+" value='"+data[i]['id_mun']+"'>"+data[i]['nombre_mun']+"</option>";
						$('#selMunicipio').append(option);
					}
				}
			}
		});
	}else{
		$('#selMunicipio').html("<option value='0'>Seleccione</option>");
		$('#selParroquia').html("<option value='0'>Seleccione</option>");
		$('#selCiudad').html("<option value='0'>Seleccione</option>");
	}
}

function cargarParroquias(mi_parro) {
	var id_municipio=$('#selMunicipio').val();
	if(!mi_parro){
		mi_parro=0;
	}
	if(id_municipio!=0){
	$.ajax( {
			type  : 'POST',
			async: false,
			url   : "../controladores/trans/tEmpleado.php",
			data  :	{ accion: "cargarParroquias",id_mun:id_municipio},
			error : function(xhr, ajaxOptions, thrownError) {
				alert("ERROR");
			},
			success : function(data) {
				var data = eval("(" + data + ")");
				if(data)
				{
					$('#selParroquia').html("<option value='0'>Seleccione</option>");
					for(var i=0; i<data.length; i++){
						var opcion="";
						if(mi_parro==data[i]['id_parro']){
							opcion="selected";
						}
						var option="<option "+opcion+" value='"+data[i]['id_parro']+"'>"+data[i]['nombre_parro']+"</option>";
						$('#selParroquia').append(option);
					}
				}
			}
		});
	}else{
		$('#selParroquia').html("<option value='0'>Seleccione</option>");
		$('#selCiudad').html("<option value='0'>Seleccione</option>");
	}
}

function cargarCiudades(mi_ciu) {

	var id_estado=$('#selEstado').val();
	if(!mi_ciu){
		mi_ciu=0;
	}
	if(id_estado!=0){
	$.ajax( {
			type  : 'POST',
			url   : "../controladores/trans/tEmpleado.php",	
			data  :	{ accion: "cargarCiudades",id_es:id_estado},
			error : function(xhr, ajaxOptions, thrownError) {
				alert("ERROR");
			},
			success : function(data) {
				var data = eval("(" + data + ")");
				if(data)
				{
					$('#selCiudad').html("<option value='0'>Seleccione</option>");
					for(var i=0; i<data.length; i++){
						var opcion="";
						if(mi_ciu==data[i]['id_ciu']){
							opcion="selected";
						}
						var option="<option "+opcion+" value='"+data[i]['id_ciu']+"'>"+data[i]['nombre_ciu']+"</option>";
						$('#selCiudad').append(option);
					}
				}
			}
		});
	}
}
function verificarEmpleado() {

	var cedula     = $("#ced_em").val();
	if ($.trim(cedula) == '') {
			alert("Ingrese un numero de cédula");
			return;
	}

	$.ajax( {
			type  : 'POST',
			url   : "../controladores/trans/tEmpleado.php",
			data  :	{ accion: "verificarEmpleado", cedula: cedula },
			error : function(xhr, ajaxOptions, thrownError) {
				alert("Ups... Algo esta mal :(");
			},
			beforeSend: function(data){
               // $("#prueba").html("<p align='center'><img src='img/Preloader_10.gif'></img></p>");
            },
			success : function(data) {
				console.log(data);
				var data = eval("(" + data + ")");
				//console.log(data);
				if(data){
					registrarEmpleado();
				}else{
					alert('El empleado con la cedula '+cedula+' existe en el sistema.');
				}
			}
	});

}

function registrarEmpleado() {

	//datos personales
	var cedula     = $("#ced_em").val();
	var nombres    = $("#nom_em").val();
	var apellidos  = $("#ape_em").val();
	var fecha_na   = $("#fe_em").val();
	var correo     = $("#correo_em").val();
	var celular    = $("#celu_em").val();
	var numero     = $("#num_em").val();
    var sexo       = "M";
    if($("#sexoF").is(':checked')){
    	sexo="F";
    }

	//datos de ubicacion
	var direccion  = $("#direc_em").val();
	var estado     = $("#selEstado").val();
	var municipio  = $("#selMunicipio").val();
	var parroquia  = $("#selParroquia").val();
	var ciudad     = $("#selCiudad").val();

	//datos de empleo
	var fecha_in   = $("#fe_em_em").val();
	var condicion  = $("#condi_em").val();
	var tipo_em    = $("#tipo_em").val();
	var cargo      = $("#cargo_em").val();

		if ($.trim(nombres) == '' || $.trim(apellidos) == '' || $.trim(fecha_na) == '' ||
			$.trim(direccion) == '' || $.trim(estado) == '0' || $.trim(municipio) == '0' ||
			$.trim(parroquia) == '0' || $.trim(ciudad) == '0' || $.trim(fecha_in) == '' || $.trim(condicion) == '-' ||
			$.trim(tipo_em) == '-' || $.trim(cargo) == '') {
			alert("Existen Campos vacios o tienen espacios");
			return;
		}
		$.ajax( {
			type : 'POST',
			url : "../controladores/trans/tEmpleado.php",
			data :	{ accion: "registrarEmpleado", cedula: cedula,
			                                        nombres: nombres,
			                                        apellidos:apellidos,
			                                        fecha_na:fecha_na,
                                                    celular:celular,
                                                    sexo:sexo,
                                                    numero:numero,
			                                        correo:correo,
			                                        direccion:direccion,
			                                        estado:estado,
			                                        municipio:municipio,
			                                        parroquia:parroquia,
			                                        ciudad:ciudad,
			                                        fecha_in:fecha_in,
			                                        condicion:condicion,
			                                        tipo_em:tipo_em,
			                                        cargo:cargo
			                                        },
			error : function(xhr, ajaxOptions, thrownError) {
				alert("Ups... Algo esta mal :(");
			},
			beforeSend: function(data){
               // $("#prueba").html("<p align='center'><img src='img/Preloader_10.gif'></img></p>");
            },
			success   : function(data) {
				console.log(data);
				var data = eval("(" + data + ")");
				//console.log(data);
				if(data){
					alert("El empleado fue registrado exitosamente");
					document.getElementById("form_em").reset();
					$("#progress_em").css("width",'0%');
					$("#progress_em").removeClass('progress-bar-info').removeClass('progress-bar-danger').addClass('progress-bar-info');
					$('#tabs_em a:first').tab('show');
				}
			}
	});
}
function bloquearFormEmpleado(){
    //datos personales
	$("#ced_em").attr("disabled", true);
	$("#nom_em").attr("disabled", true);
	$("#ape_em").attr("disabled", true);
	$("#fe_em").attr("disabled", true);
	$("#correo_em").attr("disabled", true);
	$("#celu_em").attr("disabled", true);
	$("#num_em").attr("disabled", true);
    $("#sexoM").attr("disabled", true);
    $("#sexoF").attr("disabled", true);

	//datos de ubicacion
	$("#direc_em").attr("disabled", true);
	$("#selEstado").attr("disabled", true);
	$("#selMunicipio").attr("disabled", true);
	$("#selParroquia").attr("disabled", true);
	$("#selCiudad").attr("disabled", true);

	//datos de empleo
	$("#fe_em_em").attr("disabled", true);
	$("#condi_em").attr("disabled", true);
	$("#tipo_em").attr("disabled", true);
	$("#cargo_em").attr("disabled", true);

    //botones

    $("#btn_eli").attr("disabled", true);
    $("#btn_ac").attr("disabled", true);
    $("#btn_lim").attr("disabled", true);
}
function limpiarFormEmpleado(){
    //datos personales
    $("#hidempleado").val('');
	$("#hidpersona").val('');
	$("#ced_em").val('');
	$("#nom_em").val('');
	$("#ape_em").val('');
	$("#fe_em").val('');
	$("#correo_em").val('');
	$("#celu_em").val('');
	$("#num_em").val('');
    $("#sexoM").attr('checked',true);

	//datos de ubicacion
	$("#direc_em").val('');
	$("#selEstado").val("0");
	cargarMunicipios();
	cargarParroquias();
	cargarCiudades();

	//datos de empleo
	$("#fe_em_em").val("");
	$("#condi_em").val("-");
	$("#tipo_em").val("-");
	$("#cargo_em").val("");
}
function desbloqueoFormEmpleado(){
	 //datos personales
	$("#ced_em").removeAttr('disabled');
	$("#nom_em").removeAttr('disabled');
	$("#ape_em").removeAttr('disabled');
	$("#fe_em").removeAttr('disabled');
	$("#correo_em").removeAttr('disabled');
	$("#celu_em").removeAttr('disabled');
	$("#num_em").removeAttr('disabled');
    $("#sexoM").removeAttr('disabled');
    $("#sexoF").removeAttr('disabled');

	//datos de ubicacion
	$("#direc_em").removeAttr('disabled');
	$("#selEstado").removeAttr('disabled');
	$("#selMunicipio").removeAttr('disabled');
	$("#selParroquia").removeAttr('disabled');
	$("#selCiudad").removeAttr('disabled');

	//datos de empleo
	$("#fe_em_em").removeAttr('disabled');
	$("#condi_em").removeAttr('disabled');
	$("#tipo_em").removeAttr('disabled');
	$("#cargo_em").removeAttr('disabled');

    //botones

    $("#btn_eli").removeAttr('disabled');
    $("#btn_ac").removeAttr('disabled');
    $("#btn_lim").removeAttr('disabled');
}

function actualizarDatosEmpleado(){
	//datos personales
	var hidempleado= $("#hidempleado").val();
	var hidpersona = $("#hidpersona").val();
	var cedula     = $("#ced_em").val();
	var nombres    = $("#nom_em").val();
	var apellidos  = $("#ape_em").val();
	var fecha_na   = $("#fe_em").val();
	var correo     = $("#correo_em").val();
	var celular    = $("#celu_em").val();
	var numero     = $("#num_em").val();
    var sexo       = "M";
    if($("#sexoF").is(':checked')){
    	sexo="F";
    }

	//datos de ubicacion
	var direccion  = $("#direc_em").val();
	var estado     = $("#selEstado").val();
	var municipio  = $("#selMunicipio").val();
	var parroquia  = $("#selParroquia").val();
	var ciudad     = $("#selCiudad").val();

	//datos de empleo
	var fecha_in   = $("#fe_em_em").val();
	var condicion  = $("#condi_em").val();
	var tipo_em    = $("#tipo_em").val();
	var cargo      = $("#cargo_em").val();

		if ($.trim(nombres) == '' || $.trim(apellidos) == '' || $.trim(fecha_na) == '' ||
			$.trim(direccion) == '' || $.trim(estado) == '0' || $.trim(municipio) == '0' ||
			$.trim(parroquia) == '0' || $.trim(ciudad) == '0' || $.trim(fecha_in) == '' || $.trim(condicion) == '-' ||
			$.trim(tipo_em) == '-' || $.trim(cargo) == '') {
			alert("Existen Campos vacios o tienen espacios");
			return;
		}

		$.ajax( {
			type : 'POST',
			url : "../controladores/trans/tEmpleado.php",
			data :	{ accion: "actualizarDatosEmpleado", cedula: cedula,
			                                        nombres: nombres,
			                                        apellidos:apellidos,
			                                        fecha_na:fecha_na,
                                                    celular:celular,
                                                    sexo:sexo,
                                                    numero:numero,
			                                        correo:correo,
			                                        direccion:direccion,
			                                        estado:estado,
			                                        municipio:municipio,
			                                        parroquia:parroquia,
			                                        ciudad:ciudad,
			                                        fecha_in:fecha_in,
			                                        condicion:condicion,
			                                        tipo_em:tipo_em,
			                                        cargo:cargo,
			                                        hidempleado:hidempleado,
			                                        hidpersona:hidpersona
			                                        },
			error : function(xhr, ajaxOptions, thrownError) {
				alert("Ups... Algo esta mal :(");
			},
			beforeSend: function(data){
               // $("#prueba").html("<p align='center'><img src='img/Preloader_10.gif'></img></p>");
            },
			success   : function(data) {
				console.log(data);
				var data = eval("(" + data + ")");
				//console.log(data);
				if(data){
					alert("El empleado fue actualizado exitosamente");
				}
			}
	});
}

function eliminarDatosEmpleado(){
	//datos personales
	var hidempleado= $("#hidempleado").val();
	var hidpersona = $("#hidpersona").val();


		if ($.trim(hidempleado) == '') {
			alert("Debe buscar el empleado que desee eliminar");
			return;
		}

		$.ajax( {
			type : 'POST',
			url : "../controladores/trans/tEmpleado.php",
			data :	{ accion: "eliminarDatosEmpleado",
			                                        hidempleado:hidempleado,
			                                        hidpersona:hidpersona
			                                        },
			error : function(xhr, ajaxOptions, thrownError) {
				alert("Ups... Algo esta mal :(");
			},
			beforeSend: function(data){
               // $("#prueba").html("<p align='center'><img src='img/Preloader_10.gif'></img></p>");
            },
			success   : function(data) {
				console.log(data);
				var data = eval("(" + data + ")");
				//console.log(data);
				if(data){
					alert("El empleado fue eliminado exitosamente");
				}
			}
	});
}

function habilitarRegistro(){
	//console.log('xx');
	//datos personales
	var cedula     = $("#ced_em").val();
	var nombres    = $("#nom_em").val();
	var apellidos  = $("#ape_em").val();
	var fecha_na   = $("#fe_em").val();

	//datos de ubicacion
	var direccion  = $("#direc_em").val();
	var estado     = $("#selEstado").val();
	var municipio  = $("#selMunicipio").val();
	var parroquia  = $("#selParroquia").val();
	var ciudad     = $("#selCiudad").val();

	//datos de empleo
	var fecha_in   = $("#fe_em_em").val();
	var condicion  = $("#condi_em").val();
	var tipo_em    = $("#tipo_em").val();
	var cargo      = $("#cargo_em").val();

		if ($.trim(nombres) == '' || $.trim(apellidos) == '' || $.trim(fecha_na) == '' ||
			$.trim(direccion) == '' || $.trim(estado) == '0' || $.trim(municipio) == '0' ||
			$.trim(parroquia) == '0' || $.trim(ciudad) == '0' || $.trim(fecha_in) == '' || $.trim(condicion) == '-' ||
			$.trim(tipo_em) == '-' || $.trim(cargo) == '') {
			$("#btnRegistrarEmp").attr("disabled",true);
		}
		else
		{
			$("#btnRegistrarEmp").removeAttr("disabled");
		}
}
function buscarDatosEmpleadoSoli(){
	var ced_aux = $("#ced_aux").val();
	//alert(usu_aux);
		if ($.trim(ced_aux) == '') {
			alert("ERROR, debe introducir un número de cédula");
			return
		}
	$.ajax( {
			type : 'POST',
			url : "../controladores/trans/tEmpleado.php",
			data :	{ accion: "buscarEmpleado", cedula:ced_aux},
			error : function(xhr, ajaxOptions, thrownError) {
				alert("Ups... Algo esta mal :(");
			},
			success : function(data) {
				console.log(data);
				var data = eval("(" + data + ")");
				if(data){
					$("#hidempleado").val(data[0]['id_em']);
	                $("#ced_em").val(data[0]['cedula_per']);
					$("#nom_em").val(data[0]['nombre_per']);
					$("#ape_em").val(data[0]['apellido_per']);
					$("#cargo_em").val(data[0]['cargo']);
					$("#esta").val(data[0]['nombre_es']);
					$("#ciu").val(data[0]['nombre_ciu']);
					$("#direc").val(data[0]['direccion_per']);

					if(data[0]['sexo_per']=="M"){
				    	$("#sexoM").attr('checked',true);
				    }else{
						$("#sexoF").attr('checked',true);
				    }

				} else{
					//$('#msg').html("<b><p align='center' style='font-size:1em; color: red;'>No se pudo encontrar al empleado</p><b>");
					alert("El empleado con la cédula: "+ced_aux+", no existe o no está registrado");
					//alert(data);var usuario   = $.trim($("#usuario").val());
				}
			}
	});
}
function buscarEmpleado(){
	var cedula = $("#lo").val();

		if ($.trim(cedula) == '') {
			alert("ERROR, debe introducir un número de cédula");
			return
		}
		//limpiar y bloquear formulario
		limpiarFormEmpleado();
		bloquearFormEmpleado();

		$.ajax( {
			type : 'POST',
			url : "../controladores/trans/tEmpleado.php",
			data :	{ accion: "buscarEmpleado", cedula:cedula},
			error : function(xhr, ajaxOptions, thrownError) {
				alert("Ups... Algo esta mal :(");
			},
			success : function(data) {
				console.log(data);
				var data = eval("(" + data + ")");
				if(data){
					$("#hidempleado").val(data[0]['id_em']);
					$("#hidpersona").val(data[0]['id_per']);
	                $("#ced_em").val(data[0]['cedula_per']);
					$("#nom_em").val(data[0]['nombre_per']);
					$("#ape_em").val(data[0]['apellido_per']);
					$("#fe_em").val(data[0]['fecha_na_per']);
					$("#correo_em").val(data[0]['correo_per']);
					$("#celu_em").val(data[0]['celular_per']);
					$("#num_em").val(data[0]['telefono_per']);
					if(data[0]['sexo_per']=="M"){
				    	$("#sexoM").attr('checked',true);
				    }else{
						$("#sexoF").attr('checked',true);
				    }

					//datos de ubicacion
					$("#direc_em").val(data[0]['direccion_per']);
					$("#selEstado").val(data[0]['id_es']);
					cargarMunicipios(data[0]['id_mun']);
					cargarParroquias(data[0]['id_parro']);
					cargarCiudades(data[0]['id_ciu'])

					//datos de empleo
					$("#fe_em_em").val(data[0]['fecha_ingre']);
					$("#condi_em").val(data[0]['condi_lab']);
					$("#tipo_em").val(data[0]['tipo']);
					$("#cargo_em").val(data[0]['cargo']);
				  	desbloqueoFormEmpleado();
				} else{
					//$('#msg').html("<b><p align='center' style='font-size:1em; color: red;'>No se pudo encontrar al empleado</p><b>");
					alert("El empleado con la cédula: "+cedula+", no existe o no está registrado");
					//alert(data);var usuario   = $.trim($("#usuario").val());
				}
			}
	});

}