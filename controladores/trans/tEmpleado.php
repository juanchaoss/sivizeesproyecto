<?php 
	include_once ("../../modelo/constante.php");
	include_once("../../modelo/clases/Fachada.php");
	include_once("../../modelo/Empleado.php");	
	// --- Sentencia para capturar cada uno de los parametros enviados en el request --- //
	foreach($_POST as $nombre_campo => $valor){
	   $asignacion = "\$" . $nombre_campo . "='".addslashes($valor)."';";
	   eval($asignacion);
	}
	
	switch($accion){	
		case 'cargarEstados':
			$obj   = new Empleado();
			$datos = $obj -> cargarEstados();
		break;

		case 'cargarMunicipios':
			$obj   = new Empleado();
			$datos = $obj -> cargarMunicipios($id_es);
		break; 

		case 'cargarParroquias':
			$obj   = new Empleado();
			$datos = $obj -> cargarParroquias($id_mun);
		break; 

		case 'cargarCiudades':
			$obj   = new Empleado();
			$datos = $obj -> cargarCiudades($id_es);
		break; 
		case 'registrarEmpleado':
			$obj   = new Empleado();
			$datos = $obj -> registrarEmpleado($cedula, $nombres,$apellidos, $fecha_na,$correo, $direccion, $celular, 
			$estado, $sexo, $numero, $municipio,$parroquia, $ciudad,$fecha_in,$condicion, $tipo_em,$cargo);
		break; 
		
		case 'verificarEmpleado':
			$obj   = new Empleado();
			$datos = $obj -> verificarEmpleado($cedula);
		break; 

		case 'buscarEmpleado':
			$obj = new Empleado();
			$datos = $obj -> buscarEmpleado($cedula);
		break;

		case 'actualizarDatosEmpleado':
			$obj = new Empleado();
			$datos = $obj -> actualizarDatosEmpleado($cedula, $nombres,$apellidos, $fecha_na,$correo, $direccion, $celular, 
			$estado, $sexo, $numero, $municipio,$parroquia, $ciudad,$fecha_in,$condicion, $tipo_em,$cargo, $hidempleado, $hidpersona);
		break;

		case 'eliminarDatosEmpleado':
			$obj = new Empleado();
			$datos = $obj -> eliminarDatosEmpleado($hidempleado, $hidpersona);
		break;
	
		default:
	    $datos = "Error accion no encontrada";
	}

    $salida = json_encode($datos);
    echo $salida;
    //echo $datos;
?>