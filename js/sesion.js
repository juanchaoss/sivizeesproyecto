function validarCampos() {

	var caja1 = $('#caja1').val();
	var caja2 = $('#caja2').val();

		if ($.trim(caja1) == '' || $.trim(caja2) == '') {
			$("#error").show().fadeOut(4000);
			//alert("ERROR...Existen campos vacios");
			//$('#prueba').html("<b><p align='center' style='font-size:1.5em; color: red;'>Existen campos vacios</p><b>");
			//$('#pruebar').hide('2');
			//$('#prueba').css('').html("<b><p align='center' style='font-size:1.5em; color: red;'>Existen campos vacios</p><b>");
			return;
		}

		$.ajax( {
			type : 'POST',
			url : "controladores/trans/tsesion.php",
			data :	{ accion: "iniciarSesion", usuario: caja1, clave: caja2},
			error : function(xhr, ajaxOptions, thrownError) {
				alert("Ups... Algo esta mal :(");
			},
			beforeSend: function(data){
                $("#prueba").html("<p align='center'><img src='img/Preloader_9.gif'></img></p>");
            },
            complete: function(data){
            	$("#prueba").html("");
            },
			success : function(data) {
				console.log(data);
				var data = eval("(" + data + ")");
				console.log(data);
				if(data){
					moverDirectorioSesion();
					//alert("ERROR... El usuario o la contraseña no coinciden.");
				}else{
					$("#error-usu").show().fadeOut(4000);
					//$('#prueba').html("<br><br><b><p align='center' style='font-size:1.5em; color: red;'>Datos incorrectos</p></b>");
				}
				if (data == 'ERNO01') {
					alert('ERNO01');
				}
			}
	});
}

function limpiarFormModalInicio(){

	$('#caja1').val('');
	$('#caja2').val('');
}

function moverDirectorioSesion(){
	window.location="vistas/index.php";
}
function moverDirectorioCerrado(){
	window.location="../index.php";
}

function moverDirectorioCerradoRp(){
	window.location="../../index.php";
}
function cerrarSesion(rep){
		if (rep != 1) {

				$.ajax( {
					type : 'POST',
					url : "../controladores/trans/tsesion.php",
					data :	{ accion: "cerrarSesion"},
					error : function(xhr, ajaxOptions, thrownError) {
						alert("Ups... Algo esta maaaaaaaaaaaaal :(");
					},
					success : function(data) {
						console.log(data);
						var data = eval("(" + data + ")");
						if(data){
							moverDirectorioCerrado();
						} else{
							alert("no se pudo cerrar la sesion");
							//alert(data);
						}
					}
			});

	    }else{
	    	$.ajax( {
					type : 'POST',
					url : "../../controladores/trans/tsesion.php",
					data :	{ accion: "cerrarSesion"},
					error : function(xhr, ajaxOptions, thrownError) {
						alert("Ups... Algo esta maeeeeeerererererel :(");
					},
					success : function(data) {
						console.log(data);
						var data = eval("(" + data + ")");
						if(data){
							moverDirectorioCerradoRp();
						} else{
							alert("no se pudo cerrar la sesion");
							alert(data);
						}
					}
	    });

	}
}

function validarCamposSesionOnline(){

	var cedu_em = $("#ci_em").val();
	var fe_em   = $("#fe_em").val();

	if ($.trim(cedu_em) == '' || $.trim(fe_em) == '') {
			$("#msj_error_vacio").show().fadeOut(4000);
			return;
	}
	iniciarSesionOnline(cedu_em,fe_em);
}

function iniciarSesionOnline(ced,fecha){

	$.ajax( {
			type  : 'POST',
			url   : "controladores/trans/tSesionOnline.php",
			data  :	{ accion: "iniciarSesionOnline", ced: ced, fecha: fecha},
			error : function(xhr, ajaxOptions, thrownError) {
				alert("Ups... Algo esta mal :(");
			},
			beforeSend: function(data){
                $("#img-hidden").show();
            },
            complete: function(data){
            	$("#img-hidden").hide();
            },
			success : function(data) {
				console.log(data);
				var data = eval("(" + data + ")");
				console.log(data);
				if(data){
					//alert(1);
					moverDirectorioSesionOnline();
				}else{
					//alert(0);
					limpiarFormSesionOline();
					$("#msj_error_emp").show().fadeOut(4000);
				}
		    }
	});
}

function limpiarFormSesionOline(){

	$("#ci_em").val('');
	$("#fe_em").val('');
}

function moverDirectorioSesionOnline(){url : "../../controladores/trans/tViatico.php",
	window.location='vistas/index_Online.php';
}

function moverDirectorioSesionOnlineCerrado(){
	window.location="../login.php";
}

function cerrarSessionOnline(){
	$.ajax( {
			type : 'POST',
			url : "../controladores/trans/tsesion.php",
			data :	{ accion: "cerrarSesion"},
			error : function(xhr, ajaxOptions, thrownError) {
				alert("Ups... Algo esta mal :(");
			},
			success : function(data) {
				console.log(data);
				var data = eval("(" + data + ")");
				if(data){
					console.log("cerrado");
					moverDirectorioSesionOnlineCerrado();
				} else{
					alert("no se pudo cerrar la sesion");
					alert(data);
				}
			}
	});
}