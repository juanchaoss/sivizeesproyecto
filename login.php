<?php
session_start();

    if ($_SESSION['usuarioOnline'] || $_SESSION['permitido_online']) {
        header('location: sistemaOnline/index.php');
    }
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title>Sistema Online de Gestion de Viaticos de la Zona Educativa E.S.</title>
        <link href="css/css/bootstrap.min.css" rel="stylesheet">
        <link href="css/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
        <link href="css/css/sb-admin-2.css" rel="stylesheet">
        <link href="css/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="css/css/datepicker.css">
    </head>
    <body>
    <img width="100%" height="55px" src="img/bar_gob1.png">
    <div class="col-lg-12">
        &nbsp;
    </div>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <img src="img/logo.png"  alt="">
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <img src="img/me.png" width="171" height="171" alt="">
    <div class="col-lg-12">
        &nbsp;
    </div>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="panel panel-default">
                     <div class="panel-heading">
                            <h3 class="panel-title text-center">Inicio de Sesion</h3>
                        </div>
                        <div class="panel-body">
                            <form role="form">
                                <fieldset>
                                     <div class="form-group">
                                    <label class="" for="ejemplo_password_1">N. Cedula del Empleado</label>
                                        <div class='input-group date'>
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-file">
                                                </span>
                                            </span>
                                            <input type='text' placeholder='N. de Cedula' name="ci_em" class="form-control hab" id="ci_em"/>
                                        </div>
                                  </div>
                                   <div class="form-group">
                                    <label class="" for="ejemplo_password_1">Fecha de Nacimiento</label>
                                        <div class='input-group date' id='datetimepicker1'>
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar">
                                                </span>
                                            </span>
                                            <input type='text' placeholder='Fecha de Nacimiento' class="form-control hab" id="fe_em"/>
                                        </div>
                                  </div>
                                    <button type="button" onclick="validarCamposSesionOnline();" class="center-block btn btn-outline btn-danger">Iniciar Sesion <span class="glyphicon glyphicon-log-in"></span>
                                    </button> <img src="img/Preloader_1.gif" class='center-block' id='img-hidden' alt="">
                                </fieldset>
                            </form>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="alert alert-warning" id="msj_error_emp" role="alert">¡ERROR... los del empleado introducido no existen!</div>
                    <div class="alert alert-danger" id="msj_error_vacio" role="alert">¡ERROR... Existen Campos vacios!</div>
                </div>
            </div>
        </div>
        
<!-- jQuery Version 1.11.1 -->
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.10.3.custom.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<script src="js/sesion.js"></script>
<script>
    $(document).ready(function(){

        $("#msj_error_emp").hide();
        $("#msj_error_vacio").hide();
        $("#img-hidden").hide();

        $("div[id^='datetimepicker']").datepicker({
            format: 'yyyy-mm-dd',
            language: "es",
            autoclose:true
        });
    });
</script>
</body>
</html>