<?php
session_start();
    if (!$_SESSION['usuarioOnline'] || !$_SESSION['permitido_online']) {
        header('location: ../login.php');
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Sistema de Gestión de Viaticos onLine</title>
    <link href="css/css/bootstrap.css" rel="stylesheet">
    <link href="css/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
    <link href="css/css/plugins/timeline.css" rel="stylesheet">
    <link href="css/css/sb-admin-2.css" rel="stylesheet">
    <link href="css/css/plugins/morris.css" rel="stylesheet">
    <link href="css/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand text-danger" href="index.php">Sistema de Gestión de Viáticos</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>Salir del Sistema<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> <?=$_SESSION['realname']?></a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="#" onClick='cerrarSessionOnline();'><i class="fa fa-sign-out fa-fw"></i> Cerrar Sesión</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a class="active" href="index.php"><i class="glyphicon glyphicon-home"></i> Inicio</a>
                        </li>
                        <li>
                            <a href="#" class="dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true"><i class="glyphicon glyphicon-tasks"></i> Gestión del Viático<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                <li>
                                    <a href="form_soli_gen.php"><span class="glyphicon glyphicon-pencil"></span> Generar Solicitud de Viático</a>
                                </li>
                                <li>
                                    <a href="form_consul.php"><span class="glyphicon glyphicon-search"></span> Consultar Viáticos</a>
                                </li>
                                <li>
                                    <a href="form_consul.php"><span class="glyphicon glyphicon-edit"></span> Generar Cálculo del Viatico</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
<!--FIN DEL MENU-->
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header text-danger">SIVIZEES Online</h3>
                </div>
            </div>
            <div class="col-lg-12">
                <img src="img/SIVIZE LOGO1.png" width="300px" height="300px" alt="" class="center-block">
            </div>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <script src="js/jquery-1.11.2.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/metisMenu.min.js"></script>
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>
    <script src="js/sb-admin-2.js"></script>
    <script src='../js/sesion.js'></script>
</body>
</html>
