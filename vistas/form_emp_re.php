<?php
    include_once ('libs/control_sesion.php');
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Registro de Empleados</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="stylesheet" href="bootstrap-3.2.0-dist/css/bootstrap.css">
	<link rel="stylesheet" href="bootstrap-3.2.0-dist/css/bootstrap-theme.css">
	<link rel="stylesheet" href="bootstrap-3.2.0-dist/css/datepicker.css">
	<!--link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap-responsive.css"-->
	<!--style>
		/*Escritorio*/
		@media(min-width: 1200px){
			 body{color: blue;}
		}
		/*Escritorio pequeno o tablet*/
		@media(min-width: 768px) and (max-width: 979px){
        	  body{color: green;}
		}/*tablet o smartphone*/
		@media(max-width: 767px){
			  body{color: red;}
		}/*smartphone*/
		@media(max-width: 480px){
			  body{color: orange;}
		}
	</style-->
</head>
<body>
	<div class="container-fluid"><!--Menu-->
		<div class="row">
			<div class="col-md-12">
			<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			  <div class="container-fluid">
			    <!-- Brand and toggle get grouped for better mobile display -->
			    <div class="navbar-header">
			      <button type="button" class="navbar-toggle" data-toggle="collapse"
			      data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
			      </button>
			    <a class="navbar-brand" href="index.php"><img  src="img/SIVIZE LOGO-menu.png"> SIVIZEES</a>
			    </div>

			    <!-- Collect the nav links, forms, and other content for toggling -->
			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			      <ul class="nav navbar-nav">
			        <!--li><a href="#">Link</a></li>
			        <li><a href="#">Link</a></li-->
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			          <span class="glyphicon glyphicon-user"></span> Usuarios<span class="caret"></span></a>
			          <ul class="dropdown-menu" role="menu">
			            <!--li><a href="form_usu_ac.php"><span class="glyphicon glyphicon-retweet">
			            	</span> Gestión de Usuarios</a></li-->
			            <!--li><a href="#myModal1" data-toggle="modal" onclick="listarUsuarios();"><span class="glyphicon glyphicon-align-justify"></span> Listar Usuarios</a></li-->
			            <li><a href="form_usu_cre.php">
			            	<span class="glyphicon glyphicon-plus-sign"></span> Gestión de Usuario</a></li>
			          </ul>
			        </li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			          <span class="glyphicon glyphicon-briefcase"></span> Empleados<span class="caret"></span></a>
			          <ul class="dropdown-menu" role="menu">
			            <li><a href="form_emp_re.php"><span class="glyphicon glyphicon-pencil"></span> Registro de Empleado</a></li>
			            <li><a href="form_emp_ac.php"><span class="glyphicon glyphicon-refresh"></span> Actualizar datos del empleado</a></li>
			          </ul>
			        </li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			          	<span class='glyphicon glyphicon-usd'></span> Presupuesto<span class="caret"></span></a>
			          <ul class="dropdown-menu" role="menu">
			            <li><a href="form_presu_crear_parti.php"><span class="glyphicon glyphicon-upload"></span> Crear Partida</a></li>
			            <li><a href="form_presu_crear_proyec.php"><span class="glyphicon glyphicon-upload"></span> Crear Proyecto</a></li>
			            <li><a href="form_presu_gestion_movi.php"><span class="glyphicon glyphicon-list-alt"></span> Gestionar Movimiento</a></li>
			            <li><a href="form_via_pre.php"><span class="glyphicon glyphicon-pushpin"></span> Gestión de Precios de Viáticos</a></li>
			          </ul>
			        </li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			          	<span class='glyphicon glyphicon-plane'></span> Viaticos<span class="caret"></span></a>
			          <ul class="dropdown-menu" role="menu">
			          <li><a href="#" onclick="window.open('http://www.me.gob.ve/servicios/recibo/consultacopia.php');"><span class="glyphicon glyphicon-download-alt"></span> Descargar Recibo de Pago</a></li>
			            <li><a href="form_via_soli.php"><span class="glyphicon glyphicon-folder-open"></span> Gestión de Solicitud de Viáticos</a></li>
			             <li><a href="lista_soli.php"><span class="glyphicon glyphicon-align-justify"></span> Gestión de Aprobación de Viáticos</a></li>
			             <li><a href="form_via_cal.php"><span class="glyphicon glyphicon-tasks"></span> Gestión de Cálculo de Viáticos</a></li>
			             <li><a href="lista_calc.php"><span class="glyphicon glyphicon-align-justify"></span> Gestión de Aprobación de Cálculo de Viáticos</a></li>
			             <li><a href="form_pago_via.php"><span class="glyphicon glyphicon-asterisk"></span> Pago de Solicitudes</a></li>
			            <li><a href="consulStatusSol.php"><span class="glyphicon glyphicon-list-alt"></span> Status de la Solicitud</a></li>
			            <li><a href="form_ciu.php"><span class="glyphicon glyphicon-globe"></span> Activar/Desactivar Ciudades</a></li>
			            <!--li><a href="#">Crear Periodo</a></li>
			            <li><a href="#">Crear Mes</a></li-->
			          </ul>
			        </li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			          	<span class='glyphicon glyphicon-list-alt'></span> Reportes<span class="caret"></span></a>
			          <ul class="dropdown-menu" role="menu">
			            <li><a href="reportes/rptListadoZona.php"><span class="glyphicon glyphicon-list-alt"></span> Listado de Solicitudes por Zona</a></li>
			            <li><a href="reportes/rptListadoDispon.php"><span class="glyphicon glyphicon-list-alt"></span> Listado de Disponibilidades por Partida</a></li>
			            <li><a href="reportes/rptListadoMov.php"><span class="glyphicon glyphicon-list-alt"></span> Listados de Movimientos por Partida</a></li>
			            <li><a href="reportes/rptListadoStatus.php"><span class="glyphicon glyphicon-list-alt"></span> Listados de Solicitudes por Status</a></li>
			            <li><a href="reportes/rptSolicitudesEmp.php"><span class="glyphicon glyphicon-list-alt"></span> Listados de Solicitudes por Empleado</a></li>
			            <li><a href="reportes/rptConsolidadoStatus.php"><span class="glyphicon glyphicon-list-alt"></span> Consolidado de Solicitudes por Status</a></li>
			            <li><a href="reportes/bus_avan.php"><span class="glyphicon glyphicon-search"></span> Busqueda Avanzada de Viáticos</a></li>
			          </ul>
			        </li>
			        <!--li><a href="#"><span class="glyphicon glyphicon-file"></span>Reportes PDF</a></li>
			        <li><a href="#"><span class="glyphicon glyphicon-cloud-download"></span>Descargar Manual</a></li-->
			      </ul>
			      <!--form class="navbar-form navbar-left" role="search">
			        <div class="form-group">
			          <input type="text" class="form-control" placeholder="Search">
			        </div>
			        <button type="submit" class="btn btn-default">Submit</button>
			      </form-->
			      <ul class="nav navbar-nav navbar-right">
			        <!--li><a href="#">Link</a></li-->
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			          	<span class="glyphicon glyphicon-user"></span> <?=$_SESSION['usuario']?><span class="caret"></span></a>
			          <ul class="dropdown-menu" role="menu">
			          <!--li><a href="">Cambiar contraseña</a></li-->
			            <li><a href="#" onclick="mostrarModal('Mensaje de Alerta',
			            '¿Esta seguro de Cerrar la sesión?', 'cerrarSesion();')">
			            	<span class="glyphicon glyphicon-log-out"></span> Cerrar Sesión</a></li>
			          </ul> <!--onclick="cerrarSesion();"-->
			        </li>
			      </ul>
			    </div><!-- /.navbar-collapse data-toggle="modal" data-target="#myModal"  " -->
			  </div><!-- /.container-fluid -->
			</nav>
			</div>
		</div>
	</div><!--Fin Menu-->
	<div class="span12">&nbsp;</div>
	<div class="span12">&nbsp;</div>
	<h3 class="text-center text-danger"><strong>Registro de Empleado.</strong></h3>
	<div class="container">
	   <div class="row">
	       <div class="col-md-2"></div>
	       <div class="col-md-8">
	       <div class="progress"><!--PROGRESSBAR-->
		       <div id="progress_em" class="progress-bar progress-bar-info progress-bar-striped active" id="progress"  role="progressbar"
		       			 aria-valuemin="0" aria-valuemax="100" style="width: 0%">
		      </div><!--FIN DE PROGRESSBAR-->
           </div>
	           <!-- TABS -->
					<ul class="nav nav-tabs" role="tablist" id="tabs_em">
					  <li class="active"><a href="#home" role="tab" data-toggle="tab">Datos Personales</a></li>
					  <li><a href="#profile" role="tab" data-toggle="tab">Datos de Ubicación</a></li>
					  <li><a href="#messages" role="tab" data-toggle="tab">Datos del Empleo</a></li>
					</ul>
	       </div>
	       <div class="col-md-2"></div>
	   </div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-3"></div>
			<div class=" col-md-5">
					<!-- TABS CAJA -->
				<div class="tab-content">
					  <div class="tab-pane fade in active" id="home">
					  	<form id="form_em" role="form">
                                <h4 class="text-center text-danger"><strong>Datos personales.</strong></h4>
                                  <div class="form-group">
                                       <label for="ejemplo_email_1">Cédula</label>
                                        <input type="text"  class="form-control hab" id="ced_em"
                                               placeholder="Cédula">
                                  </div>
                                  <div class="form-group">
                                    <label for="ejemplo_email_1">Nombres</label>
                                    <input type="text"  class="form-control hab " id="nom_em"
                                           placeholder="Nombres">
                                  </div>
                                  <div class="form-group">
                                    <label for="ejemplo_password_1">Apellidos</label>
                                    <input type="text" class="form-control hab" id="ape_em"
                                           placeholder="Introduzca sus Apellidos">
                                    </div>
                                  <div class="form-group">
                                    <label for="ejemplo_password_1">Fecha de Nacimiento</label>
                                        <div class='input-group date' id='datetimepicker1'>
                                            <input type='text' readonly="readonly" class="form-control hab" id="fe_em" onchange="progressBar();" />
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar">
                                            	</span>
                                            </span>
                                        </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="ejemplo_password_1">Sexo</label>
                                    <label class="radio-inline">
                                      <input type="radio" name="inlineRadioOptions" id="sexoM" checked="checked"
                                      value="Masculino"> M
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="inlineRadioOptions" id="sexoF" value="Femenino"> F
                                    </label>
                                  </div>
                                  <div class="form-group">
                                    <label for="ejemplo_password_1">Correo Electrónico</label>
                                    <input type="text" class="form-control hab" id="correo_em"
                                           placeholder="Introduzca su Correo Electrónico">
                                  </div>
                                  <div class="form-group">
                                    <label for="ejemplo_password_1">Celular</label>
                                    <input type="text" class="form-control hab" id="celu_em"
                                           placeholder="Número de Celular">
                                  </div>
                                  <div class="form-group">
                                    <label for="ejemplo_password_1">Número local</label>
                                    <input type="text" class="form-control hab" id="num_em"
                                           placeholder="Número de TLF de Casa">
                                  </div>
					  </div>
					  <div class="tab-pane fade" id="profile">
					       <h4 class="text-center text-danger"><strong>Datos de la Ubicación.</strong></h4>
				  <div class="form-group"><div class="span12">&nbsp;</div>
						<label for="ejemplo_password_1">Dirección</label>
						<textarea name="" class="form-control hab" id="direc_em" placeholder="Dirección"></textarea>
				   </div>
				   <div class="form-group">
				   		<label for="ejemplo_password_1">Estado</label>
						<select name="selEstado" id="selEstado" class="form-control" onchange="cargarMunicipios(); progressBar();">
							<option value="0">Seleccione</option>
						</select>
				   </div>
				   <div class="form-group">
				   		<label for="ejemplo_password_1">Municipio</label>
						<select name="selMunicipio" id="selMunicipio" class="form-control"
						 onchange="cargarParroquias();cargarCiudades(); progressBar();">
							<option value="0">Seleccione</option>
						</select>
				   </div>
				   <div class="form-group">
				   		<label for="ejemplo_password_1">Parroquia</label>
						<select name="selParroquia" id="selParroquia" class="form-control" onchange="progressBar();">
							<option value="0">Seleccione</option>
						</select>
				   </div>
				   <div class="form-group">
				   		<label for="ejemplo_password_1">Ciudad</label>
						<select name="selCiudad" id="selCiudad" class="form-control" onchange="progressBar();">
							<option value="0">Seleccione</option>
						</select>
				   </div>
					  </div>
					  <div class="tab-pane fade" id="messages">
					      <h4 class="text-center text-danger"><strong>Datos de Empleo.</strong></h4>
				   <div class="form-group">
				    <label for="ejemplo_password_1">Fecha de Ingreso</label>
				    <div class='input-group date' id='datetimepicker2'>
                    <input type='text' class="form-control hab" id="fe_em_em" onchange="progressBar();" />
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                    </span>
				  </div>
				  </div>
				  <div class="form-group">
				    <label for="ejemplo_password_1">Condición Laboral</label>
                    <select name="" id="condi_em" class="form-control" onchange="progressBar();">
                    	<option value="-">Seleccione</option>
                    	<option value="1">Fijo</option>
                    	<option value="0">Contratado</option>
                    </select>
				  </div>
				  <div class="form-group">
				    <label for="ejemplo_password_1">Tipo de Empleo</label>
                    <select name="" id="tipo_em" class="form-control" onchange="progressBar();">
                    	<option value="-">Seleccione</option>
                    	<option value="1">Administrativo</option>
                    	<option value="0">Obrero</option>
                    </select>
				  </div>
				  <div class="form-group">
				     <label for="ejemplo_email_1">Cargo</label>
				    <input type="text"  class="form-control hab" id="cargo_em"
				           placeholder="Introduzca el Cargo">
				  </div>
					  </div>
				</div><div class="col-md-7">
			</div>
			<div class="col-md-3"></div>
		</div>
		       <button type="button" class="btn btn-primary"  id="btnRegistrarEmp" onclick="verificarEmpleado();">
		       	<span class='glyphicon glyphicon-floppy-disk'></span></button>
               <button type="reset" class="btn btn-default"><span class='glyphicon glyphicon-file'></span></button>
               &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<span class="glyphicon glyphicon-info-sign"
               data-placement="bottom" data-toggle="tooltip" title="El boton de guardar se habilitará al llenar todos los
               	campos del formulario. Debe hacer clic en cada pestaña para llenar todos los campos." id='ol'></span>
               </form>
	</div>
  </div>
  <br><br>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;
        	</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title text-center" id="myModalLabel"></h4>
      </div>
      <div class="modal-body">
        <p align="center" id="mensajeModal"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="btnAccionModal" >Aceptar</button>
      </div>
    </div>
  </div>
</div><!-- Fin Modal 1 -->

<!--MODAL LISTAR-->
	<!-- Modal -->
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Lista de usuario en el sistema.</h4>
      </div>
      <div class="modal-body">
      <div style="height: 200px; overflow: auto;">
        <table id="tbl-usu" class="table table-bordered table-hover">
        <tr>
        	<td>#</td>
        	<td>Usuario</td>
        	<td>Tipo</td>
        	<td>Aprobador</td>
        </tr>
        </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!--FIN MODAL LISTAR-->
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/jquery-ui-1.10.3.custom.js"></script>
	<script src="bootstrap-3.2.0-dist/js/bootstrap.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="../js/sesion.js"></script>
	<script src="../controladores/js/empleado.js"></script>
	<script>
		 cargarEstados();
		 //$('#myModal').modal('show');
		 $("div[id^='datetimepicker']").datepicker({
       			format: 'yyyy-mm-dd',
       			language: "es",
        		autoclose:true
    	});
		 $("#btnRegistrarEmp").attr("disabled",true);
		 $('.form-control').change(function(){
		 	habilitarRegistro();
		 });
		 $('.hab').keyup(function(){
		 	habilitarRegistro();
		 });
		 $("#ol").click(function(){
             $(this).tooltip('show');
         });
         function progressBar(){
         	var cedula     = $("#ced_em").val();
			var nombres    = $("#nom_em").val();
			var apellidos  = $("#ape_em").val();
			var fecha_na   = $("#fe_em").val();
			var email      = $("#correo_em").val();
			var celular    = $("#celu_em").val();
			var local      = $("#num_em").val();

			//datos de ubicacion
			var direccion  = $("#direc_em").val();
			var estado     = $("#selEstado").val();
			var municipio  = $("#selMunicipio").val();
			var parroquia  = $("#selParroquia").val();
			var ciudad     = $("#selCiudad").val();

			//datos de empleo
			var fecha_in   = $("#fe_em_em").val();
			var condicion  = $("#condi_em").val();
			var tipo_em    = $("#tipo_em").val();
			var cargo      = $("#cargo_em").val();

         	var ancho = $(".progress").css("width");
         	var actual = $("#progress_em").css("width");

         	var cont=0;
			 if ($.trim(nombres) != '') {
			 	cont++;
			 	//console.log(1);
			 }
			  if ($.trim(apellidos) != '') {
			 	cont++;
			 	//console.log(2);
			 }
			  if ($.trim(fecha_na) != '') {
			 	cont++;
			 	//console.log(3);
			 }
			  if ($.trim(direccion) != '') {
			 	cont++;
			 	//console.log(4);
			 }
			  if ($.trim(estado) != '0') {
			 	cont++;
			 	//console.log(5);
			 }
			  if ($.trim(municipio) != '0') {
			 	cont++;
			 	//console.log(6);
			 }
			  if ($.trim(parroquia) != '0') {
			 	cont++;
			 	//console.log(7);
			 }
			  if ($.trim(ciudad) != '0') {
			 	cont++;
			 	//console.log(8);
			 }
			  if ($.trim(fecha_in) != '') {
			 	cont++;
			 	//console.log(9);
			 }
			  if ($.trim(condicion) != '-') {
			 	cont++;
			 	//console.log(10);
			 }
			  if ($.trim(tipo_em) != '-') {
			 	cont++;
			 	//console.log(11);
			 }
			  if ($.trim(cargo) != '') {
			 	cont++;
			 	//console.log(12);
			 }
			  if ($.trim(celular) != '') {
			 	cont++;
			 	//console.log(13);
			 }
			  if ($.trim(email) != '') {
			 	cont++;
			 	//console.log(14);
			 }
			  if ($.trim(local) != '') {
			 	cont++;
			 	//console.log(15);
			 }
			  if ($.trim(cedula) != '') {
			 	cont++;
			 	//console.log(16);
			 }
			 //console.log('cont '+cont);
         	actual = parseFloat(actual.substr(0,actual.length-2));
         	ancho  = parseFloat(ancho.substr(0,ancho.length-2));
         	var pedazo = ancho/16;
         	actual=pedazo*cont;

         	var porc=(actual*100)/ancho;
         	//console.log('%: '+porc);
         	$("#progress_em").css("width",porc+'%');
         	/*if(cont==3)
         		$("#progress_em").removeClass('progress-bar-info').addClass('progress-bar-danger');
         	else
         		$("#progress_em").removeClass('progress-bar-info').removeClass('progress-bar-danger').addClass('progress-bar-info');*/
         	if (cont==16)
         		$("#progress_em").removeClass('progress-bar-info').addClass('progress-bar-success');
         		//progress-bar-success
         	else
         		$("#progress_em").removeClass('progress-bar-info').removeClass('progress-bar-danger').removeClass('progress-bar-success').addClass('progress-bar-info');
         }
         $('.form-control').keyup(function(){
		 	if ($.trim($(this).val()) != "") {
		 		//console.log("'"+$(this).val()+"'");
		 		progressBar();
		 	}else{
		 		//console.log("<"+$(this).val()+">");
		 		progressBar();
		 	}
		 });

	</script>
	<script>
		function mostrarModal(titulo, msg, funcion){
			$('#myModalLabel').html(titulo);
			$('#mensajeModal').html(msg);
			$('#btnAccionModal').unbind('click').click({funcion:funcion}, function(eventObject){
				eval(eventObject.data.funcion); //eval('cerrarSesion();');
			});
			$('#myModal').modal('show');
		}
	</script>
</body>
</html