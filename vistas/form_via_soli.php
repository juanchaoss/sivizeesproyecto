<?php
session_start();
    include_once ('libs/control_sesion.php');
    if ($_SESSION['nivel'] != 1) {
    	?>
    	<script>
    		alert("Usted no tiene acceso a esta parte del sistema");
    		location.href='../';
    	</script>
    	<?php
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Solicitud De Viáticos</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="stylesheet" href="bootstrap-3.2.0-dist/css/bootstrap.css">
	<link rel="stylesheet" href="bootstrap-3.2.0-dist/css/datepicker.css">
	<link rel="stylesheet" href="bootstrap-3.2.0-dist/css/bootstrap-theme.css">
	<!--link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap-responsive.css"-->
	<!--style>
		/*Escritorio*/
		@media(min-width: 1200px){
			 body{color: blue;}
		}
		/*Escritorio pequeno o tablet*/
		@media(min-width: 768px) and (max-width: 979px){
        	  body{color: green;}
		}/*tablet o smartphone*/
		@media(max-width: 767px){
			  body{color: red;}
		}/*smartphone*/
		@media(max-width: 480px){
			  body{color: orange;}
		}
	</style-->
</head>
<body>
	<div class="container-fluid"><!--Menu-->
		<div class="row">
			<div class="col-md-12">
			<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			  <div class="container-fluid">
			    <!-- Brand and toggle get grouped for better mobile display -->
			    <div class="navbar-header">
			      <button type="button" class="navbar-toggle" data-toggle="collapse"
			      data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
			      </button>
			    <a class="navbar-brand" href="index.php"><img  src="img/SIVIZE LOGO-menu.png"> SIVIZEES</a>
			    </div>

			    <!-- Collect the nav links, forms, and other content for toggling -->
			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			      <ul class="nav navbar-nav">
			        <!--li><a href="#">Link</a></li>
			        <li><a href="#">Link</a></li-->
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			          <span class="glyphicon glyphicon-user"></span> Usuarios<span class="caret"></span></a>
			          <ul class="dropdown-menu" role="menu">
			            <!--li><a href="form_usu_ac.php"><span class="glyphicon glyphicon-retweet">
			            	</span> Gestión de Usuarios</a></li-->
			            <!--li><a href="#myModal1" data-toggle="modal" onclick="listarUsuarios();"><span class="glyphicon glyphicon-align-justify"></span> Listar Usuarios</a></li-->
			            <li><a href="form_usu_cre.php">
			            	<span class="glyphicon glyphicon-plus-sign"></span> Gestión de Usuario</a></li>
			          </ul>
			        </li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			          <span class="glyphicon glyphicon-briefcase"></span> Empleados<span class="caret"></span></a>
			          <ul class="dropdown-menu" role="menu">
			            <li><a href="form_emp_re.php"><span class="glyphicon glyphicon-pencil"></span> Registro de Empleado</a></li>
			            <li><a href="form_emp_ac.php"><span class="glyphicon glyphicon-refresh"></span> Actualizar datos del empleado</a></li>
			          </ul>
			        </li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			          	<span class='glyphicon glyphicon-usd'></span> Presupuesto<span class="caret"></span></a>
			          <ul class="dropdown-menu" role="menu">
			            <li><a href="form_presu_crear_parti.php"><span class="glyphicon glyphicon-upload"></span> Crear Partida</a></li>
			            <li><a href="form_presu_crear_proyec.php"><span class="glyphicon glyphicon-upload"></span> Crear Proyecto</a></li>
			            <li><a href="form_presu_gestion_movi.php"><span class="glyphicon glyphicon-list-alt"></span> Gestionar Movimiento</a></li>
			            <li><a href="form_via_pre.php"><span class="glyphicon glyphicon-pushpin"></span> Gestión de Precios de Viáticos</a></li>
			          </ul>
			        </li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			          	<span class='glyphicon glyphicon-plane'></span> Viaticos<span class="caret"></span></a>
			          <ul class="dropdown-menu" role="menu">
			          <li><a href="#" onclick="window.open('http://www.me.gob.ve/servicios/recibo/consultacopia.php');"><span class="glyphicon glyphicon-download-alt"></span> Descargar Recibo de Pago</a></li>
			            <li><a href="form_via_soli.php"><span class="glyphicon glyphicon-folder-open"></span> Gestión de Solicitud de Viáticos</a></li>
			             <li><a href="lista_soli.php"><span class="glyphicon glyphicon-align-justify"></span> Gestión de Aprobación de Viáticos</a></li>
			             <li><a href="form_via_cal.php"><span class="glyphicon glyphicon-tasks"></span> Gestión de Cálculo de Viáticos</a></li>
			             <li><a href="lista_calc.php"><span class="glyphicon glyphicon-align-justify"></span> Gestión de Aprobación de Cálculo de Viáticos</a></li>
			             <li><a href="form_pago_via.php"><span class="glyphicon glyphicon-asterisk"></span> Pago de Solicitudes</a></li>
			            <li><a href="consulStatusSol.php"><span class="glyphicon glyphicon-list-alt"></span> Status de la Solicitud</a></li>
			            <li><a href="form_ciu.php"><span class="glyphicon glyphicon-globe"></span> Activar/Desactivar Ciudades</a></li>
			            <!--li><a href="#">Crear Periodo</a></li>
			            <li><a href="#">Crear Mes</a></li-->
			          </ul>
			        </li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			          	<span class='glyphicon glyphicon-list-alt'></span> Reportes<span class="caret"></span></a>
			          <ul class="dropdown-menu" role="menu">
			            <li><a href="reportes/rptListadoZona.php"><span class="glyphicon glyphicon-list-alt"></span> Listado de Solicitudes por Zona</a></li>
			            <li><a href="reportes/rptListadoDispon.php"><span class="glyphicon glyphicon-list-alt"></span> Listado de Disponibilidades por Partida</a></li>
			            <li><a href="reportes/rptListadoMov.php"><span class="glyphicon glyphicon-list-alt"></span> Listados de Movimientos por Partida</a></li>
			            <li><a href="reportes/rptListadoStatus.php"><span class="glyphicon glyphicon-list-alt"></span> Listados de Solicitudes por Status</a></li>
			            <li><a href="reportes/rptSolicitudesEmp.php"><span class="glyphicon glyphicon-list-alt"></span> Listados de Solicitudes por Empleado</a></li>
			            <li><a href="reportes/rptConsolidadoStatus.php"><span class="glyphicon glyphicon-list-alt"></span> Consolidado de Solicitudes por Status</a></li>
			            <li><a href="reportes/bus_avan.php"><span class="glyphicon glyphicon-search"></span> Busqueda Avanzada de Viáticos</a></li>
			          </ul>
			        </li>
			        <!--li><a href="#"><span class="glyphicon glyphicon-file"></span>Reportes PDF</a></li>
			        <li><a href="#"><span class="glyphicon glyphicon-cloud-download"></span>Descargar Manual</a></li-->
			      </ul>
			      <!--form class="navbar-form navbar-left" role="search">
			        <div class="form-group">
			          <input type="text" class="form-control" placeholder="Search">
			        </div>
			        <button type="submit" class="btn btn-default">Submit</button>
			      </form-->
			      <ul class="nav navbar-nav navbar-right">
			        <!--li><a href="#">Link</a></li-->
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			          	<span class="glyphicon glyphicon-user"></span> <?=$_SESSION['usuario']?><span class="caret"></span></a>
			          <ul class="dropdown-menu" role="menu">
			          <!--li><a href="">Cambiar contraseña</a></li-->
			            <li><a href="#" onclick="mostrarModal('Mensaje de Alerta',
			            '¿Esta seguro de Cerrar la sesión?', 'cerrarSesion();')">
			            	<span class="glyphicon glyphicon-log-out"></span> Cerrar Sesión</a></li>
			          </ul> <!--onclick="cerrarSesion();"-->
			        </li>
			      </ul>
			    </div><!-- /.navbar-collapse data-toggle="modal" data-target="#myModal"  " -->
			  </div><!-- /.container-fluid -->
			</nav>
			</div>
		</div>
	</div><!--Fin Menu-->
	<div class="span12">&nbsp;</div>
	<div class="span12">&nbsp;</div>
	<div class="span12">&nbsp;</div>

  <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;
        	</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title text-center" id="myModalLabel"></h4>
      </div>
      <div class="modal-body">
        <p align="center" id="mensajeModal"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="btnAccionModal" >Aceptar</button>
      </div>
    </div>
  </div>
</div><!-- Fin Modal 1 -->
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-2">
			<div class="input-group">
			  <input type="text" onkeypress="return soloNum(event);" id="ced_aux" data-placement="bottom" data-toggle="tooltip" data-placement="bottom"
	           title="Usuario de sistema" class="form-control" placeholder='C.I. Empleado'>
			  <span class="input-group-btn">
			    <button class="btn btn-default" onclick="buscarDatosEmpleadoSoli();" type="button"><span class="glyphicon glyphicon-search"></span></button>
			  </span>
            </div>
		</div>
	</div>
</div>
<h3 class="text-center text-danger"><strong>Gestión de Solicitud de Viáticos</strong></h3>
<div class="container">
	<div class="row">
		<div class="col-lg-1"></div>
		<div class="col-lg-10">
			<div role="tabpanel">
			  <!-- Nav tabs -->
			  <ul class="nav nav-tabs" role="tablist" id="tabs_soli">
			    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Datos del Solicitante</a></li>
			    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Datos de la Misión</a></li>
			  </ul>
			  <!-- Tab panes -->
			  <div class="tab-content fade in active">
			    <div role="tabpanel" class="tab-pane active fade in active" id="home">
			    	<h4 class="text-danger"><strong>Datos del Solicitante</strong></h4>
			    	<form class="form-inline" role="form">
		                  <div class="form-group">
						   <strong>Cédula:</strong> &nbsp; <input type="text" class="form-control" id="ced_em" placeholder="Cédula" readonly="readonly" data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom">
						  </div>
						  <div class="form-group">
						   <strong>Nombre:</strong> &nbsp; <input type="text" class="form-control" id="nom_em" placeholder="Nombre" readonly="readonly">
						  </div>
						  <div class="form-group">
						   <strong>Apellido:</strong> &nbsp; <input type="text" class="form-control" id="ape_em" placeholder="Apellido" readonly="readonly"><br><br>
						  </div>
						 <div class="form-group">
						   &nbsp; <strong>Cargo:</strong> &nbsp; <input type="text" class="form-control" id="cargo_em" placeholder="Cargo que ocupa" readonly="readonly">
						 </div>
						 <div class="form-group">
						   <strong>Estado:</strong> &nbsp;&nbsp; <input type="text" class="form-control" id="esta" placeholder="Estado" readonly="readonly">
						 </div>
						 <div class="form-group">
						   <strong>Ciudad:</strong> &nbsp;&nbsp;&nbsp; <input type="text" class="form-control" id="ciu" placeholder="Ciudad" readonly="readonly">
						 </div>
						 <div class="form-group"><br>
						   <strong>Dirección:</strong> &nbsp; <input size="100" type="text" class="form-control" id="direc" placeholder="Dirección" readonly="readonly">
						 </div>
						  <br> <strong>Sexo:</strong>
						  <label class="radio-inline">
						  <input type="radio" disabled="disabled" name="inlineRadioOptions" id="sexoM" value="option1"> M
						  <input type="hidden" id="hidempleado">
						</label>
						<label class="radio-inline">
						  <input type="radio" disabled="disabled" name="inlineRadioOptions" id="sexoF" value="option2"> F
						</label>
					</form>
					<div class="col-lg-9">
						&nbsp;
					</div>
			    </div>
			    <div role="tabpanel" class="tab-pane fade" id="profile">
			    <form class="form-inline">
					<h4 class="text-danger"><strong>Datos de la Misión</strong></h4>
	                  <div class="form-group">
	                  	<strong>Estado Origen</strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select class="form-control" id="esta_ori" onchange="traerCiudades($('#esta_ori').val(),'ciu_ori');">
	                  																<option checked value="0">Seleccione</option>
	                  				                                            </select>
	                  <strong>Ciudad de Origen</strong> <select class="form-control" id="ciu_ori">
	                  						<option checked value="0">Seleccione</option>
	                  				   </select>
	                  				  <strong>Estado Destino</strong> <select class="form-control" id="esta_des" onchange="traerCiudades($('#esta_des').val(),'ciu_des');">
	                  						<option checked value="0">Seleccione</option>
	                  				   </select><br><br>
	                  	<strong>Ciudad de Destino</strong>&nbsp;<select class="form-control" id="ciu_des" >
	                  							<option checked value="0">Seleccione</option>
	                  					  </select>
	                 <!-- Cargar Nueva Ciudad <a href="#" onclick="msjAlerta();" class="btn btn-default" data-toggle="modal" data-target=".bs-example-modal-sm">Carga Ciudad</a--><br>
					  <strong> Lugar de Destino</strong> <input type="text" class="form-control" size="100" id="lugardes" placeholder="Lugar de Destino"><br><br>
					    <div class="form-group">
	                       <strong> Fecha de la misión: Desde</strong>&nbsp;
	                        <div class='input-group date' id='datetimepicker1'>
	                            <input type='text' readonly='readonly' placeholder='AAAA/MM/DD' class="form-control hab" id="fedesde" />
	                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar">
	                            	</span>
	                            </span>
	                        </div>
	                  </div>
	                  <div class="form-group">
		                    &nbsp;&nbsp;<strong>Hasta</strong>&nbsp;
		                    <div class='input-group date' id='datetimepicker1'>
		                        <input type='text' readonly='readonly' onblur="validarFechasDesHas($('#fedesde').val(),$('#fehasta').val());" placeholder='AAAA/MM/DD' class="form-control hab" id="fehasta"/>
		                        <span class="input-group-addon" onclick="validarFechasDesHas($('#fedesde').val(),$('#fehasta').val());"><span class="glyphicon glyphicon-calendar">
		                        	</span>
		                        </span>
		                    </div>
		              </div><br><br>
					  </div>
					   <div class="form-group">
					   <strong>Motivo del Viaje</strong> <textarea name="" id="motivo" cols="50" rows="2" class="form-control" placeholder='Motivo del viaje'></textarea>
					   </div><br><br>
						<strong>Transporte Utilizado de ida:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<label class="radio-inline">
						  <input type="radio" name="radtrans" checked="checked" id="transa" value="option1"> <strong>Aéreo</strong>
						</label>
						<label class="radio-inline">
						  <input type="radio" name="radtrans" id="transt" value="option2"> <strong>Terrestre</strong>
						</label>
						<label class="radio-inline">
						  <input type="radio" name="radtrans" id="transo" value="option3"> <strong>Otros</strong>
						</label>
						<div class="form-group">
					    	<input type="text" class="form-control" id="otro" placeholder="Otro">
					    </div><br><br>

					    <strong>Transporte Utilizado de Vuelta:</strong>
						<label class="radio-inline">
						  <input type="radio" name="radtransvu" checked="checked" id="transavu" value="option1"> <strong>Aéreo</strong>
						</label>
						<label class="radio-inline">
						  <input type="radio" name="radtransvu" id="transtvu" value="option2"> <strong>Terrestre</strong>
						</label>
						<label class="radio-inline">
						  <input type="radio" name="radtransvu" id="transovu" value="option3"> <strong>Otros</strong>
						</label>
						<div class="form-group">
					    	<input type="text" class="form-control" id="otrovu" placeholder="Otro">
					    </div>

				</form>
				<button type="button" onclick="registrarSolicitud();"
						class="btn btn-primary">Guardar<span class="glyphicon glyphicon-floppy-disk"></span></button>
						<div class="col-lg-12">
							&nbsp;
						</div>
			    </div>
			  </div>
			</div>
		</div>
		<div class="col-lg-1"></div>
	</div>
</div>

   <!--MODAL LISTA_SOLI-->
   	<div class="modal fade" id="myModal12" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	        <h4 class="modal-title" id="myModalLabel">Lista de Solicitudes de Viáticos</h4>
	      </div>
	      <div class="modal-body">
	      <div style="height: 200px; overflow: auto;">
		        <table id="tbl-sol" class="table table-bordered table-striped">
		        	<tr>
		        		<td><strong>#</strong></td>
		        		<td><strong>Lugar D.</strong></td>
		        		<td><strong>Fecha Desde</strong></td>
		        		<td><strong>Fecha Hasta</strong></td>
		        		<td><strong>Motivo</strong></td>
		        		<td><strong>T. Transporte</strong></td>
		        		<td><strong>Estado S.</strong></td>
		        	</tr>
		        </table>
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
	      </div>
	    </div>
	  </div>
	</div>
   <!--FIN DE MODAL LISTA_SOLI-->
   <!--COMIENZO DE MODAL CIU-->
   		<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" 
   			aria-labelledby="mySmallModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-sm">
		    <div class="modal-content">
		      <div class="modal-header">
		      	<h4 class="modal-title">Cargar nueva ciudad</h4>
		      </div>
		      <div class="modal-body">
		      		<form>
					  <div class="form-group">
					    <label for="ciudad">Nombre de la ciudad</label>
					    <input type="text" class="form-control" id="ciu_nue" placeholder="Introduzca la nueva ciudad">
					  </div>
					</form>
					<div class="alert alert-success" id="sin_error_ciu" role="alert"><strong>Listo!</strong> Se cargo la ciudad</div>
					<div class="alert alert-danger" id="error_ciu" role="alert"><strong>ERROR!</strong> No se pudo cargar la ciudad</div>
					<div class="alert alert-warning" id='error_vaci' role="alert"><strong>ERROR!</strong> Existen campos vacios</div>
		      </div>
		      <div class="modal-footer">
		    	<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        		<button type="button" class="btn btn-primary" onclick="cargarCiudad();">Cargar Ciudad</button>
		    </div>
		    </div>
		  </div>
		</div>
   <!--FIN DE MODAL CIU-->
</div>
<script src="js/jquery-1.11.1.min.js"></script>
<script src="bootstrap-3.2.0-dist/js/bootstrap.js"></script>
<script src="js/jquery-ui-1.10.3.custom.js"></script>
<script src="../controladores/js/empleado.js"></script>
<script src="../controladores/js/viatico.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<script src="../js/sesion.js"></script>
<script src="js/comunes.js"></script>
<script>
	//listarCiudadesSoli();
		function mostrarModal(titulo, msg, funcion){
			$('#myModalLabel').html(titulo);
			$('#mensajeModal').html(msg);
			$('#btnAccionModal').unbind('click').click({funcion:funcion}, function(eventObject){
				eval(eventObject.data.funcion); //eval('cerrarSesion();');
			});
			$('#myModal').modal('show');
		}
		$('#usuario_aux').tooltip('show');
		$("div[id^='datetimepicker']").datepicker({
	   			format: 'yyyy-mm-dd',
	   			language: "es",
	    		autoclose:true
		});
		//bloquearBotonesUsu();
		//$('#ced_aux').tooltip('show');


		$("#otro").attr('disabled', true);
		$("input[name='radtrans']").click(function(){
			if ($(this).attr('id') == 'transo') {
				 $("#otro").removeAttr('disabled');
			}else{
				$("#otro").attr('disabled', true);
				$("#otro").val('');
			}
		});

		$("#otrovu").attr('disabled', true);
		$("input[name='radtransvu']").click(function(){
			if ($(this).attr('id') == 'transovu') {
				 $("#otrovu").removeAttr('disabled');
			}else{
				$("#otrovu").attr('disabled', true);
				$("#otrovu").val('');
			}
		});


		$(document).ready(function(){
	        $("#sin_error_ciu").hide();
	    	$("#error_ciu").hide();
	    	$("#error_vaci").hide();
	    	listarEstados();
	    });
</script>
</body>
</html>