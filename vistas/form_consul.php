<?php
session_start();
    if (!$_SESSION['usuarioOnline'] || !$_SESSION['permitido_online']) {
        header('location: ../login.php');
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Sistema de Gestión de Viaticos onLine</title>
    <link href="css2/css/bootstrap.css" rel="stylesheet">
    <link href="css2/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
    <link href="css2/css/plugins/timeline.css" rel="stylesheet">
    <link href="css2/css/sb-admin-2.css" rel="stylesheet">
    <link href="css2/css/plugins/morris.css" rel="stylesheet">
    <link href="css2/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand text-danger" href="index_Online.php">Sistema de Gestión de Viáticos</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>Salir del Sistema<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> <?=$_SESSION['realname']?></a></li>
                        <li class="divider"></li>
                        <li><a href="#" onClick='cerrarSessionOnline();'><i class="fa fa-sign-out fa-fw"></i> Cerrar Sesión</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a class="active" href="index_Online.php"><i class="glyphicon glyphicon-home"></i> Inicio</a>
                        </li>
                        <li>
                            <a href="#" class="dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true"><i class="glyphicon glyphicon-tasks"></i> Gestión del Viático<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                <li>
                                    <a href="#" onclick="window.open('http://www.me.gob.ve/servicios/recibo/consultacopia.php/')"><span class="glyphicon glyphicon-download-alt"></span> Descargar recibo de pago</a>
                                </li>
                                <li>
                                    <a href="form_soli_onli.php"><span class="glyphicon glyphicon-pencil"></span> Generar Solicitud de Viático</a>
                                </li>
                                <li>
                                    <a href="form_consul.php"><span class="glyphicon glyphicon-search"></span> Consultar Viáticos</a>
                                </li>
                                <li>
                                    <a href="form_cal_onli.php"><span class="glyphicon glyphicon-edit"></span> Generar Cálculo del Viatico</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
<!--FIN DEL MENU-->
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header text-danger">SIVIZEES Online - Consultas de Viáticos Realizados</h3>
                </div>
            </div>
            <div class="col-lg-12">
               <div class="row">
                    <form class="form-inline">
                        <div class="form-group">
                            <strong>Tipo de Consulta</strong>
                            <select id="consul" class='form-control' onchange="verificarSelect($('#consul').val());">
                                <option value="0" checked>Seleccione</option>SIVIZEES Online - Consultas de Viáticos Realizados
                                <option value="1">Historial Completo de Viáticos</option>
                                <option value="2">Solicitud Específico</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <strong>Cédula</strong>
                            <input type="text" onkeypress='return soloNum(event);' placeholder='Cédula del Empleado' id="cedu" class='form-control'>
                        </div>
                        <div class="form-group">
                            <strong>Código de Solicitud</strong>
                            <input type="text" onkeypress='return soloNum(event);' placeholder='Código de la solicitud' id="cod" class='form-control'>
                        </div><br><br>
                        <button type='button' onclick='historialDeViaticos();' class='btn btn-danger'>Realizar Consulta <span class="glyphicon glyphicon-list-alt"></span></button>
                    </form>
                    <div class="col-lg-12">
                        &nbsp;
                    </div>
               </div>
            <div class="table-responsive">
            <div style='height: 330px; overflow:auto;'>
            <table class="table table-bordered table-hovered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>N°</th>
                        <th>Cédula</th>
                        <th>Empleado</th>
                        <th>Motivo</th>
                        <th>Destino</th>
                        <th>F. De Creación</th>
                        <th>F. Desde</th>
                        <th>F. Hasta</th>
                        <th>Statud del Viático</th>
                    </tr>
                </thead>
                <tbody id="tbl-his"></tbody>
            </table>
            </div>
          </div>
            </div>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <script src="js/jquery-1.11.2.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery-ui-1.10.3.custom.js"></script>
    <script src="js/plugins/metisMenu/metisMenu.min.js"></script>
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>
    <script src="js/sb-admin-2.js"></script>
    <script src="../controladores/js/empleado.js"></script>
    <script src="../controladores/js/viatico.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    <script src="../js/sesion.js"></script>
    <script src="js/comunes.js"></script>
    <script>
    //listarCiudadesSoli();
     listarEstados();-
        function mostrarModal(titulo, msg, funcion){
            $('#myModalLabel').html(titulo);
            $('#mensajeModal').html(msg);
            $('#btnAccionModal').unbind('click').click({funcion:funcion}, function(eventObject){
                eval(eventObject.data.funcion); //eval('cerrarSesion();');
            });
            $('#myModal').modal('show');
        }
        $('#usuario_aux').tooltip('show');
        $("div[id^='datetimepicker']").datepicker({
                format: 'yyyy-mm-dd',
                language: "es",
                autoclose:true
        });
        //bloquearBotonesUsu();
        //$('#ced_aux').tooltip('show');


        $("#otro").attr('disabled', true);
        $("input[name='radtrans']").click(function(){
            if ($(this).attr('id') == 'transo') {
                 $("#otro").removeAttr('disabled');
            }else{
                $("#otro").attr('disabled', true);
                $("#otro").val('');
            }
        });

        $("#otrovu").attr('disabled', true);
        $("input[name='radtransvu']").click(function(){
            if ($(this).attr('id') == 'transovu') {
                 $("#otrovu").removeAttr('disabled');
            }else{
                $("#otrovu").attr('disabled', true);
                $("#otrovu").val('');
            }
        });


        $(document).ready(function(){
            $("#sin_error_ciu").hide();
            $("#error_ciu").hide();
            $("#error_vaci").hide();
        });
        function verificarSelect(value){

             $("#cod").removeAttr('disabled');
             $("#cedu").removeAttr('disabled');
             $("#cod").val('');
             $("#cedu").val('');
            
            if (value == 1) {
                $("#cod").attr("disabled", true);
            }else if(value == 2){
                //$("#cedu").attr("disabled", true);
            }
        }
</script>
</body>
</html>
