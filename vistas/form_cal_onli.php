<?php
session_start();
    if (!$_SESSION['usuarioOnline'] || !$_SESSION['permitido_online']) {
        header('location: ../login.php');
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Sistema de Gestión de Viaticos onLine</title>
    <link href="css2/css/bootstrap.css" rel="stylesheet">
    <link href="css2/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
    <link href="css2/css/plugins/timeline.css" rel="stylesheet">
    <link href="css2/css/sb-admin-2.css" rel="stylesheet">
    <link href="css2/css/plugins/morris.css" rel="stylesheet">
    <link href="css2/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand text-danger" href="index_Online.php">Sistema de Gestión de Viáticos</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>Salir del Sistema<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> <?=$_SESSION['realname']?></a></li>
                        <li class="divider"></li>
                        <li><a href="#" onClick='cerrarSessionOnline();'><i class="fa fa-sign-out fa-fw"></i> Cerrar Sesión</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a class="active" href="index_Online.php"><i class="glyphicon glyphicon-home"></i> Inicio</a>
                        </li>
                        <li>
                            <a href="#" class="dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true"><i class="glyphicon glyphicon-tasks"></i> Gestión del Viático<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                <li>
                                    <a href="#" onclick="window.open('http://www.me.gob.ve/servicios/recibo/consultacopia.php/')"><span class="glyphicon glyphicon-download-alt"></span> Descargar recibo de pago</a>
                                </li>
                                <li>
                                    <a href="form_soli_onli.php"><span class="glyphicon glyphicon-pencil"></span> Generar Solicitud de Viático</a>
                                </li>
                                <li>
                                    <a href="form_consul.php"><span class="glyphicon glyphicon-search"></span> Consultar Viáticos</a>
                                </li>
                                <li>
                                    <a href="form_cal_onli.php"><span class="glyphicon glyphicon-edit"></span> Generar Cálculo del Viatico</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
<!--FIN DEL MENU-->
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header text-danger">SIVIZEES - Gestión de Cálculo de Viáticos</h3>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-3">
                     <div class="input-group pull-right">
                      <input type="text" onkeypress="return soloNum(event);" id="nSoli" data-placement="bottom" data-toggle="tooltip" data-placement="bottom"
                       title="Usuario de sistema" class="form-control" placeholder='N° Solicitud'>
                      <span class="input-group-btn">
                        <button class="btn btn-default" onclick="buscarSolicitud($('#nSoli').val());" type="button"><span class="glyphicon glyphicon-search"></span></button>
                      </span>
                   </div>
                </div>
                <div class="col-12">
                    <div class="row">
                        <div role="tabpanel">
                          <!-- Nav tabs -->
                          <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Datos del Solicitante</a></li>
                            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Datos de la Misión</a></li>
                            <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Relación de Gatos</a></li>
                          </ul>

                          <!-- Tab panes -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="home">
                             <h4 class="text-danger"><strong>Datos del Solicitante</strong></h4>
                    <form class="form-inline">
                        <div class="form-group">
                           <strong>Cédula:</strong> &nbsp; <input type="text" class="form-control" id="ced_em" placeholder="Cédula" readonly="readonly" data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom">
                          </div>
                          <div class="form-group">
                           <strong>Nombre:</strong> &nbsp; <input type="text" class="form-control" id="nom_em" placeholder="Nombre" readonly="readonly">
                          </div>
                          <div class="form-group">
                           <strong>Apellido:</strong> &nbsp; <input type="text" class="form-control" id="ape_em" placeholder="Apellido" readonly="readonly"><br><br>
                          </div>
                         <div class="form-group">
                           &nbsp;<strong>Cargo:</strong> &nbsp; <input type="text" class="form-control" id="cargo_em" placeholder="Cargo que ocupa" readonly="readonly">
                         </div>
                         <div class="form-group">
                           <strong>Estado:</strong> &nbsp;&nbsp;&nbsp; <input type="text" class="form-control" id="esta" placeholder="Estado" readonly="readonly">
                         </div>
                         <div class="form-group">
                          <strong> Ciudad:</strong> &nbsp;&nbsp;&nbsp; <input type="text" class="form-control" id="ciu" placeholder="Ciudad" readonly="readonly">
                         </div>
                         <div class="form-group"><br>
                          <strong> Dirección:</strong> &nbsp; <input size="100" type="text" class="form-control" id="direc" placeholder="Dirección" readonly="readonly">
                         </div>
                          <br> <strong>Sexo:</strong>
                          <label class="radio-inline">
                          <input type="radio" disabled="disabled" name="inlineRadioOptions" id="sexoM" value="option1"> M
                          <input type="hidden" id="hidempleado">
                        </label>
                        <label class="radio-inline">
                          <input type="radio" disabled="disabled" name="inlineRadioOptions" id="sexoF" value="option2"> F
                        </label>
                    </form>
                    </div>
                            <div role="tabpanel" class="tab-pane" id="profile">
                            <h4 class="text-danger"><strong>Datos de la Misión</strong></h4>
                    <form class="form-inline">
                        <!--rgehteth-->
                        <strong>Estado Origen</strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select class="form-control" id="esta_ori">
                                                                                    <option checked value="0">Seleccione</option>
                                                                                </select>
                      <strong>Ciudad de Origen</strong> <select class="form-control" id="ciu_ori">
                                            <option checked value="0">Seleccione</option>
                                       </select>
                                      <strong>Estado Destino</strong> <select class="form-control" id="esta_des">
                                            <option checked value="0">Seleccione</option>
                                       </select><br><br>
                        <strong>Ciudad de Destino</strong> <select class="form-control" id="ciu_des" >
                                                <option checked value="0">Seleccione</option>
                                          </select>
                                          <br><br>
                            <div class="form-group">
                               <strong>Lugar de Destino</strong> <input type="text" readonly="readonly" class="form-control" size="100" id="lugardes" placeholder="Lugar de Destino"><br><br>
                                <div class="form-group">
                                    <strong>Fecha de la misión: Desde</strong>&nbsp;
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input type='text' readonly="readonly" class="form-control hab" id="fedesde" />
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar">
                                            </span>
                                        </span>
                                    </div>
                              </div>
                              <div class="form-group">
                                    &nbsp;&nbsp;<strong>Hasta</strong>&nbsp;
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input type='text' readonly="readonly" class="form-control hab" id="fehasta"/>
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar">
                                            </span>
                                        </span>
                                    </div>
                              </div><br><br>
                              </div>
                              <input type="hidden" id="idHidden">
                               <div class="form-group">
                                 <strong>Motivo del Viaje</strong> <textarea name="" readonly="readonly" id="motivo" cols="50" rows="2" class="form-control" placeholder='Motivo del viaje'></textarea>
                               </div><br><br>
                                <strong>Transporte Utilizado de Ida:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <label class="radio-inline">
                                  <input type="radio" disabled="disabled" name="radtrans" checked="checked" id="transa" value="option1"> <strong>Aéreo</strong>
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" disabled="disabled" name="radtrans" id="transt" value="option2"> <strong>Terrestre</strong>
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" disabled="disabled" name="radtrans" id="transo" value="option3"> <strong>Otros</strong>
                                </label>
                                <div class="col-lg-8">
                                    &nbsp;
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="otro" placeholder="Otro">
                                </div><br><br>

                        <strong>Transporte Utilizado de Vuelta:</strong>
                        <label class="radio-inline">
                          <input type="radio" name="radtransvu" disabled="disabled" checked="checked" id="transavu" value="option1"> <strong>Aéreo</strong>
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="radtransvu" disabled="disabled" id="transtvu" value="option2"> <strong>Terrestre</strong>
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="radtransvu" disabled="disabled" id="transovu" value="option3"> <strong>Otros</strong>
                        </label>
                        <div class="form-group">
                            <input type="text" class="form-control" disabled="disabled" id="otrovu" placeholder="Otro">
                        </div>
                        <!--vfrdgertrt-->
                    </form>
                    </div>
                            <div role="tabpanel" class="tab-pane" id="messages">
                            <h4 class="text-danger"><strong>Relacion de Gastos</strong></h4>
                    <form class="form-inline">
                    <div class="form-group">
                            <strong>Proyecto:</strong> <select name="" class="form-control" id="proyec_presu" onchange="buscarCuentasProyecto(this.value);">
                                        <option value="0">Seleccione</option>
                                      </select>
                        </div>
                            <div class="form-group">
                                <strong>Imputación P.</strong> <input type="text" readonly="readonly" class="form-control" id="impu_presu">
                            </div>
                        <!--div class="form-group">
                            Proyecto: <select name="" class="form-control" id="proyec_presu" onchange="buscarCuentasProyecto(this.value);">
                                        <option value="0">Seleccione</option>
                                      </select>
                        </div-->
                         <div class="col-lg-12">
                            &nbsp;
                        </div>
                        <div class="form-group">
                            <strong>Cuenta:</strong> <input type="text" readonly="readonly" id="cuenta_presu" class="form-control">
                        </div>
                    </form>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <form id="form_precios_via" class="form-inline" role="form" action="">
                        <hr>
                        <div class="form-group">
                            <strong>Pasaje de Ida</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <select name="selprecio_ida" id="selprecio_ida" class="form-control" onchange="verificarIdaVu(this.id);">
                                <option id="preciovia_0_0" value="">Selecione</option>
                                <option id="preciovia_1_0" value="0">Aéreo</option>
                                <option id="preciovia_5_0" value="0">Terrestre</option>
                                <option id="preciovia_6_0" value="0">Otro</option>
                            </select>
                            <input id="preciovia_ida" size="8" class="form-control" type="text" readonly="readonly">
                            <input id="preciovia_0" class="form-control" type="hidden">
                        </div>
                        <div class="form-group">
                            <strong>Monto</strong> <input id="montovia_0" onkeypress="return AcceptNum(event,this.id,false,false);" size="5" type="text" class="form-control mitotal">
                        </div>
                        <div class="form-group">
                            <strong>Cantidad</strong>
                            <input id="cantvia_0" size="3" class="form-control mitotal" type="text"  onkeypress="return soloNum(event);" >
                        </div>
                        <div class="form-group">
                            <strong>Total</strong>
                            <input id="totalvia_0" size="3" class="form-control" type="text" readonly="">
                        </div>
                        <hr>
                        <div class="form-group">
                            <strong>Pasaje de Vuelta</strong>
                            <select name="selprecio_vu" id="selprecio_vu" class="form-control" onchange="verificarIdaVu(this.id);">
                                <option id="preciovia_0_1" value="">Selecione</option>
                                <option id="preciovia_1_1" value="0">Aéreo</option>
                                <option id="preciovia_5_1" value="0">Terrestre</option>
                                <option id="preciovia_6_1" value="0">Otro</option>
                            </select>
                            <input id="preciovia_vu" size="8"  class="form-control"  type="text" readonly="readonly">
                            <input id="preciovia_1" class="form-control" type="hidden">
                        </div>
                        <div class="form-group">
                            <strong>Monto</strong> <input id="montovia_1" onkeypress="return AcceptNum(event,this.id,false,false);" size="5" type="text" class="form-control mitotal">
                        </div>
                        <div class="form-group">
                            <strong>Cantidad</strong> 
                            <input id="cantvia_1" size="3" class="form-control mitotal" type="text" onkeypress="return soloNum(event);" >
                        </div>
                        <div class="form-group">
                            <strong>Total</strong> 
                            <input id="totalvia_1" size="3" class="form-control" type="text" readonly="">
                        </div>
                        <hr>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <div class="form-group">
                            <strong>Comida</strong> 
                            <input id="preciovia_2" size="8" class="form-control" type="text" readonly="readonly">
                        </div>
                        <div class="form-group">
                            <strong>Monto</strong> <input id="montovia_2" onkeypress="return AcceptNum(event,this.id,false,false);" size="5" type="text" class="form-control mitotal">
                        </div>
                        <div class="form-group">
                            <strong>Cantidad</strong> 
                            <input id="cantvia_2" size="3" class="form-control mitotal" type="text"  onkeypress="return soloNum(event);" >
                        </div>
                        <div class="form-group">
                            <strong>Total</strong>
                            <input id="totalvia_2" size="3" class="form-control" type="text" readonly="">
                        </div>
                        <hr>
                        <div class="form-group">
                            <strong>Hospedaje</strong> 
                            <input id="preciovia_3" size="8" class="form-control" type="text" readonly="readonly">
                        </div>
                        <div class="form-group">
                            <strong>Monto</strong> <input id="montovia_3" onkeypress="return AcceptNum(event,this.id,false,false);" size="5" type="text" class="form-control mitotal">
                        </div>
                        <div class="form-group">
                            <strong>Cantidad </strong>
                            <input id="cantvia_3" size="3" class="form-control mitotal" type="text" onkeypress="return soloNum(event);">
                        </div>
                        <div class="form-group">
                            <strong>Total</strong> 
                            <input id="totalvia_3" size="3" class="form-control" type="text" readonly="">
                        </div>
                        <div class="col-lg-12"> </div>
                        <hr>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <div class="form-group">
                            <strong>Taxi</strong> 
                            <input id="preciovia_4" size="8" class="form-control" type="text" readonly="readonly">
                        </div>
                        <div class="form-group">
                            <strong>Monto</strong> <input id="montovia_4"  onkeypress="return AcceptNum(event,this.id,false,false);" size="5" type="text" class="form-control mitotal">
                        </div>
                        <div class="form-group">
                            <strong>Cantidad </strong>
                            <input id="cantvia_4" size="3" class="form-control mitotal" type="text" onkeypress="return soloNum(event);">
                        </div>
                        <div class="form-group">
                            <strong>Total </strong>
                            <input id="totalvia_4" size="3" class="form-control" type="text" readonly="">
                        </div>
                    </form>
                            </div>
                        </div>
                    </div><br><br>
                    <div class="col-lg-3">
                        <form action="" class="form-inline">
                        <div class="form-group">
                        <strong>Total Viáticos</strong> <input type="text" readonly class="form-control pu" id="preciTotal" placeholder=''>
                        </div><br><br>
                        <div class="form-group">
                        <button type="button" onclick="guardarGastos();"
                        class="btn btn-primary pull-right">Guardar <span class="glyphicon glyphicon-floppy-disk"></span>
                    </button>
                    </div>
                    </form>
                    </div>
                    
                    <div class="col-lg-12">
                        &nbsp;
                    </div>
                    </div>
                          </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/comunes.js"></script>
    <script src="bootstrap-3.2.0-dist/js/bootstrap.js"></script>
    <script src="js/jquery-ui-1.10.3.custom.js"></script>
    <script src="../controladores/js/empleado.js"></script>
    <script src="../controladores/js/viatico.js"></script>
    <script src='../controladores/js/presupuesto.js'></script>
    <script src="js/bootstrap-datepicker.js"></script>
    <script src="js/comunes.js"></script>
    <script src="../js/sesion.js"></script>

    <script>
        buscarProyectos();
        listarEstados();
        function mostrarModal(titulo, msg, funcion){
            $('#myModalLabel').html(titulo);
            $('#mensajeModal').html(msg);
            $('#btnAccionModal').unbind('click').click({funcion:funcion}, function(eventObject){
                eval(eventObject.data.funcion); //eval('cerrarSesion();');
            });
            $('#myModal').modal('show');
        }
        $('#usuario_aux').tooltip('show');
        $("div[id^='datetimepicker']").datepicker({
                format: 'yyyy-mm-dd',
                language: "es",
                autoclose:true
        });
        //bloquearBotonesUsu();
        //$('#ced_aux').tooltip('show');
        $("#otro").attr('disabled', true);
        $("#transo").click(function(){
          $("#otro").removeAttr('disabled');
        });
        //generarInputsPrecios();
        $('#esta_ori').attr("disabled", true);
        $('#esta_des').attr("disabled", true);
        $('#ciu_ori').attr("disabled", true);
        $('#ciu_des').attr("disabled", true);

        $("input.mitotal").blur(function(event) {
            event.preventDefault();
            var pt = this.id.split('_');
            var indice = pt[1];
            var cant = 0;
            var error = false;
            if($("#cantvia_"+indice).val() != ''){
                cant = $("#cantvia_"+indice).val();
            }
            var monto = 0;
            /*if($("#montovia_"+indice).val() != ''){
                monto = $("#montovia_"+indice).val();
            }*/
            var ind = indice;
                if (indice == 0) {
                    ind = 'ida';
                }else if(indice == 1){
                    ind = 'vu';
                }
            if($.trim($("#preciovia_"+ind).val()) == '' || $.trim($("#preciovia_"+ind).val()) == 0){
                $("#montovia_"+indice).val('');
            }else{
                if($("#montovia_"+indice).val() != ''){
                    monto = $("#montovia_"+indice).val();
                }
                
                var pt2 = $("#preciovia_"+ind).val().split('-');
                var min = parseFloat(pt2[0]);
                var max = parseFloat(pt2[1]);
                if (monto < min || monto > max) {
                    error = true;
                    $("#montovia_"+indice).val('');
                };
            }
            $("#totalvia_"+indice).val(parseFloat(monto)*parseInt(cant,10));
            calcularTotal();

            if (error) {
                //alert("El monto debe estar comprendido entre "+min+" y "+max);

            };
        });

        /*$("input[id^='montovia_']").blur(function(event) {
            var pt = this.id.split('_');
            var indice = pt[1];

            
            }
        });*/

        function calcularTotal(){
            var total = 0;
            $("input[id^='montovia_']").each(function(index, el) {
                var pt = this.id.split('_');
                var indice = pt[1];
                var cant = 0;
                if($("#cantvia_"+indice).val() != ''){
                    cant = $("#cantvia_"+indice).val();
                }
                var monto = 0;
                if($("#montovia_"+indice).val() != ''){
                    monto = $("#montovia_"+indice).val();
                }
                total = parseFloat(total)+(parseFloat(monto)*parseInt(cant,10));
                $("#totalvia_"+indice).val(parseFloat(monto)*parseInt(cant,10));
            });
            $('#preciTotal').val(total);
        }

        function verificarIdaVu(id){
            var pt = id.split('_');
            var indice = pt[1];
            var ind = 0;
            if (indice == 'vu') {
                ind = 1;
            };
            $('#preciovia_'+indice).val($('#selprecio_'+indice).val());  
            if ($('#selprecio_'+indice).val() == '' || $('#selprecio_'+indice).val() == 0) {
                $("#montovia_"+ind).val('');
            }else{
                
                $('#preciovia_'+ind).val($('#selprecio_'+indice).find('option:selected').attr('id'));
            }
            calcularTotal();
        }
    </script>
</body>
</html>
