<?php
    include_once ('libs/control_sesion.php');

    if ($_SESSION['tipo'] == 3 || $_SESSION['nivel'] == 1) {
    	?>
    	<script>
    		alert("Usted no tiene acceso a este segmento del sistema");
    		location.href='index.php';
    	</script>
    	<?php
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Gestión de Usuarios</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="stylesheet" href="bootstrap-3.2.0-dist/css/bootstrap.css">
	<link rel="stylesheet" href="bootstrap-3.2.0-dist/css/datepicker.css">
	<link rel="stylesheet" href="bootstrap-3.2.0-dist/css/bootstrap-theme.css">
	<!--link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap-responsive.css"-->
	<!--style>
		/*Escritorio*/
		@media(min-width: 1200px){
			 body{color: blue;}
		}
		/*Escritorio pequeno o tablet*/
		@media(min-width: 768px) and (max-width: 979px){
        	  body{color: green;}
		}/*tablet o smartphone*/
		@media(max-width: 767px){
			  body{color: red;}
		}/*smartphone*/
		@media(max-width: 480px){
			  body{color: orange;}
		}
	</style-->
</head>
<body>
	<div class="container-fluid"><!--Menu-->
		<div class="row">
			<div class="col-md-12">
			<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			  <div class="container-fluid">
			    <!-- Brand and toggle get grouped for better mobile display -->
			    <div class="navbar-header">
			      <button type="button" class="navbar-toggle" data-toggle="collapse"
			      data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
			      </button>
			    <a class="navbar-brand" href="index.php"><img  src="img/SIVIZE LOGO-menu.png"> SIVIZEES</a>
			    </div>

			    <!-- Collect the nav links, forms, and other content for toggling -->
			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			      <ul class="nav navbar-nav">
			        <!--li><a href="#">Link</a></li>
			        <li><a href="#">Link</a></li-->
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			          <span class="glyphicon glyphicon-user"></span> Usuarios<span class="caret"></span></a>
			          <ul class="dropdown-menu" role="menu">
			            <!--li><a href="form_usu_ac.php"><span class="glyphicon glyphicon-retweet">
			            	</span> Gestión de Usuarios</a></li-->
			            <!--li><a href="#myModal1" data-toggle="modal" onclick="listarUsuarios();"><span class="glyphicon glyphicon-align-justify"></span> Listar Usuarios</a></li-->
			            <li><a href="form_usu_cre.php">
			            	<span class="glyphicon glyphicon-plus-sign"></span> Gestión de Usuario</a></li>
			          </ul>
			        </li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			          <span class="glyphicon glyphicon-briefcase"></span> Empleados<span class="caret"></span></a>
			          <ul class="dropdown-menu" role="menu">
			            <li><a href="form_emp_re.php"><span class="glyphicon glyphicon-pencil"></span> Registro de Empleado</a></li>
			            <li><a href="form_emp_ac.php"><span class="glyphicon glyphicon-refresh"></span> Actualizar datos del empleado</a></li>
			          </ul>
			        </li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			          	<span class='glyphicon glyphicon-usd'></span> Presupuesto<span class="caret"></span></a>
			          <ul class="dropdown-menu" role="menu">
			            <li><a href="form_presu_crear_parti.php"><span class="glyphicon glyphicon-upload"></span> Crear Partida</a></li>
			            <li><a href="form_presu_crear_proyec.php"><span class="glyphicon glyphicon-upload"></span> Crear Proyecto</a></li>
			            <li><a href="form_presu_gestion_movi.php"><span class="glyphicon glyphicon-list-alt"></span> Gestionar Movimiento</a></li>
			            <li><a href="form_via_pre.php"><span class="glyphicon glyphicon-pushpin"></span> Gestión de Precios de Viáticos</a></li>
			          </ul>
			        </li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			          	<span class='glyphicon glyphicon-plane'></span> Viaticos<span class="caret"></span></a>
			          <ul class="dropdown-menu" role="menu">
			          <li><a href="#" onclick="window.open('http://www.me.gob.ve/servicios/recibo/consultacopia.php');"><span class="glyphicon glyphicon-download-alt"></span> Descargar Recibo de Pago</a></li>
			            <li><a href="form_via_soli.php"><span class="glyphicon glyphicon-folder-open"></span> Gestión de Solicitud de Viáticos</a></li>
			             <li><a href="lista_soli.php"><span class="glyphicon glyphicon-align-justify"></span> Gestión de Aprobación de Viáticos</a></li>
			             <li><a href="form_via_cal.php"><span class="glyphicon glyphicon-tasks"></span> Gestión de Cálculo de Viáticos</a></li>
			             <li><a href="lista_calc.php"><span class="glyphicon glyphicon-align-justify"></span> Gestión de Aprobación de Cálculo de Viáticos</a></li>
			             <li><a href="form_pago_via.php"><span class="glyphicon glyphicon-asterisk"></span> Pago de Solicitudes</a></li>
			            <li><a href="consulStatusSol.php"><span class="glyphicon glyphicon-list-alt"></span> Status de la Solicitud</a></li>
			            <li><a href="form_ciu.php"><span class="glyphicon glyphicon-globe"></span> Activar/Desactivar Ciudades</a></li>
			            <!--li><a href="#">Crear Periodo</a></li>
			            <li><a href="#">Crear Mes</a></li-->
			          </ul>
			        </li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			          	<span class='glyphicon glyphicon-list-alt'></span> Reportes<span class="caret"></span></a>
			          <ul class="dropdown-menu" role="menu">
			            <li><a href="reportes/rptListadoZona.php"><span class="glyphicon glyphicon-list-alt"></span> Listado de Solicitudes por Zona</a></li>
			            <li><a href="reportes/rptListadoDispon.php"><span class="glyphicon glyphicon-list-alt"></span> Listado de Disponibilidades por Partida</a></li>
			            <li><a href="reportes/rptListadoMov.php"><span class="glyphicon glyphicon-list-alt"></span> Listados de Movimientos por Partida</a></li>
			            <li><a href="reportes/rptListadoStatus.php"><span class="glyphicon glyphicon-list-alt"></span> Listados de Solicitudes por Status</a></li>
			            <li><a href="reportes/rptSolicitudesEmp.php"><span class="glyphicon glyphicon-list-alt"></span> Listados de Solicitudes por Empleado</a></li>
			            <li><a href="reportes/rptConsolidadoStatus.php"><span class="glyphicon glyphicon-list-alt"></span> Consolidado de Solicitudes por Status</a></li>
			            <li><a href="reportes/bus_avan.php"><span class="glyphicon glyphicon-search"></span> Busqueda Avanzada de Viáticos</a></li>
			          </ul>
			        </li>
			        <!--li><a href="#"><span class="glyphicon glyphicon-file"></span>Reportes PDF</a></li>
			        <li><a href="#"><span class="glyphicon glyphicon-cloud-download"></span>Descargar Manual</a></li-->
			      </ul>
			      <!--form class="navbar-form navbar-left" role="search">
			        <div class="form-group">
			          <input type="text" class="form-control" placeholder="Search">
			        </div>
			        <button type="submit" class="btn btn-default">Submit</button>
			      </form-->
			      <ul class="nav navbar-nav navbar-right">
			        <!--li><a href="#">Link</a></li-->
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			          	<span class="glyphicon glyphicon-user"></span> <?=$_SESSION['usuario']?><span class="caret"></span></a>
			          <ul class="dropdown-menu" role="menu">
			          <!--li><a href="">Cambiar contraseña</a></li-->
			            <li><a href="#" onclick="mostrarModal('Mensaje de Alerta',
			            '¿Esta seguro de Cerrar la sesión?', 'cerrarSesion();')">
			            	<span class="glyphicon glyphicon-log-out"></span> Cerrar Sesión</a></li>
			          </ul> <!--onclick="cerrarSesion();"-->
			        </li>
			      </ul>
			    </div><!-- /.navbar-collapse data-toggle="modal" data-target="#myModal"  " -->
			  </div><!-- /.container-fluid -->
			</nav>
			</div>
		</div>
	</div><!--Fin Menu-->
	<div class="span12">&nbsp;</div>
	<div class="span12">&nbsp;</div>
	<div class="span12">&nbsp;</div>

  <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;
        	</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title text-center" id="myModalLabel"></h4>
      </div>
      <div class="modal-body">
        <p align="center" id="mensajeModal"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="btnAccionModal" >Aceptar</button>
      </div>
    </div>
  </div>
</div><!-- Fin Modal 1 -->
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-3">
			<div class="input-group">
			  <input type="text" placeholder='Usuario del Sistema' id="usuario_aux" data-placement="bottom" data-toggle="tooltip" data-placement="bottom"
	           title="Usuario de sistema" class="form-control">
			  <span class="input-group-btn">
			    <button class="btn btn-default" onclick="buscarUsuario($('#usuario_aux').val());" type="button"><span class="glyphicon glyphicon-search"></span></button>
			  </span>
            </div>
		</div>
	</div>
</div>
<h3 class="text-center text-danger"><strong>Gestión de Usuarios</strong></h3>
<div class="containe-fluid">
<div class="col-lg-2"></div>
<div class="col-lg-8">
<div class="panel panel-danger center-block">
    <div class="panel-heading text-danger">
        <span>Formulario de Gestión de Usuarios</span>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">
            <h4 class="text-danger"><strong>Verificación de Datos del Empleado</strong></h4>
                <form role="form">
                    <div class="form-group">
				    <label for="ejemplo_email_1">Cédula</label>
				    <input type="text" onblur="empleadoExis();" onkeypress="return soloNum(event);" class="form-control" id="ci"
				           placeholder="Introduzca su Cédula">
				    <!--label for="ejemplo_email_1">Cédula del Empleado</label>
				    <input type="text" readonly class="form-control" id="ced"-->
				    <label for="ejemplo_email_1">Nombre </label>
				    <input type="text" readonly class="form-control" id="nom">
				    <label for="ejemplo_email_1">Apellido </label>
				    <input type="text" readonly class="form-control" id="ap">
				    <div class="span5">&nbsp;</div>
				  </div>
				  <input type="hidden" id="hidusu">
                </form>
            </div>
            <!-- /.col-lg-6 (nested) -->
            <div class="col-lg-6">
                <h4 class="text-danger"><strong>Crear Usuario</strong></h4>
                <form role="form">
                    <label for="ejemplo_email_1">Usuario</label>
				    <input type="text"  class="form-control" id="usuario"
				           placeholder="Introduzca Usuario">
				           <label for="ejemplo_email_1">Contraseña de usuario</label>
				    		<input type="password"  class="form-control" id="clave"
				           placeholder="Contraseña de usuario">
				           <label for="ejemplo_email_1">Repita la Contraseña</label>
				    		<input type="password"  class="form-control" id="aux_clave"
				           placeholder="Repetir contraseña">
				    <!--label for="ejemplo_email_1">Contraseña</label>
				    <input type="password"  class="form-control" id="clave"
				           placeholder="Introduzca su Contraseña">
				    <label for="ejemplo_email_1">Repetir Contraseña</label>
				    <input type="password"  class="form-control" id="aux_clave"
				           placeholder="Repetir su Contraseña"-->
				    <label for="ejemplo_email_1">Tipo de Usuario</label>
				    <select name="" class="form-control" id="tipo">
				    	<option value="0">Seleccione</option>
				    	<option value="1">Administrador - Sistema</option>
				    	<option value="2">Administrador</option>
				    	<option value="3">Administrativo</option>
				    </select>
				     <label for="ejemplo_email_1">Niveles de Aprobación</label>
				    <select name="" class="form-control" id="apro">
				    	<option value='0'>Seleccione</optiom>
				    	<option value='1'>Usuario sin privilegio de aprobación</option>
				    	<option value='2'>Aprob. Administración - Administración y Servicio</option>
				    	<option value='3'>Aprob. Jefe de Zona</option>
				    	<option value='4'>Aprob. Div. Planificación y P.</option>
				    	<option value='5'>Aprob. Coord. Verificación y Control</option>
				    	<option value='6'>Aprob. Contraloria Interna</option>
				    </select>
                    </form>
                </div>
                <!-- /.col-lg-6 (nested) -->
            </div>
            <!-- /.row (nested) -->
            <br>
            <div class="btn-group">
				<button type="button" onclick="verificarUsuario();"
						class="btn btn-primary">Guardar Datos <span class="glyphicon glyphicon-floppy-disk"></span></button>
				<button type="button" id="btn_list" data-target="#myModal1"
				        data-toggle="modal" onclick="listarUsuarios();"
				        class="btn btn-success">Listar Usuarios <span class="glyphicon glyphicon-th-list"></span></button>
				<button class="btn btn-warning" onclick="desbloquearFormUsu();" id="btn-modi">Modificar <span class='glyphicon glyphicon-refresh'></span></button>
				<button class="btn btn-danger" id="btn-eli" onclick="eliminarUsuario();">Eliminar <span class='glyphicon glyphicon-remove'></span></button>
				<button class="btn btn-info" id="btn-con" data-toggle="modal" data-target="#myModalcon">Cambiar Contraseña <span class="glyphicon glyphicon-lock"></span></button>
            </div>
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
<div class="col-lg-2"></div>
<!-- /.col-lg-12 -->
   </div>
</div>
<!--MODAL LISTAR-->
	<!-- Modal -->
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel" class="text-danger text-center"><strong>Lista de usuario en el sistema.</strong></h4>
      </div>
      <div class="modal-body">
      <div style="height: 200px; overflow: auto;">
      <div class="table responsive">
        <table id="tbl-usu" class="table table-bordered table-hover">
        <thead>
          <tr>
        	<td>#</td>
        	<td>Usuario</td>
        	<td>Tipo</td>
        	<td>Aprobador</td>
        	<td>Nivel de Aprobación</td>
          </tr>
        </thead>
        <tbody id="tbl_usu">
        </tbody>
        </table>
        </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!--button class="btn btn-default" onclick="holaId();">id</button-->
<!--FIN MODAL LISTAR-->
<!--MODAL CONTRA-->
<div class="modal fade" id="myModalcon" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Cambio de Contraseña</h4>
      </div>
      <div class="modal-body">
      	<form role="form">
		  <div class="form-group erno el">
		    <label for="exampleInputEmail1">Nombre de Usuaro</label>
		    <input type="text" class="form-control" id="usu" placeholder="Usuario">
		  </div>
		  <div class="form-group erno el">
		    <label for="exampleInputPassword1">Contraseña antigua</label>
		    <input type="password" class="form-control" id="passvie" placeholder="Contraseña antigual">
		  </div>
		  <div class="form-group">
		    <label for="exampleInputPassword1">Nueva Contraseña</label>
		    <input type="password" class="form-control" id="passnue" placeholder="Nueva Contraseña">
		  </div>
		  <div class="form-group">
		    <label for="exampleInputPassword1">Repita la Nueva Contraseña</label>
		    <input type="password" class="form-control" id="passnure" placeholder="Repita la Nueva Contraseña">
		  </div>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="location.reload();" data-dismiss="modal">Cerrar</button>
        <button type="button" onclick="cambiarClave();" class="btn btn-primary">Cambiar Contraseña</button>
      </div>
    </div>
  </div>
</div>
<!--FIN MODAL CONTRA-->
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="bootstrap-3.2.0-dist/js/bootstrap.js"></script>
	<script src="../controladores/js/usuario.js"></script>
	<script src="../js/sesion.js"></script>
	<script src="js/comunes.js"></script>
	<script>
		function mostrarModal(titulo, msg, funcion){
			$('#myModalLabel').html(titulo);
			$('#mensajeModal').html(msg);
			$('#btnAccionModal').unbind('click').click({funcion:funcion}, function(eventObject){
				eval(eventObject.data.funcion); //eval('cerrarSesion();');
			});
			$('#myModal').modal('show');
		}
		$('#usuario_aux').tooltip('show');
		bloquearBotonesUsu();
	</script>
</body>
</html>