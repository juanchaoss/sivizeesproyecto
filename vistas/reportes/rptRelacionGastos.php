<?php
include_once ("../../modelo/constante.php");
	include_once ("../../modelo/clases/Fachada.php");
	include_once ("../../modelo/clases/funciones_php.php");
	include_once ("../../modelo/Viatico.php");
	include_once ("../../modelo/Presupuesto.php");


$bd = new Fachada();
$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

$idSoli=$_GET['idSoli'];

//buscamos los datos de la solicitud
$sql = "SELECT S.*, P.cedula_per, P.nombre_per, P.apellido_per, E.cargo, PR.nombre_pro, PA.*, G.*, V.*
FROM solicitud_via AS S JOIN empleado AS E ON(E.id_em=S.id_em) 
JOIN persona AS P ON (P.id_per=E.id_per) JOIN GASTO AS G ON(G.id_soli=S.id_soli)
JOIN proyecto AS PR ON(G.id_pro=PR.id_pro) JOIN partida AS PA ON (PR.id_partida=PA.id_partida)
JOIN viatico AS V ON (G.id_via=V.id_via)
WHERE S.id_soli = ".$idSoli." AND S.estado_soli>1
ORDER BY G.id_via";
$resultado=$bd->consultar($sql,'ARREGLO');
$num_soli = 0;

ob_end_clean();
require('../../modelo/clases/fpdf/fpdf.php');//OJO, CAMBIAR AQUÍ POR LA RUTA CORRECTA

class PDF_P extends FPDF
{
    function Header()
    {
        $this->Image("img_logo/logo_system.jpg",12,12,20);//OJO, CAMBIAR AQUÍ POR LA RUTA CORRECTA
        $this->SetFont('helvetica','B',13);
        $this->Ln(4);
        $this->Cell(190,5,'VIÁTICOS Y PASAJES','',1,'C',0);
		$this->SetFont('helvetica','',11);
		$this->Ln(4);
    }
    
    function MultiCelda($w,$h,$txt,$border,$align,$fill)
    {
        $x=$this->GetX();
        $y=$this->GetY();
        $this->MultiCell($w,$h,$txt,$border,$align,$fill);
        $this->SetXY($x+$w,$y);
    }
}
$altoFila=6;
$pdf=new PDF_P("P","mm","letter");
$pdf->SetLeftMargin(12);
$pdf->SetTopMargin(20);
$pdf->SetAutoPageBreak(true,15);
$pdf->SetLineWidth(0.2);

$pdf->SetFont('helvetica','',11);
$pdf->SetFillColor(255,255,255);

$pdf->AddPage();

//CONTENIDO DE LA PÁGINA
if ($resultado)
{
	$num_soli = completarCodigoCeros($resultado[0]["num_soli"],10);
	$pdf->SetFont('helvetica','B',11);
	$pdf->Cell(50,$altoFila-2,"IMPUTACIÓN",0,1,'L',1);
	$pdf->Cell(50,$altoFila,"PRESUPUESTARIA:",0,0,'L',1); $pdf->Cell(10,$altoFila,"",'',0,'C',0);
	$pdf->Cell(55,$altoFila,"PROYECTO:",0,0,'L',1); $pdf->Cell(5,$altoFila,"",'',0,'C',0);
	$pdf->Cell(70,$altoFila,"CUENTA:",0,1,'L',1); 
	$pdf->SetFont('helvetica','',11);
	$pdf->Cell(50,$altoFila,$resultado[0]["cuenta_par"],1,0,'L',1); $pdf->Cell(10,$altoFila,"",'',0,'C',0);
	$pdf->Cell(55,$altoFila,mb_strtoupper($resultado[0]["nombre_pro"], 'UTF-8'),1,0,'L',1); $pdf->Cell(5,$altoFila,"",'',0,'C',0);
	$pdf->Cell(70,$altoFila,mb_strtoupper($resultado[0]["descripcion_par"], 'UTF-8'),1,1,'L',1);
	
	$pdf->Ln(5);
	$pdf->SetFont('helvetica','B',11);
	$pdf->Cell(50,$altoFila,"CÉDULA DE IDENTIDAD:",0,0,'L',1); $pdf->Cell(10,$altoFila,"",'',0,'C',0);
	$pdf->SetFont('helvetica','',11);
	$pdf->Cell(55,$altoFila,number_format($resultado[0]["cedula_per"],0,",","."),1,0,'L',1); $pdf->Cell(5,$altoFila,"",'',0,'C',0);
	$pdf->Cell(70,$altoFila,"",0,1,'L',1);
	
	$pdf->Ln(3);
	$pdf->SetFont('helvetica','B',11);
	$pdf->Cell(50,$altoFila,"NOMBRE DEL FUNCIONARIO:",0,0,'L',1); $pdf->Cell(10,$altoFila,"",'',0,'C',0);
	$pdf->SetFont('helvetica','',11);
	$pdf->Cell(130,$altoFila,mb_strtoupper($resultado[0]["nombre_per"].' '.$resultado[0]["apellido_per"], 'UTF-8'),1,1,'L',1);
	
	$pdf->Ln(3);
	$pdf->SetFont('helvetica','B',11);
	$pdf->Cell(50,$altoFila,"CARGO:",0,0,'L',1); $pdf->Cell(10,$altoFila,"",'',0,'C',0);
	$pdf->SetFont('helvetica','',11);
	$pdf->Cell(130,$altoFila,mb_strtoupper($resultado[0]["cargo"], 'UTF-8'),1,1,'L',1);
	
	$longitud=$pdf->GetStringWidth(mb_strtoupper($resultado[0]["motivovia_soli"], 'UTF-8'));
	$numFilas=ceil($longitud/125);
	$alt=$altoFila;
	if($numFilas>1){
		$alt=$altoFila-1;
	}
		
	$pdf->Ln(3);
	$pdf->SetFont('helvetica','B',11);
	$pdf->Cell(50,$altoFila,"MISIÓN:",0,0,'L',1); $pdf->Cell(10,$altoFila,"",'',0,'C',0);
	$x=$pdf->GetX();
	$y=$pdf->GetY();
	$pdf->Line($x, $y, $x+130, $y);
	$pdf->Line($x, $y+35, $x+130, $y+35);
	$pdf->Line($x, $y, $x, $y+35);
	$pdf->Line($x+130, $y, $x+130, $y+35);
	$pdf->SetFont('helvetica','',11);
	$pdf->MultiCelda(130,$alt,mb_strtoupper($resultado[0]["motivovia_soli"]),0,'L',0);
	
	$pdf->SetXY(12,115);
	$pdf->Ln(3);
	$pdf->SetFont('helvetica','B',11);
	$pdf->Cell(50,$altoFila,"LUGAR DE DESTINO:",0,0,'L',1); $pdf->Cell(10,$altoFila,"",'',0,'C',0);
	$pdf->SetFont('helvetica','',11);
	$pdf->Cell(130,$altoFila,mb_strtoupper($resultado[0]["lugardes_soli"], 'UTF-8'),1,1,'L',1);
	
	$pdf->Ln(3);
	$pdf->SetFont('helvetica','B',11);
	$pdf->Cell(50,$altoFila,"DESDE:",0,0,'L',1); $pdf->Cell(10,$altoFila,"",'',0,'C',0);
	$pdf->SetFont('helvetica','',11);
	$pdf->Cell(55,$altoFila,formatearFecha($resultado[0]["fechades_soli"]),1,0,'R',1); $pdf->Cell(5,$altoFila,"",'',0,'C',0);
	$pdf->Cell(70,$altoFila,"",0,1,'L',1);
	
	$pdf->Ln(3);
	$pdf->SetFont('helvetica','B',11);
	$pdf->Cell(50,$altoFila,"HASTA:",0,0,'L',1); $pdf->Cell(10,$altoFila,"",'',0,'C',0);
	$pdf->SetFont('helvetica','',11);
	$pdf->Cell(55,$altoFila,formatearFecha($resultado[0]["fechahas_soli"]),1,0,'R',1); $pdf->Cell(5,$altoFila,"",'',0,'C',0);
	$pdf->Cell(70,$altoFila,"",0,1,'L',1);
	
	$pdf->Ln(5);
	$pdf->SetFont('helvetica','B',13);
	$pdf->Cell(190,$altoFila,"RELACIÓN DE GASTOS",0,1,'C',1);
	$pdf->SetFont('helvetica','',11);
	
	$total = 0;
	if($resultado[0]["tipotrans_soli"]==1){
		$ida="AÉREO";
		$id=1;
	}else if($resultado[0]["tipotrans_soli"]==2){
		$ida="TERRESTRE";
		$id=5;
	}else if($resultado[0]["tipotrans_soli"]==3){
		$ida=mb_strtoupper($resultado[0]["otro"], 'UTF-8');
		$id=6;
	}
	
	for($i=0;$i<count($resultado);$i++)
	{
		if ($resultado[$i]["id_via"]==$id){
			break;
		}
	}

	$pdf->Ln(2);
	$pdf->SetFont('helvetica','B',11);
	$pdf->Cell(190,$altoFila,"PASAJE DE IDA",0,1,'L',0);
	$pdf->SetFont('helvetica','',11);
	$pdf->Cell(30,$altoFila,$ida,0,0,'L',0);
	$pdf->Cell(40,$altoFila,number_format($resultado[$i]["precio"],2,",","."),1,0,'R',1);
	$pdf->Cell(14,$altoFila,"Bs.",0,0,'L',0);
	$pdf->Cell(16,$altoFila,"LAPSO",0,0,'L',0);
	$pdf->Cell(20,$altoFila,$resultado[$i]["lapso"],1,0,'R',0);
	$pdf->Cell(20,$altoFila,"TOTAL",0,0,'R',0);
	$pdf->Cell(40,$altoFila,number_format($resultado[$i]["total"],2,",","."),1,0,'R',0);
	$pdf->Cell(10,$altoFila,"Bs.",0,1,'L',0);
	$total = $total + $resultado[$i]["total"];
	
	if($resultado[0]["tipotransvu_soli"]==1){
		$vuelta="AÉREO";
		$id=1;
	}else if($resultado[0]["tipotransvu_soli"]==2){
		$vuelta="TERRESTRE";
		$id=5;
	}else if($resultado[0]["tipotransvu_soli"]==3){
		$vuelta=mb_strtoupper($resultado[0]["otrovu"], 'UTF-8');
		$id=6;
	}
	
	for($i=0;$i<count($resultado);$i++)
	{
		if ($resultado[$i]["id_via"]==$id){
			break;
		}
	}
	
	$pdf->Ln(2);
	$pdf->SetFont('helvetica','B',11);
	$pdf->Cell(190,$altoFila,"PASAJE DE VUELTA",0,1,'L',0);
	$pdf->SetFont('helvetica','',11);
	$pdf->Cell(30,$altoFila,$vuelta,0,0,'L',0);
	$pdf->Cell(40,$altoFila,number_format($resultado[$i]["precio"],2,",","."),1,0,'R',1);
	$pdf->Cell(14,$altoFila,"Bs.",0,0,'L',0);
	$pdf->Cell(16,$altoFila,"LAPSO",0,0,'L',0);
	$pdf->Cell(20,$altoFila,$resultado[$i]["lapso"],1,0,'R',0);
	$pdf->Cell(20,$altoFila,"TOTAL",0,0,'R',0);
	$pdf->Cell(40,$altoFila,number_format($resultado[$i]["total"],2,",","."),1,0,'R',0);
	$pdf->Cell(10,$altoFila,"Bs.",0,1,'L',0);
	$total = $total + $resultado[$i]["total"];
	
	for($i=0;$i<count($resultado);$i++)
	{
		if($resultado[$i]["mostrar"]==1){
			$pdf->Ln(2);
			$pdf->SetFont('helvetica','B',11);
			$pdf->Cell(30,$altoFila,mb_strtoupper($resultado[$i]["nombre_via"], 'UTF-8'),0,0,'L',0);
			$pdf->SetFont('helvetica','',11);
			$pdf->Cell(40,$altoFila,number_format($resultado[$i]["precio"],2,",","."),1,0,'R',1);
			$pdf->Cell(14,$altoFila,"Bs.",0,0,'L',0);
			$pdf->Cell(16,$altoFila,"LAPSO",0,0,'L',0);
			$pdf->Cell(20,$altoFila,$resultado[$i]["lapso"],1,0,'R',0);
			$pdf->Cell(20,$altoFila,"TOTAL",0,0,'R',0);
			$pdf->Cell(40,$altoFila,number_format($resultado[$i]["total"],2,",","."),1,0,'R',0);
			$pdf->Cell(10,$altoFila,"Bs.",0,1,'L',0);
			$total = $total + $resultado[$i]["total"];
		}
	}

	$pdf->Ln(4);
	$pdf->SetFont('helvetica','B',11);
	$pdf->Cell(140,$altoFila,"TOTAL VIÁTICOS",0,0,'R',0);
	$pdf->Cell(40,$altoFila,number_format($resultado[0]["monto_soli"],2,",","."),1,0,'R',0);
	$pdf->Cell(10,$altoFila,"Bs.",0,1,'L',0);
	$pdf->SetFont('helvetica','',11);

	$pdf->SetXY(12,230);
	$x=$pdf->GetX();
	$y=$pdf->GetY();
	$k=0;
	for($i=0;$i<5;$i++)
	{
		$pdf->Line($x, $y, $x+36, $y);
		$pdf->Line($x, $y+36, $x+36, $y+36);
		$pdf->Line($x, $y, $x, $y+36);
		$pdf->Line($x+36, $y, $x+36, $y+36);
		$x=$x+38.5;
	}
	
	$pdf->SetXY(12,250);
	$pdf->Cell(36,$altoFila-2,"",0,0,'C',0);$pdf->Cell(2.5,$altoFila-2,"",0,0,'C',0);
	$pdf->Cell(36,$altoFila-2,"Coord.",0,0,'C',0);$pdf->Cell(2.5,$altoFila-2,"",0,0,'C',0);
	$pdf->Cell(36,$altoFila-2,"Div.",0,0,'C',0);$pdf->Cell(2.5,$altoFila-2,"",0,0,'C',0);
	$pdf->Cell(36,$altoFila-2,"",0,0,'C',0);$pdf->Cell(2.5,$altoFila-2,"",0,0,'C',0);
	$pdf->Cell(36,$altoFila-2,"",0,1,'C',0);
	
	$pdf->Cell(36,$altoFila-2,"Div. Planificación",0,0,'C',0);$pdf->Cell(2.5,$altoFila-2,"",0,0,'C',0);
	$pdf->Cell(36,$altoFila-2,"Verificación y",0,0,'C',0);$pdf->Cell(2.5,$altoFila-2,"",0,0,'C',0);
	$pdf->Cell(36,$altoFila-2,"Administración y",0,0,'C',0);$pdf->Cell(2.5,$altoFila-2,"",0,0,'C',0);
	$pdf->Cell(36,$altoFila-2,"",0,0,'C',0);$pdf->Cell(2.5,$altoFila-2,"",0,0,'C',0);
	$pdf->Cell(36,$altoFila-2,"Contraloría",0,1,'C',0);
	
	$pdf->Cell(36,$altoFila-2,"y Presupuesto",0,0,'C',0);$pdf->Cell(2.5,$altoFila-2,"",0,0,'C',0);
	$pdf->Cell(36,$altoFila-2,"Control",0,0,'C',0);$pdf->Cell(2.5,$altoFila-2,"",0,0,'C',0);
	$pdf->Cell(36,$altoFila-2,"Servicios",0,0,'C',0);$pdf->Cell(2.5,$altoFila-2,"",0,0,'C',0);
	$pdf->Cell(36,$altoFila-2,"Recibido por",0,0,'C',0);$pdf->Cell(2.5,$altoFila-2,"",0,0,'C',0);
	$pdf->Cell(36,$altoFila-2,"Interna",0,1,'C',0);
	
}
else
{
	$pdf->Ln(2);
	$pdf->Cell(190,5,'NO SE PUEDEN MOSTRAR LOS DATOS','TBLR',1,'C',0);
}

$pdf->AliasNbPages();
$fecha = date("Y-m-d");
$pdf->Output("RELACION DE GASTOS SOL_".$num_soli."-".$fecha.".pdf","I");

?>