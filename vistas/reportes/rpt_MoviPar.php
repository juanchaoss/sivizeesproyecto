 <?php
	/*session_start();

	if (!$_SESSION['usuario'] || !$_SESSION['usuarioOnline']) {
		header('location:../../index.php');
	}*/

	$FECHA=date("d/m/Y h:i a",time());
	include_once ("../../modelo/constante.php");
	include_once ("../../modelo/clases/Fachada.php");
	include_once ("../../modelo/clases/funciones_php.php");
	include_once ("../../modelo/Viatico.php");

	$bd = new Fachada();
	$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

	$partida  = $_GET['partida'];
	$fedesde = $_GET['fedesde'];
	$fehasta = $_GET['fehasta'];

	$criterios='';

			if ($fedesde != '') {
				$fechades = "AND fecha_movi >= '$fedesde'";
				$criterios.= "DESDE: ".formatearFecha($fedesde);
			}

			if ($fehasta != '') {
				$fechahas = "AND fecha_movi <= '$fehasta'";
				$criterios.= " HASTA: ".formatearFecha($fehasta);
			}

			$sql = "SELECT * FROM movimiento AS M join partida AS P ON (P.id_partida = M.id_partida) 
					WHERE M.id_partida = $partida $fechades $fechahas ORDER BY fecha_movi";


	$resultado = $bd->consultar($sql, 'ARREGLO');
	$cuenpart='';
	if ($resultado){
		$despart  = $resultado[0]['descripcion_par'];
		$cuenpart = $resultado[0]['cuenta_par'];
	}
	ob_end_clean();
	require('../../modelo/clases/fpdf/fpdf.php');

	class PDF_P extends FPDF{
	    function Header(){

	        $this->Image("img_logo/logo_system.jpg",12,12,20);
	        $this->Cell(250,5,"Fecha: ".date("d/m/Y h:i a",time()),'',1,'R',0);
	        $this->Cell(250,5,'Página: '.$this->PageNo().' de {nb}','',1,'R',0);
	        $this->Ln(5);
	        $this->SetFont('helvetica','BI',10);
	        $this->Cell(250,5,'ZONA EDUCATIVA DEL ESTADO SUCRE','',1,'C',0);
	        $this->Cell(250,5,'LISTADO DE MOVIENTOS POR PARTIDA','',1,'C',0);
	        $this->Cell(257,5,'PARTIDA: '.$GLOBALS["cuenpart"].' '.$GLOBALS["despart"],'',1,'C',0);
			$this->SetFont('helvetica','BI',9);
			if ($GLOBALS["criterios"]!="") {
				$this->Cell(250,4,$GLOBALS["criterios"],'',1,'C',0); 
			}
	        $this->Ln(5);

	        $this->SetFillColor(204,204,204);
			$this->SetFont('helvetica','B',8);
			$this->Cell(12,$GLOBALS["altoFila"],"#",'TB',0,'C',1); $this->Cell(1,$GLOBALS["altoFila"],"",'',0,'C',0);
			$this->Cell(26,$GLOBALS["altoFila"],"F. Creado",'TB',0,'C',1); $this->Cell(1,$GLOBALS["altoFila"],"",'',0,'C',0);
			$this->Cell(80,$GLOBALS["altoFila"],"Descripción",'TB',0,'C',1); $this->Cell(1,$GLOBALS["altoFila"],"",'',0,'C',0);
			$this->Cell(35,$GLOBALS["altoFila"],"Modo",'TB',0,'C',1); $this->Cell(1,$GLOBALS["altoFila"],"",'',0,'C',0);
			$this->Cell(50,$GLOBALS["altoFila"],"N° Referencia",'TB',0,'C',1); $this->Cell(1,$GLOBALS["altoFila"],"",'',0,'C',0);
			$this->Cell(40,$GLOBALS["altoFila"],"Monto (Bs.)",'TB',1,'C',1);
			$this->SetFillColor(255,255,255);
			$this->SetFont('helvetica','',9.5);
	    }

		function MultiCelda($w,$h,$txt,$border,$align,$fill){
	        $x=$this->GetX();
	        $y=$this->GetY();
	        $this->MultiCell($w,$h,$txt,$border,$align,$fill);
	        $this->SetXY($x+$w,$y);
	    }
	}

	$altoFila=5;
	$pdf=new PDF_P("L","mm","letter");
	$pdf->SetLeftMargin(15);
	$pdf->SetTopMargin(10);
	$pdf->SetAutoPageBreak(true,10);
	$pdf->SetLineWidth(0.2);

	$pdf->SetFont('helvetica','',9.5);
	$pdf->SetFillColor(255,255,255);

	$pdf->AddPage();

	//CONTENIDO DE LA PÁGINA
	if ($resultado){
		$nreg=count($resultado);

		for($i=0;$i<$nreg;$i++){

			$modo = $resultado[$i]['modo_movi'];

                    if ($modo == 0) {
                        $modo = 'Cheque';
                    }else if($modo == 1){
                        $modo = 'Transferencia';
                    }else if($modo == 2){
                        $modo = 'Depósito';
                    }else
                        $modo = 'Traspaso';

			$longitud=$pdf->GetStringWidth($resultado[$i]["descripcion_movi"]);
			$numFilas=ceil($longitud/78);

			if ($pdf->GetY()+($alt*$numFilas) > 195)
			{
				$pdf->AddPage();
			}

			$pdf->Cell(12,$altoFila,$i+1,'T',0,'C',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
			$pdf->Cell(26,$altoFila,formatearFecha($resultado[$i]['fecha_movi']),'T',0,'C',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
			$pdf->MultiCelda(80,$altoFila,$resultado[$i]["descripcion_movi"],'T','L',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
			$pdf->Cell(35,$altoFila,$modo,'T',0,'C',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
			$pdf->Cell(50,$altoFila,$resultado[$i]["num_referencia"],'T',0,'C',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
			$pdf->Cell(40,$altoFila,$resultado[$i]["monto_movi"],'T',0,'R',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
			$pdf->Cell(1,$altoFila*$numFilas,"",0,1,'C',0);
		}

		$pdf->Cell(250,$altoFila,"",'T',1,'C',1);
	}

	else{
		$pdf->Ln(2);
		$pdf->Cell(250,5,'NO HAY REGISTROS QUE MOSTRAR','TBLR',1,'C',0);
	}

	$pdf->AliasNbPages();
	$fecha = date("d-m-Y");
	$pdf->Output("LISTADO DE MOVIMIENTO POR PARTIDA ".$cuenpart."_".$fecha.".pdf","I");