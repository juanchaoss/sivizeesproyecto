 <?php
	/*session_start();

	if (!$_SESSION['usuario'] || !$_SESSION['usuarioOnline']) {
		header('location:../../index.php');
	}*/

	$FECHA=date("d/m/Y h:i a",time());
	include_once ("../../modelo/constante.php");
	include_once ("../../modelo/clases/Fachada.php");
	//include_once ("../../../trans/funciones_php.php");
	include_once ("../../modelo/Viatico.php");

	$bd = new Fachada();
	$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

	$mes       = $_GET['mes'];
	$anio      = $_GET['anio'];
	$filtro    = $_GET['filtro'];
	$cedula_em = $_GET['cedula_em'];
	$fedesde   = $_GET['fedesde'];
	$fehasta   = $_GET['fehasta'];
	$esta_ori  = $_GET['esta_ori'];
	$ciu_ori   = $_GET['ciu_ori'];
	$esta_des  = $_GET['esta_des'];
	$ciu_des   = $_GET['ciu_des'];

	$AND = "";
	$criterios = "";

	if ($filtro != "-") {
		$AND.=" AND A.estado_soli = ".$filtro;
		if($filtro==0){
			$status="Solicitudes recién creadas";
		}else if($filtro==1){
			$status="Solicitudes aprobadas sin cálculo de gastos";
		}else if($filtro==2){
			$status="Solicitudes con gastos aún no aprobados";
		}else if($filtro==3){
			$status="Solicitudes con gastos aprobados";
		}else if($filtro==4){
			$status="Solicitudes pagadas";
		}else if($filtro==5){
			$status="Solicitudes canceladas";
		}

		$criterios.="Status: ".$status.". ";
	}

	if ($cedula_em!="") {
		$AND.=" AND C.cedula_per = '".$cedula_em."'";
		$criterios.="Cédula: ".$cedula_em.". ";
	}

	if ($esta_ori!=0) {
		$AND.=" AND D.id_es = '".$esta_ori."'";
		$query = "SELECT * FROM estado WHERE id_es = $esta_ori";
		$result = $bd->consultar($query, 'ARREGLO');
		if ($result) {
			$nomb_es = $result[0]['nombre_es'];
		}
		$criterios.="Estado Origen: ".$nomb_es.". ";
	}

	if ($ciu_ori!=0) {
		$AND.=" AND E.id_ciu = '".$ciu_ori."'";
	    $query = "SELECT * FROM ciudad WHERE id_ciu = $ciu_ori";
		$result = $bd->consultar($query, 'ARREGLO');
		if ($result) {
			$nomb_ciu = $result[0]['nombre_ciu'];
		}
		$criterios.="Ciudad Origen: ".$nomb_ciu.". ";
	}

	if ($esta_des!=0) {
		$AND.=" AND D.id_es = '".$esta_des."'";
		$query = "SELECT * FROM estado WHERE id_es = $esta_des";
		$result = $bd->consultar($query, 'ARREGLO');
		if ($result) {
			$nomb_es = $result[0]['nombre_es'];
		}
		$criterios.="Estado Destino: ".$nomb_es.". ";
	}

	if ($ciu_des!=0) {
		$AND.=" AND E.id_ciu = '".$ciu_des."'";
	    $query = "SELECT * FROM ciudad WHERE id_ciu = $ciu_des";
		$result = $bd->consultar($query, 'ARREGLO');
		if ($result) {
			$nomb_ciu = $result[0]['nombre_ciu'];
		}
		$criterios.="Ciudad Destini: ".$nomb_ciu.". ";
	}

	$cent=0;
	$periodo="";
	if ($fedesde!="") {
		$AND.=" AND A.fechades_soli = '".$fedesde."'";
		$periodo.=" desde ".$fedesde;
		$cent=1;
	}

	if ($fehasta!="") {
		$AND.=" AND A.fechahas_soli = '".$fehasta."'";
		$periodo.=" hasta ".$fehasta;
		$cent=1;
	}

	if($cent==1){
		$criterios.="Fecha misión:".$periodo.". ";
	}

	$sql = "SELECT * FROM solicitud_via AS A JOIN empleado AS B ON (A.id_em = B.id_em)
	JOIN persona AS C ON (C.id_per = B.id_per) JOIN estado AS D ON (C.id_es = D.id_es) JOIN
	ciudad AS E ON (C.id_ciu = E.id_ciu) WHERE text(A.fe_creado) like '$anio-$mes-%' ".$AND."
	ORDER BY A.num_soli";

	$resultado = $bd->consultar($sql, 'ARREGLO');

	ob_end_clean();
	require('../../modelo/clases/fpdf/fpdf.php');

	class PDF_P extends FPDF{
	    function Header(){

	        $this->Image("img_logo/logo_system.jpg",12,12,20);
	        $this->Cell(257,5,"Fecha: ".date("d/m/Y h:i a",time()),'',1,'R',0);
	        $this->Cell(257,5,'Página: '.$this->PageNo().' de {nb}','',1,'R',0);
	        $this->Ln(5);
	        $this->SetFont('helvetica','BI',10);
	        $this->Cell(257,5,'ZONA EDUCATIVA DEL ESTADO SUCRE','',1,'C',0);
	        $this->Cell(257,5,'LISTADO DE SOLICITUDES','',1,'C',0);
			$this->SetFont('helvetica','BI',9);
			if ($GLOBALS["criterios"]!="") {
				$this->Cell(257,4,$GLOBALS["criterios"],'',1,'C',0); 
			}
	        $this->Ln(5);

	        $this->SetFillColor(204,204,204);
			$this->SetFont('helvetica','B',8);
			$this->Cell(12,$GLOBALS["altoFila"],"#",'TB',0,'C',1); $this->Cell(1,$GLOBALS["altoFila"],"",'',0,'C',0);
			$this->Cell(26,$GLOBALS["altoFila"],"N° Solicitud",'TB',0,'C',1); $this->Cell(1,$GLOBALS["altoFila"],"",'',0,'C',0);
			$this->Cell(20,$GLOBALS["altoFila"],"C.I.",'TB',0,'C',1); $this->Cell(1,$GLOBALS["altoFila"],"",'',0,'C',0);
			$this->Cell(34,$GLOBALS["altoFila"],"Empleado",'TB',0,'C',1); $this->Cell(1,$GLOBALS["altoFila"],"",'',0,'C',0);
			$this->Cell(58,$GLOBALS["altoFila"],"Motivo",'TB',0,'C',1); $this->Cell(1,$GLOBALS["altoFila"],"",'',0,'C',0);
			$this->Cell(50,$GLOBALS["altoFila"],"Destino",'TB',0,'C',1); $this->Cell(1,$GLOBALS["altoFila"],"",'',0,'C',0);
			$this->Cell(25,$GLOBALS["altoFila"],"F. Desde",'TB',0,'C',1); $this->Cell(1,$GLOBALS["altoFila"],"",'',0,'C',0);
			$this->Cell(25,$GLOBALS["altoFila"],"F. Hasta",'TB',1,'C',1);
			$this->SetFillColor(255,255,255);
			$this->SetFont('helvetica','',9.5);
	    }

		function MultiCelda($w,$h,$txt,$border,$align,$fill){
	        $x=$this->GetX();
	        $y=$this->GetY();
	        $this->MultiCell($w,$h,$txt,$border,$align,$fill);
	        $this->SetXY($x+$w,$y);
	    }
	}

	$altoFila=5;
	$pdf=new PDF_P("L","mm","letter");
	$pdf->SetLeftMargin(10);
	$pdf->SetTopMargin(10);
	$pdf->SetAutoPageBreak(true,10);
	$pdf->SetLineWidth(0.2);

	$pdf->SetFont('helvetica','',9.5);
	$pdf->SetFillColor(255,255,255);

	$pdf->AddPage();

	//CONTENIDO DE LA PÁGINA
	if ($resultado){
		$nreg=count($resultado);

		for($i=0;$i<$nreg;$i++){
			$longitud=$pdf->GetStringWidth($resultado[$i]["motivovia_soli"]);
			$numFilas=ceil($longitud/57);

			if ($pdf->GetY()+($alt*$numFilas) > 205){
				$pdf->AddPage();
			}

			$name     = explode(" ", $resultado[$i]["nombre_per"]);
			$lastname = explode(" ", $resultado[$i]["apellido_per"]);
			$fullname = $name[0].' '.$lastname[0];

			$pdf->Cell(12,$altoFila,$i+1,'T',0,'C',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
			$pdf->Cell(26,$altoFila,$resultado[$i]["num_soli"],'T',0,'C',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
			$pdf->Cell(20,$altoFila,$resultado[$i]["cedula_per"],'T',0,'C',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
			$pdf->Cell(34,$altoFila,$fullname,'T',0,'C',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
			$pdf->MultiCelda(58,$altoFila,$resultado[$i]["motivovia_soli"],'T','L',1);$pdf->Cell(1,$altoFila,"",'',0,'C',0);
			$pdf->Cell(50,$altoFila,$resultado[$i]["lugardes_soli"],'T',0,'C',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
			$pdf->Cell(25,$altoFila,$resultado[$i]["fechades_soli"],'T',0,'C',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
			$pdf->Cell(25,$altoFila,$resultado[$i]["fechahas_soli"],'T',0,'C',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
			$pdf->Cell(1,$altoFila*$numFilas,"",0,1,'C',0);
		}

		$pdf->Cell(257,$altoFila,"",'T',1,'C',1);
	}

	else{
		$pdf->Ln(2);
		$pdf->Cell(257,5,'NO HAY REGISTROS QUE MOSTRAR','TBLR',1,'C',0);
	}

	$pdf->AliasNbPages();
	$fecha = date("d-m-Y");
	$pdf->Output("LISTADO DE SOLICITUDES_".$fecha.".pdf","I");
?>