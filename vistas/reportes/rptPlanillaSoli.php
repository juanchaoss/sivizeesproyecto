 <?php
	session_start();
	
	$FECHA=date("d/m/Y h:i a",time());
	include_once ("../../modelo/constante.php");
	include_once ("../../modelo/clases/Fachada.php");
	include_once ("../..//modelo/clases/funciones_php.php");
	include_once ("../../modelo/Viatico.php");

	$bd = new Fachada();
	$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

	$id_soli = $_GET['id_soli'];

	$sql = "SELECT * FROM solicitud_via AS S JOIN empleado AS EM ON (S.id_em = EM.id_em)
	JOIN persona AS P ON (P.id_per = EM.id_per) JOIN ciudad AS C ON (C.id_ciu = S.id_ciu_des) JOIN
	estado AS E ON (C.id_es = E.id_es) 
	WHERE id_soli=".$id_soli;

	$resultado = $bd->consultar($sql, 'ARREGLO');

	ob_end_clean();
	require('../../modelo/clases/fpdf/fpdf.php');

	class PDF_P extends FPDF{
	    function Header(){

	        $this->Image("../../vistas/img/bar_gob.jpg",10,10,194);
	        $this->Ln(5);
			$this->SetFillColor(255,255,255);
			$this->SetFont('helvetica','',9.5);
			$this->Ln(14);
	    }

		function MultiCelda($w,$h,$txt,$border,$align,$fill){
	        $x=$this->GetX();
	        $y=$this->GetY();
	        $this->MultiCell($w,$h,$txt,$border,$align,$fill);
	        $this->SetXY($x+$w,$y);
	    }
	}

	$altoFila=7;
	$fontsize=12;
	$pdf=new PDF_P("P","mm","letter");
	$pdf->SetLeftMargin(10);
	$pdf->SetTopMargin(10);
	$pdf->SetAutoPageBreak(true,10);
	$pdf->SetLineWidth(0.2);

	$pdf->SetFont('helvetica','',9.5);
	$pdf->SetFillColor(255,255,255);

	$pdf->AddPage();

	//CONTENIDO DE LA PÁGINA
	if ($resultado){

		$pdf->SetFont('helvetica','B',14);
		$pdf->Cell(150,$altoFila,"PLANILLA DE SOLICITUD DE VIÁTICOS Y PASAJES",0,0,'L',1);
		$pdf->SetFont('helvetica','',14);
		$pdf->Cell(44,$altoFila,"SOLICITUD N° ".completarCodigoCeros($resultado[0]['num_soli'],10),0,1,'R',1); 
		$pdf->Ln(5);
		$pdf->SetFillColor(150,150,150);

		$pdf->SetFont('helvetica','B',$fontsize);
		$pdf->Cell(194,$altoFila,"A. DATOS DEL SOLICITANTE",1,1,'L',1);
		$pdf->Cell(97,$altoFila,"1. Nombres",1,0,'L',0);
		$pdf->Cell(97,$altoFila,"2. Apellidos",1,1,'L',0);
		$pdf->SetFont('helvetica','',$fontsize);
		$pdf->Cell(97,$altoFila+2,$resultado[0]['nombre_per'],1,0,'L',0);
		$pdf->Cell(97,$altoFila+2,$resultado[0]['apellido_per'],1,1,'L',0);

		$pdf->SetFont('helvetica','B',$fontsize);
		$pdf->Cell(60,$altoFila,"3. Cédula de Identidad",1,0,'L',0);
		$pdf->Cell(37,$altoFila,"4. Sexo",1,0,'L',0);
		$pdf->Cell(97,$altoFila,"5. Cargo que Ocupa",1,1,'L',0);

		$pdf->SetFont('helvetica','',$fontsize);
		$pdf->Cell(60,$altoFila+4,$resultado[0]['cedula_per'],1,0,'L',0);
		$pdf->Cell(37,$altoFila+4,"",1,0,'L',0);
		$pdf->Cell(97,$altoFila+4,$resultado[0]['cargo'],1,1,'L',0);
		$x=$pdf->GetX();
		$y=$pdf->GetY();

		$pdf->SetXY(72,74);
		$sexm="";
		$sexf="x";
		if ($resultado[0]['sexo_per']=='M'){
			$sexm="x";
			$sexf="";
		}
		$pdf->Cell(5,5,$sexm,1,0,'C',0);$pdf->Cell(10,5,"M",0,0,'L',0);$pdf->Cell(5,5,$sexf,1,0,'C',0);$pdf->Cell(5,5,"F",0,0,'C',0);
		$pdf->SetXY($x,$y);


		$pdf->SetFont('helvetica','B',$fontsize);
		$pdf->Cell(194,$altoFila,"B. DATOS DE LA MISIÓN",1,1,'L',1);

		$pdf->Cell(97,$altoFila,"6. Lugar de Destino",1,0,'L',0);
		$pdf->Cell(48,$altoFila,"7. Ciudad",1,0,'L',0);
		$pdf->Cell(49,$altoFila,"8. Estado",1,1,'L',0);
		$pdf->SetFont('helvetica','',$fontsize);
		$pdf->Cell(97,$altoFila+4,$resultado[0]['lugardes_soli'],1,0,'L',0);
		$pdf->Cell(48,$altoFila+4,$resultado[0]['nombre_ciu'],1,0,'L',0);
		$pdf->Cell(49,$altoFila+4,$resultado[0]['nombre_es'],1,1,'L',0);

		$pdf->SetFont('helvetica','B',$fontsize);
		$pdf->Cell(194,$altoFila,"9. Fecha de la Misión",1,1,'L',0);
		$pdf->Cell(27,$altoFila+4,"Desde",1,0,'L',0);
		$pdf->SetFont('helvetica','',$fontsize);
		$pdf->Cell(70,$altoFila+4,$resultado[0]['fechades_soli'],1,0,'L',0); 
		$pdf->SetFont('helvetica','B',$fontsize);
		$pdf->Cell(27,$altoFila+4,"Hasta",1,0,'L',0);
		$pdf->SetFont('helvetica','',$fontsize);
		$pdf->Cell(70,$altoFila+4,$resultado[0]['fechahas_soli'],1,1,'L',0);
		$pdf->SetFont('helvetica','B',$fontsize);
		$pdf->Cell(194,$altoFila,"10. Motivo del Viaje",1,1,'L',0);
		$pdf->SetFont('helvetica','',$fontsize);
		$motivo=$resultado[0]['motivovia_soli'];
		$longitud=$pdf->GetStringWidth($motivo);
		$numFilas=ceil($longitud/192);
		$pdf->MultiCelda(194,$altoFila,$motivo,'T','L',0);$pdf->Cell(1,$altoFila*$numFilas,"",0,1,'C',0);

		$pdf->SetXY(10,170);
		$pdf->SetFont('helvetica','B',$fontsize);
		$pdf->Cell(194,$altoFila,"11. Transporte Utilizado",1,1,'L',0);
		$pdf->SetFont('helvetica','',$fontsize);
		$pdf->Cell(20,$altoFila+2,"Ida:",1,0,'L',0);
		$pdf->Cell(38,$altoFila+2,"      Aéreo",1,0,'L',0);
		$pdf->Cell(39,$altoFila+2,"      Terrestre",1,0,'L',0);
		$pdf->Cell(97,$altoFila+2,"      Otro:",1,1,'L',0);

		$pdf->Cell(20,$altoFila+2,"Vuelta:",1,0,'L',0);
		$pdf->Cell(38,$altoFila+2,"      Aéreo",1,0,'L',0);
		$pdf->Cell(39,$altoFila+2,"      Terrestre",1,0,'L',0);
		$pdf->Cell(97,$altoFila+2,"      Otro:",1,1,'L',0);

		$x=$pdf->GetX();
		$y=$pdf->GetY();
		
		//ida
		$aer="x";
		$ter="";
		$otr="";
		$otrotxt="";
		if ($resultado[0]['tipotrans_soli']==2){
			$aer="";
			$ter="x";
			$otr="";
		}else if ($resultado[0]['tipotrans_soli']==3){
			$aer="";
			$ter="";
			$otr="x";
			$otrotxt=$resultado[0]['otro'];
		}
		$pdf->SetXY(32,179);
		$pdf->Cell(5,5, $aer,1,0,'C',0);
		$pdf->SetXY(70,179);
		$pdf->Cell(5,5,$ter,1,0,'C',0);
		$pdf->SetXY(109,179);
		$pdf->Cell(5,5,$otr,1,0,'C',0);
		$pdf->SetXY(125,179);
		$pdf->Cell(80,5,$otrotxt,0,0,'L',0);
		$pdf->SetXY($x,$y);

		//vuelta
		$aer="x";
		$ter="";
		$otr="";
		$otrotxt="";
		if ($resultado[0]['tipotransvu_soli']==2){
			$aer="";
			$ter="x";
			$otr="";
		}else if ($resultado[0]['tipotransvu_soli']==3){
			$aer="";
			$ter="";
			$otr="x";
			$otrotxt=$resultado[0]['otrovu'];
		}
		$pdf->SetXY(32,188);
		$pdf->Cell(5,5, $aer,1,0,'C',0);
		$pdf->SetXY(70,188);
		$pdf->Cell(5,5,$ter,1,0,'C',0);
		$pdf->SetXY(109,188);
		$pdf->Cell(5,5,$otr,1,0,'C',0);
		$pdf->SetXY(125,188);
		$pdf->Cell(80,5,$otrotxt,0,0,'L',0);
		
		$pdf->SetXY($x,$y);

		$pdf->Ln(30);
		$pdf->Line(10, 200, 204, 200);
		$pdf->Line(10, 242, 204, 242);

		$pdf->Line(10, 200, 10, 242);
		$pdf->Line(58, 200, 58, 242);
		$pdf->Line(107, 200, 107, 242);
		$pdf->Line(155, 200, 155, 242);
		$pdf->Line(204, 200, 204, 242);

		$pdf->Cell(48,$altoFila-2,"",0,0,'C',0);
		$pdf->Cell(49,$altoFila-2,"FIRMA Y SELLO DE",0,0,'C',0);
		$pdf->Cell(48,$altoFila-2,"FIRMA Y SELLO DE",0,0,'C',0);
		$pdf->Cell(49,$altoFila-2,"FIRMA Y SELLO",0,1,'C',0);

		$pdf->Cell(48,$altoFila-2,"FIRMA DEL",0,0,'C',0);
		$pdf->Cell(49,$altoFila-2,"AUTORIZACIÓN DEL",0,0,'C',0);
		$pdf->Cell(48,$altoFila-2,"APROBACIÓN",0,0,'C',0);
		$pdf->Cell(49,$altoFila-2,"DIRECTOR DE",0,1,'C',0);

		$pdf->Cell(48,$altoFila-2,"FUNCIONARIO",0,0,'C',0);
		$pdf->Cell(49,$altoFila-2,"JEFE INMEDIATO",0,0,'C',0);
		$pdf->Cell(48,$altoFila-2,"(ADMINISTRACIÓN)",0,0,'C',0);
		$pdf->Cell(49,$altoFila-2,"ZONA EDUCATIVA",0,1,'C',0);



		$pdf->Ln(5);
		$pdf->SetFont('helvetica','',$fontsize-2);
		$nota="NOTA: DEBE ANEXAR COPIA CÉDULA DE IDENTIDAD, OFICIO DE SOLICITUD O INVITACIÓN Y VOUCHERS DEL FUNCIONARIO. PRESENTAR LOS BOLETOS DE PASAJAES CANCELADOS Y CUALQUIER OTRO DOCUMENTO DE GASTO. COPIA DE INFORME O DE ACTA DE LA ACTIVIDAD REALIZADA.";
		$longitud=$pdf->GetStringWidth($nota);
		$numFilas=ceil($longitud/192);
		$pdf->MultiCelda(194,$altoFila,$nota,0,'L',0);$pdf->Cell(1,$altoFila*$numFilas,"",0,1,'C',0);

	}

	else{
		$pdf->Ln(2);
		$pdf->Cell(194,5,'NO HAY REGISTROS QUE MOSTRAR','TBLR',1,'C',0);
	}

	$pdf->AliasNbPages();
	$fecha = date("d-m-Y");
	$pdf->Output("PLANILLA_SOLICITUD_".$fecha.".pdf","I");
?>