 <?php
	session_start();
	if (!$_SESSION['usuario']) {
		header('location:../../index.php');
	}
	$FECHA=date("d/m/Y h:i a",time());
	include_once ("../../modelo/constante.php");
	include_once ("../../modelo/clases/Fachada.php");
	include_once ("../../modelo/clases/funciones_php.php");
	include_once ("../../modelo/Viatico.php");
	include_once ("../../modelo/Presupuesto.php");

	$bd = new Fachada();
	$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

	$partida = $_GET['partida'];
	//$where='';

			if ($partida != 0) {
				$where = "WHERE P.id_partida = $partida";
			}

			$sql = "SELECT P.cuenta_par, P.descripcion_par, CASE WHEN SUM(M.monto_movi) 
				    IS NOT NULL THEN SUM(M.monto_movi) ELSE 0 END AS disponible 
          			FROM partida AS P LEFT JOIN movimiento AS M ON (P.id_partida = M.id_partida)" 
					.$where. "GROUP BY P.id_partida ORDER BY P.cuenta_par";

	$resultado = $bd->consultar($sql, 'ARREGLO');

	ob_end_clean();
	require('../../modelo/clases/fpdf/fpdf.php');

	class PDF_P extends FPDF{
	    function Header(){

	        $this->Image("img_logo/logo_system.jpg",12,12,20);
	        $this->Cell(164,5,"Fecha: ".date("d/m/Y h:i a",time()),'',1,'R',0);
	        $this->Cell(164,5,'Página: '.$this->PageNo().' de {nb}','',1,'R',0);
	        $this->Ln(5);
	        $this->SetFont('helvetica','BI',10);
	        $this->Cell(144,5,'ZONA EDUCATIVA DEL ESTADO SUCRE','',1,'C',0);
	        $this->Cell(144,5,'LISTADO DE DINERO DISPONIBLE POR PARTIDA','',1,'C',0);
	        $this->Ln(10);

	        $this->SetFillColor(204,204,204);
			$this->SetFont('helvetica','B',8);

			$this->Cell(10,$GLOBALS["altoFila"],"#",'TB',0,'C',1); $this->Cell(1,$GLOBALS["altoFila"],"",'',0,'C',0);
			$this->Cell(40,$GLOBALS["altoFila"],"Cuenta",'TB',0,'C',1); $this->Cell(1,$GLOBALS["altoFila"],"",'',0,'C',0);
			$this->Cell(60,$GLOBALS["altoFila"],"Partida",'TB',0,'C',1); $this->Cell(1,$GLOBALS["altoFila"],"",'',0,'C',0);
			$this->Cell(40,$GLOBALS["altoFila"],"Disponibilidad (Bs.)",'TB',1,'C',1);
			$this->SetFillColor(255,255,255);
			$this->SetFont('helvetica','',9.5);
	    }

		function MultiCelda($w,$h,$txt,$border,$align,$fill){
	        $x=$this->GetX();
	        $y=$this->GetY();
	        $this->MultiCell($w,$h,$txt,$border,$align,$fill);
	        $this->SetXY($x+$w,$y);
	    }
	}
//$porc = number_format($porc, 2, ',', '.' );
	$altoFila=5;
	$pdf=new PDF_P("P","mm","letter");
	$pdf->SetLeftMargin(30);
	$pdf->SetTopMargin(10);
	$pdf->SetAutoPageBreak(true,10);
	$pdf->SetLineWidth(0.2);

	$pdf->SetFont('helvetica','',9.5);
	$pdf->SetFillColor(255,255,255);

	$pdf->AddPage();

	//CONTENIDO DE LA PÁGINA
	if ($resultado){
	//$monto_final = 0;
	$nreg=count($resultado);
	for($i=0;$i<$nreg;$i++){

		$longitud=$pdf->GetStringWidth($resultado[$i]["descripcion_movi"]);
		$numFilas=ceil($longitud/74);

		if ($pdf->GetY()+($alt*$numFilas) > 205)
		{
			$pdf->AddPage();
		}

		$pdf->Cell(10,$altoFila,$i+1,'T',0,'C',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
		$pdf->Cell(40,$altoFila,$resultado[$i]["cuenta_par"],'T',0,'C',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
		$pdf->Cell(60,$altoFila,$resultado[$i]["descripcion_par"],'T',0,'L',1);$pdf->Cell(1,$altoFila,"",'',0,'C',0);
		$pdf->Cell(40,$altoFila,number_format($resultado[$i]["disponible"],2,',','.'),'T',1,'R',1);
		//$pdf->Cell(1,$altoFila,"",'',0,'C',0);
		//$pdf->Cell(1,$altoFila*$numFilas,"",0,1,'C',0);
	}
	$pdf->Cell(135,$altoFila,'','T',1,'C',0);
	$pdf->ln(3);
	$pdf->SetFont('helvetica','B',9.5);
}
else
{
	$pdf->Ln(2);
	$pdf->Cell(135,5,'NO HAY REGISTROS QUE MOSTRAR','TBLR',1,'C',0);
}
	$pdf->AliasNbPages();
	$fecha = date("d-m-Y");
	$pdf->Output("LISTADO DE DINERO DISPONIBLE POR PARTIDA_".$fecha.".pdf","I");
?>