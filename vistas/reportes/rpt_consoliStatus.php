 <?php
	/*session_start();

	if (!$_SESSION['usuario'] || !$_SESSION['usuarioOnline']) {
		header('location:../../index.php');
	}*/

	$FECHA=date("d/m/Y h:i a",time());
	include_once ("../../modelo/constante.php");
	include_once ("../../modelo/clases/Fachada.php");
	include_once ("../../modelo/clases/funciones_php.php");
	include_once ("../../modelo/Viatico.php");

	$bd = new Fachada();
	$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

	$fedesde = $_GET['fedesde'];
	$fehasta = $_GET['fehasta'];
	$filtro  = $_GET['filtro'];

		$and = '';
		$criterios = '';

		if ($filtro != '-') {
			$and = "AND estado_soli = $filtro";

			if($filtro==0){
				$status="Solicitudes recién creadas";
			}else if($filtro==1){
				$status="Solicitudes aprobadas sin cálculo de gastos";
			}else if($filtro==2){
				$status="Solicitudes con gastos aún no aprobados";
			}else if($filtro==3){
				$status="Solicitudes con gastos aprobados";
			}else if($filtro==4){
				$status="Solicitudes pagadas";
			}else if($filtro==5){
				$status="Solicitudes canceladas";
			}
		}else{
			$status = 'TODOS LOS STATUS';
		}

		if ($fedesde != '') {
			$and = "AND fechades_soli >= '$fedesde'";
			$criterios .= 'DESDE: '.formatearFecha($fedesde);
		}

		if ($fehasta != '') {
			$and = "AND fechahas_soli <= '$$fehasta'";
			$criterios .= ' HASTA: '.formatearFecha($fehasta);
		}


		$sql = "SELECT COUNT(id_soli) AS cont, estado_soli FROM solicitud_via WHERE 1=1 $and --estado_soli = 0
				/*and fechades_soli >= '2014-10-16' AND fechahas_soli <= '2015-03-12' */
				GROUP BY estado_soli order BY estado_soli";


	$resultado = $bd->consultar($sql, 'ARREGLO');

	ob_end_clean();
	require('../../modelo/clases/fpdf/fpdf.php');

	class PDF_P extends FPDF{
	    function Header(){

	        $this->Image("img_logo/logo_system.jpg",12,12,20);
	        $this->Cell(257,5,"Fecha: ".date("d/m/Y h:i a",time()),'',1,'R',0);
	        $this->Cell(257,5,'Página: '.$this->PageNo().' de {nb}','',1,'R',0);
	        $this->Ln(5);
	        $this->SetFont('helvetica','BI',10);
	        $this->Cell(257,5,'ZONA EDUCATIVA DEL ESTADO SUCRE','',1,'C',0);
	        $this->Cell(257,5,'CONSOLIDADO DE SOLICITUDES POR STATUS','',1,'C',0);
	        $this->Cell(257,5,'STATUS: '.$GLOBALS["status"],'',1,'C',0);
			$this->SetFont('helvetica','BI',9);
			if ($GLOBALS["criterios"]!="") {
				$this->Cell(257,4,$GLOBALS["criterios"],'',1,'C',0); 
			}
	        $this->Ln(5);

	        $this->SetFillColor(204,204,204);
			$this->SetFont('helvetica','B',8);
			$this->Cell(60,$GLOBALS["altoFila"],"",0,0,'C',0);
			$this->Cell(12,$GLOBALS["altoFila"],"#",'TB',0,'C',1); $this->Cell(1,$GLOBALS["altoFila"],"",'',0,'C',0);
			$this->Cell(80,$GLOBALS["altoFila"],"Estado de la Solicitud",'TB',0,'C',1); $this->Cell(1,$GLOBALS["altoFila"],"",'',0,'C',0);
			$this->Cell(30,$GLOBALS["altoFila"],"Cant. Total",'TB',1,'C',1);
			$this->SetFillColor(255,255,255);
			$this->SetFont('helvetica','',9.5);
	    }

		function MultiCelda($w,$h,$txt,$border,$align,$fill){
	        $x=$this->GetX();
	        $y=$this->GetY();
	        $this->MultiCell($w,$h,$txt,$border,$align,$fill);
	        $this->SetXY($x+$w,$y);
	    }
	}

	$altoFila=5;
	$pdf=new PDF_P("L","mm","letter");
	$pdf->SetLeftMargin(10);
	$pdf->SetTopMargin(10);
	$pdf->SetAutoPageBreak(true,10);
	$pdf->SetLineWidth(0.2);

	$pdf->SetFont('helvetica','',10);
	$pdf->SetFillColor(255,255,255);

	$pdf->AddPage();

	//CONTENIDO DE LA PÁGINA
	if ($resultado){
		$nreg=count($resultado);

		for($i=0;$i<$nreg;$i++){
			$longitud=$pdf->GetStringWidth($resultado[$i]["motivovia_soli"]);
			$numFilas=ceil($longitud/57);

			if ($pdf->GetY()+($alt*$numFilas) > 205){
				$pdf->AddPage();
			}
			$filtro = $resultado[$i]['estado_soli'];
			if($filtro==0){
				$status="Solicitudes recién creadas";
			}else if($filtro==1){
				$status="Solicitudes aprobadas sin cálculo de gastos";
			}else if($filtro==2){
				$status="Solicitudes con gastos aún no aprobados";
			}else if($filtro==3){
				$status="Solicitudes con gastos aprobados";
			}else if($filtro==4){
				$status="Solicitudes pagadas";
			}else if($filtro==5){
				$status="Solicitudes canceladas";
			}

			$pdf->Cell(60,$altoFila,"",0,0,'C',0);
			$pdf->Cell(12,$altoFila,$i+1,'T',0,'C',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
			$pdf->Cell(80,$altoFila,$status,'T',0,'C',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
			$pdf->Cell(30,$altoFila,$resultado[$i]["cont"],'T',1,'C',1); 
		}

		$pdf->Cell(60,$altoFila,"",0,0,'C',0);
		$pdf->Cell(130,$altoFila,"",'T',1,'C',1);
	}

	else{
		$pdf->Ln(2);
		$pdf->Cell(130,5,'NO HAY REGISTROS QUE MOSTRAR','TBLR',1,'C',0);
	}

	$pdf->AliasNbPages();
	$fecha = date("d-m-Y");
	$pdf->Output("LISTADO DE SOLICITUDES_".$fecha.".pdf","I");
?>