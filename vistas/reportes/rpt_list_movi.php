<?php

$FECHA=date("d/m/Y h:i a",time());
include_once ("../../modelo/constante.php");
include_once ("../../modelo/clases/Fachada.php");
//include_once ("../../../trans/funciones_php.php"); 
include_once ("../../modelo/Viatico.php"); 

$bd = new Fachada();
$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

$mes  = $_GET['mes'];
$anio = $_GET['anio'];
$impu = $_GET['impu'];

$id_parti = explode('&@', $impu);
$criterios.="Partida Presupuestaria: ".$id_parti[1];


$sql = "SELECT * FROM movimiento AS M JOIN partida AS P ON (M.id_partida=P.id_partida)
WHERE P.id_partida = '$id_parti[0]' AND text(M.fecha_movi) like '$anio-$mes-%' ORDER BY M.fecha_movi, M.id_movi";

$resultado = $bd->consultar($sql, 'ARREGLO');
ob_end_clean();
require('../../modelo/clases/fpdf/fpdf.php');

class PDF_P extends FPDF
{
    function Header()
    {
        $this->Image("img_logo/logo_system.jpg",12,12,20);
        $this->Cell(257,5,"Fecha: ".date("d/m/Y h:i a",time()),'',1,'R',0);
        $this->Cell(257,5,'Página: '.$this->PageNo().' de {nb}','',1,'R',0);
        $this->Ln(5);
        $this->SetFont('helvetica','BI',10);
        $this->Cell(257,5,'ZONA EDUCATIVA DEL ESTADO SUCRE','',1,'C',0);
        $this->Cell(257,5,'LISTADO DE MOVIMIENTOS','',1,'C',0);
		$this->SetFont('helvetica','BI',9);
		if ($GLOBALS["criterios"]!="") {
			$this->Cell(257,4,$GLOBALS["criterios"],'',1,'C',0); 
		}
        $this->Ln(5);

        $this->SetFillColor(204,204,204);
		$this->SetFont('helvetica','B',8);
		$this->Cell(12,$GLOBALS["altoFila"],"#",'TB',0,'C',1); $this->Cell(1,$GLOBALS["altoFila"],"",'',0,'C',0);
		$this->Cell(76,$GLOBALS["altoFila"],"Descrip. Del Movimiento",'TB',0,'C',1); $this->Cell(1,$GLOBALS["altoFila"],"",'',0,'C',0);
		$this->Cell(31,$GLOBALS["altoFila"],"Monto Bs.",'TB',0,'C',1); $this->Cell(1,$GLOBALS["altoFila"],"",'',0,'C',0);
		$this->Cell(25,$GLOBALS["altoFila"],"Fecha",'TB',0,'C',1); $this->Cell(1,$GLOBALS["altoFila"],"",'',0,'C',0);
		$this->Cell(30,$GLOBALS["altoFila"],"Modo del Movimiento",'TB',0,'C',1); $this->Cell(1,$GLOBALS["altoFila"],"",'',0,'C',0);
		$this->Cell(40,$GLOBALS["altoFila"],"Patida",'TB',0,'C',1); $this->Cell(1,$GLOBALS["altoFila"],"",'',0,'C',0);
		$this->Cell(40,$GLOBALS["altoFila"],"Num. Referencia",'TB',0,'C',1); $this->Cell(1,$GLOBALS["altoFila"],"",'TB',1,'C',1);
		$this->SetFillColor(255,255,255);
		$this->SetFont('helvetica','',9.5);
    }

	function MultiCelda($w,$h,$txt,$border,$align,$fill)
    {
        $x=$this->GetX();
        $y=$this->GetY();
        $this->MultiCell($w,$h,$txt,$border,$align,$fill);
        $this->SetXY($x+$w,$y);
    }
}
$altoFila=6;
$pdf=new PDF_P("L","mm","letter");
$pdf->SetLeftMargin(10);
$pdf->SetTopMargin(10);
$pdf->SetAutoPageBreak(true,10);
$pdf->SetLineWidth(0.2);

$pdf->SetFont('helvetica','',9.5);
$pdf->SetFillColor(255,255,255);

$pdf->AddPage();

//CONTENIDO DE LA PÁGINA
if ($resultado){
	$monto_final = 0;
	$nreg=count($resultado);

	for($i=0;$i<$nreg;$i++){
		$longitud=$pdf->GetStringWidth($resultado[$i]["descripcion_movi"]);
		$numFilas=ceil($longitud/74);

		if ($resultado[$i]["modo_movi"] == 0) {
			$msj = 'Cheque';
		}elseif ($resultado[$i]["modo_movi"] == 1) {
			$msj = 'Transferencia';
		}
		elseif ($resultado[$i]["modo_movi"] == 1) {
			$msj = 'Depsito';
		}else{
			$msj = 'Traspaso';
		}

		if ($pdf->GetY()+($alt*$numFilas) > 205)
		{
			$pdf->AddPage();
		}
		$pdf->Cell(12,$altoFila,$i+1,'T',0,'C',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
		$pdf->MultiCelda(76,$altoFila,$resultado[$i]["descripcion_movi"],'T','L',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
		$pdf->Cell(31,$altoFila,number_format($resultado[$i]["monto_movi"],2,',','.'),'T',0,'R',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
		$pdf->Cell(25,$altoFila,$resultado[$i]["fecha_movi"],'T',0,'C',1);$pdf->Cell(1,$altoFila,"",'',0,'C',0);
		$pdf->Cell(30,$altoFila,$msj,'T',0,'C',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
		$pdf->Cell(40,$altoFila,$resultado[$i]["cuenta_par"],'T',0,'C',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
		$pdf->Cell(40,$altoFila,$resultado[$i]["num_referencia"],'T',0,'C',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
		$pdf->Cell(1,$altoFila*$numFilas,"",0,1,'C',0);
		$monto_final = $monto_final+$resultado[$i]["monto_movi"];
	}
	$pdf->Cell(257,$altoFila,'','T',1,'C',0);

	$pdf->ln(3);
	$pdf->SetFont('helvetica','B',9.5);
	$pdf->Cell(75,$altoFila,'MONTO FINAL DE LA PARTIDA: '.number_format($monto_final,2,',','.'),'',0,'L',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
}
else
{
	$pdf->Ln(2);
	$pdf->Cell(257,5,'NO HAY REGISTROS QUE MOSTRAR','TBLR',1,'C',0);
}

$pdf->AliasNbPages();
$fecha = date("d-m-Y");
$pdf->Output("LISTADO DE MOVIMIENTOS_".$fecha.".pdf","I");
?>