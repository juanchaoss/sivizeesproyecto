 <?php
	session_start();
	if (!$_SESSION['usuario']) {
		header('location:../../index.php');
	}
	$FECHA=date("d/m/Y h:i a",time());
	include_once ("../../modelo/constante.php");
	include_once ("../../modelo/clases/Fachada.php");
	include_once ("../../modelo/clases/funciones_php.php");
	include_once ("../../modelo/Viatico.php");

	$bd = new Fachada();
	$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

	
	
	$filtro    = $_GET['filtro'];
	$fedesde   = $_GET['fedesde'];
	$fehasta   = $_GET['fehasta'];
	$estades   = $_GET['estades'];
	$ciudes    = $_GET['ciudes'];
	$cons      = $_GET['cons'];

	$AND = "";
	$criterios = "";
	$GO = "";
	$SELECT = "";

	if ($filtro != "-") {
		$AND.=" AND S.estado_soli = ".$filtro;
		if($filtro==0){
			$status="Solicitudes recién creadas";
		}else if($filtro==1){
			$status="Solicitudes aprobadas sin cálculo de gastos";
		}else if($filtro==2){
			$status="Solicitudes con gastos aún no aprobados";
		}else if($filtro==3){
			$status="Solicitudes con gastos aprobados";
		}else if($filtro==4){
			$status="Solicitudes pagadas";
		}else if($filtro==5){
			$status="Solicitudes canceladas";
		}

		$criterios.="Status: ".$status.". ";
	}

	if ($estades!=0) {
		$AND.=" AND ED.id_es = '".$estades."'";
		$query = "SELECT * FROM estado WHERE id_es = $estades";
		$result = $bd->consultar($query, 'ARREGLO');
		if ($result) {
			$nomb_es = $result[0]['nombre_es'];
		}
		$criterios.="Estado Destino: ".$nomb_es.". ";
	}

	if ($cons==0){//no es consolidado
		$SELECT=",CD.*";
		$GO=",CD.id_ciu";

		if ($ciudes!=0) {
			$AND.=" AND CD.id_ciu = '".$ciudes."'";
		    $query = "SELECT * FROM ciudad WHERE id_ciu = $ciudes";
			$result = $bd->consultar($query, 'ARREGLO');
			if ($result) {
				$nomb_ciu = $result[0]['nombre_ciu'];
			}
			$criterios.="Ciudad Destini: ".$nomb_ciu.". ";
		}
	}


	$cent=0;
	$periodo="";
	if ($fedesde!="") {
		$AND.=" AND CAST(S.fechades_soli AS DATE) >= '".$fedesde."'";
		$periodo.=" desde ".$fedesde;
		$cent=1;
	}

	if ($fehasta!="") {
		$AND.=" AND CAST(S.fechahas_soli AS DATE) <= '".$fehasta."'";
		$periodo.=" hasta ".$fehasta;
		$cent=1;
	}

	if($cent==1){
		$criterios.="Fecha misión:".$periodo.". ";
	}

	$sql = "SELECT COUNT(S.id_soli) AS cont,
			(
				SELECT COUNT(S.id_soli) AS cont
				FROM solicitud_via AS S JOIN empleado AS E ON (S.id_em = E.id_em)
							JOIN persona AS P ON (P.id_per = E.id_per) 
							JOIN ciudad AS CD ON (S.id_ciu_des = CD.id_ciu)
							RIGHT JOIN estado AS ED ON (CD.id_es = ED.id_es)
							WHERE 1=1 $AND
			) AS total, ED.id_es AS id_edo, ED.nombre_es AS nombre_edo $SELECT
			FROM solicitud_via AS S JOIN empleado AS E ON (S.id_em = E.id_em)
						JOIN persona AS P ON (P.id_per = E.id_per) 
						JOIN ciudad AS CD ON (S.id_ciu_des = CD.id_ciu)
						RIGHT JOIN estado AS ED ON (CD.id_es = ED.id_es)
						WHERE 1=1 $AND
						GROUP BY ED.id_es $GO
						ORDER BY ED.id_es $GO";

	$resultado = $bd->consultar($sql, 'ARREGLO');

	ob_end_clean();
	require('../../modelo/clases/fpdf/fpdf.php');

	class PDF_P extends FPDF{
	    function Header(){

	        $this->Image("img_logo/logo_system.jpg",12,12,20);
	        $this->Cell(164,5,"Fecha: ".date("d/m/Y h:i a",time()),'',1,'R',0);
	        $this->Cell(164,5,'Página: '.$this->PageNo().' de {nb}','',1,'R',0);
	        $this->Ln(5);
	        $this->SetFont('helvetica','BI',10);
	        $this->Cell(144,5,'ZONA EDUCATIVA DEL ESTADO SUCRE','',1,'C',0);
	        $this->Cell(144,5,'LISTADO DE SOLICITUDES POR ZONA','',1,'C',0);
			$this->SetFont('helvetica','BI',9);
			if ($GLOBALS["criterios"]!="") {
				$this->Cell(144,4,$GLOBALS["criterios"],'',1,'C',0); 
			}
	        $this->Ln(10);

	        $this->SetFillColor(204,204,204);
			$this->SetFont('helvetica','B',8);

			$this->Cell(12,$GLOBALS["altoFila"],"#",'TB',0,'C',1); $this->Cell(1,$GLOBALS["altoFila"],"",'',0,'C',0);
			$this->Cell(80,$GLOBALS["altoFila"],"Destino",'TB',0,'C',1); $this->Cell(1,$GLOBALS["altoFila"],"",'',0,'C',0);
			$this->Cell(20,$GLOBALS["altoFila"],"Cantidad",'TB',0,'C',1); $this->Cell(1,$GLOBALS["altoFila"],"",'',0,'C',0);
			$this->Cell(20,$GLOBALS["altoFila"],"%",'TB',1,'C',1); 
			$this->SetFillColor(255,255,255);
			$this->SetFont('helvetica','',9.5);
	    }

		function MultiCelda($w,$h,$txt,$border,$align,$fill){
	        $x=$this->GetX();
	        $y=$this->GetY();
	        $this->MultiCell($w,$h,$txt,$border,$align,$fill);
	        $this->SetXY($x+$w,$y);
	    }
	}

	$altoFila=5;
	$pdf=new PDF_P("P","mm","letter");
	$pdf->SetLeftMargin(40);
	$pdf->SetTopMargin(10);
	$pdf->SetAutoPageBreak(true,10);
	$pdf->SetLineWidth(0.2);

	$pdf->SetFont('helvetica','',9.5);
	$pdf->SetFillColor(255,255,255);

	$pdf->AddPage();

	//CONTENIDO DE LA PÁGINA
	if ($resultado)
	{
		$nreg=count($resultado);
		$total = $resultado[0]['total'];
        $es=$resultado[0]['id_edo'];
        $enc=0;
        $total_es=0;
        $k=1;


		for($i=0;$i<$nreg;$i++)
		{
			$cant = $resultado[$i]['cont'];
            $porc = $cant*100/$total;
            $porc = number_format($porc, 2, ',', '.' );

            if ($cons==0)
            {
                if($enc==0)
                {
                    $total_es=$resultado[$i]['cont'];
                    $ciudad="-";
                    if($resultado[$i]['nombre_ciu'] && $resultado[$i]['nombre_ciu']!=""){
                        $ciudad=$resultado[$i]['nombre_ciu'];
                    }
                    //-----encabezado, cuando es detallado
                    $pdf->SetFillColor(190,190,190);
                    $x=$pdf->GetX();
                    $y=$pdf->GetY();
                    $pdf->Cell(12,$altoFila,$k,'T',0,'C',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
                    $pdf->Cell(80,$altoFila,$resultado[$i]['nombre_edo'],'T',0,'L',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
                    $xt=$pdf->GetX();
                    $yt=$pdf->GetY();
                    $pdf->Cell(20,$altoFila,"",'T',0,'C',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
                    $xp=$pdf->GetX();
                    $yp=$pdf->GetY();
                    $pdf->Cell(20,$altoFila,"",'T',1,'C',1);
                    $pdf->SetFillColor(255,255,255);
                    //-----
                    $pdf->Cell(12,$altoFila,"",'T',0,'C',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
                    $pdf->Cell(80,$altoFila,$ciudad,'T',0,'L',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
                    $pdf->Cell(20,$altoFila,$resultado[$i]['cont'],'T',0,'C',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
                    $pdf->Cell(20,$altoFila,$porc.'%','T',1,'C',1);

                    $enc=1;
                    $k++;
                }
                else
                {

                    if($resultado[$i]['id_edo']==$es)
                    {
                        $total_es=$total_es+$resultado[$i]['cont'];
                        $pdf->Cell(12,$altoFila,"",'T',0,'C',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
	                    $pdf->Cell(80,$altoFila,$resultado[$i]['nombre_ciu'],'T',0,'L',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
	                    $pdf->Cell(20,$altoFila,$resultado[$i]['cont'],'T',0,'C',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
	                    $pdf->Cell(20,$altoFila,$porc.'%','T',1,'C',1);
                    }
                    else
                    {
                        $porc_es = $total_es*100/$total;
                        $porc_es = number_format($porc_es, 2, ',', '.' );
                        $x=$pdf->GetX();
                    	$y=$pdf->GetY();

                    	$pdf->SetXY($xt,$yt);
                    	$pdf->SetFillColor(190,190,190);
                    	$pdf->Cell(20,$altoFila,$total_es,'T',0,'C',1);
                    	$pdf->SetXY($xp,$yp);
                    	$pdf->Cell(20,$altoFila,$porc_es,'T',0,'C',1);
                    	$pdf->SetFillColor(255,255,255);

                    	$pdf->SetXY($x,$y);
                        $es=$resultado[$i]['id_edo'];
                        $i--;
                        $enc=0;
                    }
                }
            }
            else
            {
            	$pdf->Cell(12,$altoFila,($i+1),'T',0,'C',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
                $pdf->Cell(80,$altoFila,$resultado[$i]['nombre_edo'],'T',0,'L',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
                $pdf->Cell(20,$altoFila,$resultado[$i]['cont'],'T',0,'C',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
                $pdf->Cell(20,$altoFila,$porc.'%','T',1,'C',1);
            }
        }

        if ($cons==0)
        {
            $porc_es = $total_es*100/$total;
            $porc_es = number_format($porc_es, 2, ',', '.' );
            $x=$pdf->GetX();
        	$y=$pdf->GetY();

        	$pdf->SetFillColor(190,190,190);
        	$pdf->SetXY($xt,$yt);
        	$pdf->Cell(20,$altoFila,$total_es,'T',0,'C',1);
        	$pdf->SetXY($xp,$yp);
        	$pdf->Cell(20,$altoFila,$porc_es,'T',0,'C',1);
        	$pdf->SetFillColor(255,255,255);

        	$pdf->SetXY($x,$y);
        }

        $pdf->Cell(12,$altoFila,"",'T',0,'C',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
        $pdf->Cell(80,$altoFila,"TOTAL",'T',0,'C',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
        $pdf->Cell(20,$altoFila,$total,'T',0,'C',1); $pdf->Cell(1,$altoFila,"",'',0,'C',0);
        $pdf->Cell(20,$altoFila,'100,00%','T',1,'C',1);
	}
	else{
		$pdf->Ln(2);
		$pdf->Cell(257,5,'NO HAY REGISTROS QUE MOSTRAR','TBLR',1,'C',0);
	}

	$pdf->AliasNbPages();
	$fecha = date("d-m-Y");
	$pdf->Output("LISTADO DE SOLICITUDES POR ZONA_".$fecha.".pdf","I");
?>