//################### FUNCI�N ENCARGADA DE CONVERTIR LETRAS A MAYUSCULAS
function soloLetras(evt){
 var nav4=window.Event?true:false;
 var key=nav4?evt.which:evt.keyCode;
 return (key<=13 || key==127 || (key>=48 && key<=57) || (key>=65 && key<=90) || (key>=97 && key<=122) );
}
  
function fechaJStoMySql(fecha){
	var pedazo = fecha.split("/");
	var newFecha=pedazo[2]+'-'+pedazo[1]+'-'+pedazo[0];
	return newFecha;
}

function fechaMySqltoJS(fecha){
	var pedazo = fecha.split("-");
	var newFecha=pedazo[2]+'/'+pedazo[1]+'/'+pedazo[0];
	return newFecha;
}

function lTrim(sStr)
{
	while (sStr.charAt(0) == " ")
		sStr = sStr.substr(1, sStr.length - 1);
		
    return sStr;
}

function rTrim(sStr)
{
	while (sStr.charAt(sStr.length - 1) == " ")
		sStr = sStr.substr(0, sStr.length - 1);
		
    return sStr;
}

function isDefined(variable)
{
    return (!(!(xGetElementById(variable))));
}

function number_format( number, decimals, dec_point, thousands_sep ) 
{
	// http://kevin.vanzonneveld.net
	// + original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
	// + improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// + bugfix by: Michael White (http://crestidg.com)
	
	// + bugfix by: Benjamin Lupton
	
	// + bugfix by: Allan Jensen (http://www.winternet.no)
	
	// + revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
	// * example 1: number_format(1234.5678, 2, '.', '');
	// * returns 1: 1234.57
	
	var n = number, c = isNaN(decimals = Math.abs(decimals)) ? 2 : decimals;	
	var d = dec_point == undefined ? "," : dec_point;	
	var t = thousands_sep == undefined ? "." : thousands_sep, s = n < 0 ? "-" : "";	
	var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
	
	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

//agregadas por Irving Martínez
function str_replace(_text,_replace,_search){
    return _text.split(_search).join(_replace);
}

function EsFechaValida(cadena){
    var FECHA=cadena.split("/");
    if(FECHA.length!=3)
        return false;

    var dia=FECHA[0];
    if(dia.length!=2||isNaN(dia))
        return false;

    var mes=FECHA[1];
    if(mes.length!=2||isNaN(mes))
        return false;

    var anio=FECHA[2];
    if(anio.length!=4||isNaN(anio))
        return false;

    var elMes = parseInt(mes,10);
    if(elMes>12)
        return false;

    // MES FEBRERO
    if(elMes == 2){
        if(esBisiesto(anio)){
            if(parseInt(dia,10) > 29)
                return false;
            else
                return true;
        }
        else{
        if(parseInt(dia) > 28)
            return false;
        else
            return true;
        }
    }

    //RESTO DE MESES
    if(elMes== 4 || elMes==6 || elMes==9 || elMes==11){
        if(parseInt(dia,10) > 30)
            return false;
    }
    else
        if(parseInt(dia,10) > 31)
            return false;
    return true;
}


function esBisiesto(ano){
    return ((ano%4==0 && ano%100!=0)||(ano%400==0)?true:false)
}

function completarCodigoCeros(cadena,tamano)
{
	if(!tamano)
		tamano=3;
	cadena=String(cadena);
    var p=new String("");
    for(;tamano>cadena.length;tamano--)
        p+='0';
    return (p+=cadena);
}

//función que valida que se introduzcan sólo números y ciertos caracteres
//NOTA: Backspace=8, Enter=13, '0'=48, '9'=57, Suprimir=127
function soloNum(evt){
 var nav4=window.Event?true:false;
 var key=nav4?evt.which:evt.keyCode;
	//if(key==13) return false;//necesario para evitar recargas de pagina (ocurre ocacionalmente al presionar enter en el input text de una tabla)
 return (key<=13 || key==127 || (key>=48 && key<=57));
}

function AcceptNum(evt,ID,Negativo,Porcentaje)
{
	var nav4 = window.Event ? true : false;
	var key = nav4 ? evt.which : evt.keyCode;
	if(document.getElementById(ID)){
		var Cadena=document.getElementById(ID).value; 
		if(key==45 && Negativo){//si es "-" y está activado para aceptar números negativos
			if(Cadena.indexOf("-")==-1)//si no encuentra "-", lo colocamos al inicio, retornamos falso para que no se el incluya "-" donde presionamos
				document.getElementById(ID).value="-"+Cadena;
			else
			{
				var inicio=document.getElementById(ID).selectionStart;
				var fin=document.getElementById(ID).selectionEnd;
				var longitud=document.getElementById(ID).value.length;
				var seleccion=fin-inicio;
				if (seleccion==longitud)
				{
					return true;
				}
			}
			return false;
			}
		else if(key==43 && Negativo){//si es "+" y está activado para aceptar números negativos
			if(Cadena.indexOf("-")==0){//si no encuentra "-", lo colocamos al inicio, retornamos falso para que no se el incluya "-" donde presionamos
				var str=xGetElementById(ID).value;
				document.getElementById(ID).value=str.substring(1,str.length);
				} 
			return false;
			}
		else if(key==46){//si es punto
			if(Cadena.length==0)//si la cadena tiene longitud 0 no puedo meter "."
				return false;
			if(Cadena.indexOf(".")==-1)//sólo debe haber un "." en la cadena
				return true;
			return false;
			}
		else if(key==48){//si es cero
			if(Cadena.length==1 && Cadena.indexOf("0")==0)//si hay un caracter en la cadena y ese caracter es cero, no puedo meter otro cero
				return false;
			return true;
			}
		else if(key==37 && Porcentaje){//si es % y está activado porcentaje
			if(Cadena.indexOf("%")==-1)
				document.getElementById(ID).value=Cadena+"%";
			return false;
			}
		}
	if(key==13) return false;//necesario para evitar recargas de página
	return (key <= 13 || (key >= 48 && key <= 57) || key == 46);
}

function validarEmail(email)
{
    if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)))
    {
    	return false;
    }
    
    return true;
}

function activarBoton2(id,funcion)
{
	$(id).button( "option", "disabled", false );
	$(id).unbind('click');
	$(id).click(function() { eval(funcion); });
}

function desactivarBoton2(id)
{
	$(id).button( "option", "disabled", true );
	$(id).unbind('click');
}

function activarBoton(id,funcion)
{
	xGetElementById(id).setAttribute('class','boton ui-corner-all');
	xGetElementById(id).setAttribute('onclick',funcion);
}

function desactivarBoton(id)
{
	xGetElementById(id).setAttribute('class','botonD ui-corner-all');
	xGetElementById(id).setAttribute('onclick',"");
}

//Copia un array u objeto de cualquier tipo
function copy(o){
	if (typeof o != "object" || o === null) return o;
	var r = o.constructor == Array ? [] : {};
	for (var i in o) {
		r[i] = copy(o[i]);
		}
    	return r;
	}

////Redimensionar la foto del personal para mostrar (a 100x100)
var ancho, alto;
function redimensionar(xx,yy)
{
    var n, p;
    var maxAlto=100;
    var maxAncho=100;
    alto=0;
    ancho=0;
    if (xx>yy)
    {
        if (xx>maxAncho)
        {
            n=xx-maxAncho;
            p=(n*100)/xx;
            alto=yy*(1-p/100);
        }
        else
        {
            n=maxAncho-xx;
            p=(n*100)/xx;
            alto=yy*(1+p/100);
        }
        ancho=maxAncho;
    }
    else if (xx<yy)
    {
        if (yy>maxAlto)
        {
            n=yy-maxAlto;
            p=(n*100)/yy;
            ancho=xx*(1-p/100);
        }
        else
        {
            n=maxAlto-yy;
            p=(n*100)/yy;
            ancho=xx*(1+p/100);
        }
        alto=maxAlto;
    }
    else
    {
        ancho=maxAncho;
        alto=maxAlto;
    }

    xGetElementById('imgFoto').width = ancho;
    xGetElementById('imgFoto').height = alto;
}

function enmascarar(valor,partes,largo,sep)
{
	var valor=desenmascarar(valor);
	valor=allTrim(valor)+'                   ';
	var k=0;
	var parte=new Array();
	for(var i=0; i<partes; i++) 
	{
		longitud=parseInt(largo[i],10);
		parte[i]=valor.slice(k,k+longitud);

		k+=longitud;
	}
	
	var num=parte.length;
	var cadena="";
	for(var i=0; i<num; i++) 
	{
		if (allTrim(parte[i])!="")
			cadena+=sep+allTrim(parte[i]);
	}

	cadena=cadena.substring(1,cadena.length);
	return cadena;
}

function desenmascarar(valor)
{
	cadena=allTrim(str_replace(str_replace(valor,"","-"),""," "));
	return cadena;
}

function ponerMascara(id,partes,largo,sep)
{
	if(xGetElementById(id).value)
	{
		var cadena=enmascarar(xGetElementById(id).value,partes,largo,sep);
		xGetElementById(id).value=cadena;
	}
}

function quitarMascara(id)
{
	if(xGetElementById(id).value)
	{
		var cadena=desenmascarar(xGetElementById(id).value);
		xGetElementById(id).value=cadena;
	}
}

//------GRUPO DE FUNCIONES PARA CONVERTIR NÚMEROS A LETRAS

//Función mod, regresa el residuo de una división (módulo)
function mod(dividendo, divisor) 
{ 
resDiv = dividendo / divisor ;  
parteEnt = Math.floor(resDiv);		// Obtiene la parte entera de resDiv
parteFrac = resDiv - parteEnt;		// Obtiene la parte fraccionaria de la división
modulo=dividendo-(parteEnt*divisor);
//modulo = Math.round(parteFrac * divisor);  // Regresa la parte fraccionaria * la división (módulo) 
return modulo; 
}

//Función ObtenerParteEntDiv, regresa la parte entera de una división
function ObtenerParteEntDiv(dividendo , divisor) 
{ 
resDiv = dividendo / divisor ;  
parteEntDiv = Math.floor(resDiv); 
return parteEntDiv; 
}

//Función fraction_part, regresa la parte fraccionaria de una cantidad
function fraction_part(dividendo , divisor) 
{ 
resDiv = dividendo / divisor ;  
f_part = Math.floor(resDiv); 
return f_part; 
}


//Función string_literal_conversion es el núcleo de este grupo de funciones
//convierte el número a cadenas en español, manejando los casos especiales
//generales en el lenguaje español. 
function string_literal_conversion(number) 
{   
	// primero, divide el número en cientos, decenas y unidades,  
	// haciendo cascadas a través de las divisiones subsecuentes,
	// usando los módulos de cada división para la siguiente. 
	
	centenas = ObtenerParteEntDiv(number, 100);
	number = mod(number, 100);
	
	decenas = ObtenerParteEntDiv(number, 10);
	number = mod(number, 10);
	
	unidades = ObtenerParteEntDiv(number, 1);
	number = mod(number, 1);
	
	string_hundreds="";
	string_tens="";
	string_units="";
 
	// cascada hacia los cientos. Esto convertirá las partes de cientos a
	// su correspondiente cadena en español.
	if(centenas == 1){
		string_hundreds = "ciento ";
	}
	
	if(centenas == 2){
		string_hundreds = "doscientos ";
	}
	
	if(centenas == 3){
		string_hundreds = "trescientos ";
	}
	
	if(centenas == 4){
		string_hundreds = "cuatrocientos ";
	}
	
	if(centenas == 5){
		string_hundreds = "quinientos ";
	}
	
	if(centenas == 6){
		string_hundreds = "seiscientos ";
	}
	
	if(centenas == 7){
		string_hundreds = "setecientos ";
	}
	
	if(centenas == 8){
		string_hundreds = "ochocientos ";
	}
	
	if(centenas == 9){
		string_hundreds = "novecientos ";
	}
	
	// cascada hacia las decenas. Esto convertirá las partes de decenas a 
	// la cadena en español correspondiente. Note, sin embargo, que las cadenas entre 11 y 19 
	// son todas casos especiales. También 21-29 es un caso especial en español. 
	if(decenas == 1)
	{
		//Caso especial, depende de las unidades para cada conversión
		if(unidades == 1){
			string_tens = "once";
		}
		
		if(unidades == 2){
			string_tens = "doce";
		}
		
		if(unidades == 3){
			string_tens = "trece";
		}
		
		if(unidades == 4){
			string_tens = "catorce";
		}
		
		if(unidades == 5){
			string_tens = "quince";
		}
		
		if(unidades == 6){
			string_tens = "dieciseis";
		}
		
		if(unidades == 7){
			string_tens = "diecisiete";
		}
		
		if(unidades == 8){
			string_tens = "dieciocho";
		}
		
		if(unidades == 9){
			string_tens = "diecinueve";
		}
	}
	
	if(decenas == 2){
		string_tens = "veinti";
	}
	
	if(decenas == 3){
		string_tens = "treinta";
	}
	
	if(decenas == 4){
		string_tens = "cuarenta";
	}
	
	if(decenas == 5){
		string_tens = "cincuenta";
	}
	
	if(decenas == 6){
		string_tens = "sesenta";
	}
	
	if(decenas == 7){
		string_tens = "setenta";
	}
	
	if(decenas == 8){
		string_tens = "ochenta";
	}
	
	if(decenas == 9){
		string_tens = "noventa";
	}
	
	// cascada a través de las unidades. Esto convertirá las partes de unidades a las cadenas 
	// en español correspondientes. Note, sin embargo, que se hace un chequeo para ver si 
	// fueron usados los casos especiales 11-19. En ese caso, la conversión completa de 
	// las unidades individuales es ignorada puesto que ya fue hecha en la cascada de las decenas. 
	
	if (decenas == 1) 
	{
		string_units="";  // vacía el chequeo de unidades, puesto que ya ha sido manejado en el switche de las decenas 
	} 
	else 
	{
		if(unidades == 1){
			string_units = "un";
		}
		
		if(unidades == 2){
			string_units = "dos";
		}
		
		if(unidades == 3){
			string_units = "tres";
		}
		
		if(unidades == 4){
			string_units = "cuatro";
		}
		
		if(unidades == 5){
			string_units = "cinco";
		}
		
		if(unidades == 6){
			string_units = "seis";
		}
		
		if(unidades == 7){
			string_units = "siete";
		}
		
		if(unidades == 8){
			string_units = "ocho";
		}
		
		if(unidades == 9){
			string_units = "nueve";
		}
	}
	
	//casos especiales finales. Estas condiciones manejarán los casos especiales los cuales
	//no son tan generales como los de las cascadas. Básicamente cuatro: 
	
	// cuando se tiene 100, no se dice 'ciento' se dice 'cien' 
	// 'ciento' se usa sólo para [101 >= número > 199] 
	if (centenas == 1 && decenas == 0 && unidades == 0) 
	{ 
	   string_hundreds = "cien " ; 
	}  
	
	// cuando se tiene 10, no se dice ninguno de los casos especiales 11-19 
	// simplemente se dice 'diez' 
	if (decenas == 1 && unidades ==0) 
	{ 
	   string_tens = "diez " ; 
	} 
	
	// cuando se tiene 20, no se dice 'veinti', que sólo se usa para [21 >= número > 29] 
	if (decenas == 2 && unidades ==0) 
	{ 
	  string_tens = "veinte " ; 
	} 
	
	// para números >= 30, no se usa una sola palabra como veintiuno, 
	// se debe agregar 'y', y se usan dos palabras p.ej. 31 'treinta y uno'
	if (decenas >=3 && unidades >=1) 
	{ 
	   string_tens = string_tens+" y "; 
	} 
	
	// esta línea reúne todos los cientos, decenas y unidades en una cadena final 
	// y la retorna como el valor de la función.
	final_string = string_hundreds+string_tens+string_units;
	
	
	return final_string ; 

} //fin de la función string_literal_conversion()================================ 

//maneja algunos casos especiales externos. Especialmente los descriptores de millones, miles 
//y cientos. Puesto que se aplican las mismas reglas a todas las tríadas de números,  
//las descripciones se manejan fuera de la función de conversión de la cadena, para que así  
//pueda ser re-usada para cada tríada. 


function covertirNumLetras(number)
{
	number=number_format(number,2,".","");
	var number1=number;
	var cent = number1.split(".");
	var centimos = cent[1];
	
	if (centimos == 0 || centimos == undefined){
		centimos = "00";
	}
	
	if (number == 0 || number == "") 
	{ // si monto = 0, entonces no hacer ninguna conversión 
		centenas_final_string=" cero "; // monto es cero. 
	} 
	else 
	{

//------------------		
		mil_millon=ObtenerParteEntDiv(number, 1000000000);
		number = mod(number, 1000000000);
		
		if (mil_millon != 1)
		{
			mil_millon_final_string =string_literal_conversion(mil_millon) + " mil "; 
		}
		
		if (mil_millon == 1)
		{
			mil_millon_final_string = " mil "; 
		}
		
		if (mil_millon < 1) 
		{ 
			mil_millon_final_string = " "; 
		}
//-----------------
		
		var millions  = ObtenerParteEntDiv(number, 1000000); // primero, enviar los millones a la función de conversión de cadena
		number = mod(number, 1000000);

		if (millions != 0)
		{
			// Esta condición maneja el caso plural 
			if (millions == 1) 
			{              // si es sólo 1, usar 'millon'. si es > 1, usar 'millones' como un desriptor para esta tríada
				descriptor= " millon ";
			} 
			else 
			{                           
				descriptor = " millones ";
			} 
		} 
		else 
		{    
			descriptor = " "; // si son 0 millones, entonces no usar ningún descriptor. 
		} 

		millions_final_string = string_literal_conversion(millions)+descriptor;     

		thousands = ObtenerParteEntDiv(number, 1000);	// ahora, enviar los miles a la funcion de conversión de la cadena. 
		number = mod(number, 1000);            			

		//imprimir "mil:".miles;
		if (thousands != 1) 
		{                   // Esta condición elimina al descriptor
			thousands_final_string =string_literal_conversion(thousands) + " mil "; 
			//  descriptor = " mil ";          // si no hay ningunos miles en el monto
		} 
		
		if (thousands == 1)
		{
			thousands_final_string = " mil "; 
		}
		
		if (thousands < 1) 
		{ 
			thousands_final_string = " "; 
		} 

		// esto manejará los números entre 1 y 999 los cuales no necesitan ningún descriptor.

		centenas  = number;
		centenas_final_string = string_literal_conversion(centenas) ; 
	} //fin de if (number ==0) 

	/* Concatena los miles de millones, millones, miles y cientos*/
	cad = mil_millon_final_string+millions_final_string+thousands_final_string+centenas_final_string; 
	
	/* Convierte la cadena a Mayúsculas*/
	cad = cad.toUpperCase();       

	if (centimos.length>2)
	{   
		if(centimos.substring(2,3)>= 5)
		{
			centimos = centimos.substring(0,1)+(parseInt(centimos.substring(1,2))+1).toString();
		}
		else
		{
			centimos = centimos.substring(0,2);
		}
	}

	/* Concatena a los céntimos la cadena "/100" */
	if (centimos.length==1)
	{
		centimos = centimos+"0";
	}
	centimos = "con "+centimos+ "/100";
	
	/* Asigna el tipo de moneda, para 1 -> bolivar, para distinto de 1 -> bolivares*/
	if (number == 1)
	{
		moneda = " bolivar ";  
	}
	else
	{
		moneda = " bolivares ";  
	}

	//concatenamos toda la cadena y la retornamos
	letras=cad+moneda+centimos;
	letras=allTrim(str_replace(str_replace(letras.toLowerCase()," ", "  ")," ", "  "));
	letras=str_replace(letras,"millones de bolivares", "millones bolivares");
	letras=str_replace(letras,"millon de bolivares", "millon bolivares");
	return letras;
}
//------FIN GRUPO DE FUNCIONES PARA CONVERTIR NÚMEROS A LETRAS

function siguienteCampo(event,id_sig,funcion)
{
	if(event.keyCode==13)
	{
		if (id_sig!="ejecutar")
		{
			xGetElementById(id_sig).focus();
			return false;
		}
		else
			eval(funcion);	
	}
	else
		return true;
}

function seleccionarTexto(id)
{
	xGetElementById(id).focus();
	xGetElementById(id).select();
}

function recortarCadena(cadena,tamano)
{
	var longitud=cadena.length;
	
	if (longitud>tamano)
		cadena=cadena.substr(0,tamano)+"...";
	
	return cadena;
}

//agregadas por Víctor Meza
function toComa(id,decimales)
{
	var valor=parseFloat(document.getElementById(id).value);
	if (!isNaN(valor))
	{
		if(document.getElementById(id).value)
		{
			document.getElementById(id).value=number_format(document.getElementById(id).value, decimales, ",", ".");
		}
	}
}

function toPto(id)
{
	var valor=parseFloat(str_replace(str_replace(document.getElementById(id).value, "", "."),".",","));
	if (!isNaN(valor))
	{
		if(document.getElementById(id).value)
		{
			document.getElementById(id).value=valor;
		}
	}
}
