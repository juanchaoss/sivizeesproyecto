<?php
	session_start();
	if (!$_SESSION['usuario']) {
?>
		<script charset="utf-8">
			alert("La sesión ha sido cerrada por inactividad");
			window.location='../index.php';
		</script>
<?php
	}
?>
