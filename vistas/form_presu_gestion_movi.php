<?php
    include_once ('libs/control_sesion.php');
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Gestión de Movimientos</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="stylesheet" href="bootstrap-3.2.0-dist/css/bootstrap.css">
	<link rel="stylesheet" href="bootstrap-3.2.0-dist/css/datepicker.css">
	<!--link rel="stylesheet" href="bootstrap-3.2.0-dist/css/datepicker.css"-->
	<link rel="stylesheet" href="bootstrap-3.2.0-dist/css/bootstrap-theme.css">
	<!--link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap-responsive.css"-->
	<!--style>
		/*Escritorio*/
		@media(min-width: 1200px){
			 body{color: blue;}
		}
		/*Escritorio pequeno o tablet*/
		@media(min-width: 768px) and (max-width: 979px){
        	  body{color: green;}
		}/*tablet o smartphone*/
		@media(max-width: 767px){
			  body{color: red;}
		}/*smartphone*/
		@media(max-width: 480px){
			  body{color: orange;}
		}
	</style-->
</head>
<body>
	<div class="container-fluid"><!--Menu-->
		<div class="row">
			<div class="col-md-12">
			<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			  <div class="container-fluid">
			    <!-- Brand and toggle get grouped for better mobile display -->
			    <div class="navbar-header">
			      <button type="button" class="navbar-toggle" data-toggle="collapse"
			      data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
			      </button>
			    <a class="navbar-brand" href="index.php"><img  src="img/SIVIZE LOGO-menu.png"> SIVIZEES</a>
			    </div>

			    <!-- Collect the nav links, forms, and other content for toggling -->
			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			      <ul class="nav navbar-nav">
			        <!--li><a href="#">Link</a></li>
			        <li><a href="#">Link</a></li-->
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			          <span class="glyphicon glyphicon-user"></span> Usuarios<span class="caret"></span></a>
			          <ul class="dropdown-menu" role="menu">
			            <!--li><a href="form_usu_ac.php"><span class="glyphicon glyphicon-retweet">
			            	</span> Gestión de Usuarios</a></li-->
			            <!--li><a href="#myModal1" data-toggle="modal" onclick="listarUsuarios();"><span class="glyphicon glyphicon-align-justify"></span> Listar Usuarios</a></li-->
			            <li><a href="form_usu_cre.php">
			            	<span class="glyphicon glyphicon-plus-sign"></span> Gestión de Usuario</a></li>
			          </ul>
			        </li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			          <span class="glyphicon glyphicon-briefcase"></span> Empleados<span class="caret"></span></a>
			          <ul class="dropdown-menu" role="menu">
			            <li><a href="form_emp_re.php"><span class="glyphicon glyphicon-pencil"></span> Registro de Empleado</a></li>
			            <li><a href="form_emp_ac.php"><span class="glyphicon glyphicon-refresh"></span> Actualizar datos del empleado</a></li>
			          </ul>
			        </li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			          	<span class='glyphicon glyphicon-usd'></span> Presupuesto<span class="caret"></span></a>
			          <ul class="dropdown-menu" role="menu">
			            <li><a href="form_presu_crear_parti.php"><span class="glyphicon glyphicon-upload"></span> Crear Partida</a></li>
			            <li><a href="form_presu_crear_proyec.php"><span class="glyphicon glyphicon-upload"></span> Crear Proyecto</a></li>
			            <li><a href="form_presu_gestion_movi.php"><span class="glyphicon glyphicon-list-alt"></span> Gestionar Movimiento</a></li>
			            <li><a href="form_via_pre.php"><span class="glyphicon glyphicon-pushpin"></span> Gestión de Precios de Viáticos</a></li>
			          </ul>
			        </li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			          	<span class='glyphicon glyphicon-plane'></span> Viaticos<span class="caret"></span></a>
			          <ul class="dropdown-menu" role="menu">
			          <li><a href="#" onclick="window.open('http://www.me.gob.ve/servicios/recibo/consultacopia.php');"><span class="glyphicon glyphicon-download-alt"></span> Descargar Recibo de Pago</a></li>
			            <li><a href="form_via_soli.php"><span class="glyphicon glyphicon-folder-open"></span> Gestión de Solicitud de Viáticos</a></li>
			             <li><a href="lista_soli.php"><span class="glyphicon glyphicon-align-justify"></span> Gestión de Aprobación de Viáticos</a></li>
			             <li><a href="form_via_cal.php"><span class="glyphicon glyphicon-tasks"></span> Gestión de Cálculo de Viáticos</a></li>
			             <li><a href="lista_calc.php"><span class="glyphicon glyphicon-align-justify"></span> Gestión de Aprobación de Cálculo de Viáticos</a></li>
			             <li><a href="form_pago_via.php"><span class="glyphicon glyphicon-asterisk"></span> Pago de Solicitudes</a></li>
			            <li><a href="consulStatusSol.php"><span class="glyphicon glyphicon-list-alt"></span> Status de la Solicitud</a></li>
			            <li><a href="form_ciu.php"><span class="glyphicon glyphicon-globe"></span> Activar/Desactivar Ciudades</a></li>
			            <!--li><a href="#">Crear Periodo</a></li>
			            <li><a href="#">Crear Mes</a></li-->
			          </ul>
			        </li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			          	<span class='glyphicon glyphicon-list-alt'></span> Reportes<span class="caret"></span></a>
			          <ul class="dropdown-menu" role="menu">
			            <li><a href="reportes/rptListadoZona.php"><span class="glyphicon glyphicon-list-alt"></span> Listado de Solicitudes por Zona</a></li>
			            <li><a href="reportes/rptListadoDispon.php"><span class="glyphicon glyphicon-list-alt"></span> Listado de Disponibilidades por Partida</a></li>
			            <li><a href="reportes/rptListadoMov.php"><span class="glyphicon glyphicon-list-alt"></span> Listados de Movimientos por Partida</a></li>
			            <li><a href="reportes/rptListadoStatus.php"><span class="glyphicon glyphicon-list-alt"></span> Listados de Solicitudes por Status</a></li>
			            <li><a href="reportes/rptSolicitudesEmp.php"><span class="glyphicon glyphicon-list-alt"></span> Listados de Solicitudes por Empleado</a></li>
			            <li><a href="reportes/rptConsolidadoStatus.php"><span class="glyphicon glyphicon-list-alt"></span> Consolidado de Solicitudes por Status</a></li>
			            <li><a href="reportes/bus_avan.php"><span class="glyphicon glyphicon-search"></span> Busqueda Avanzada de Viáticos</a></li>
			          </ul>
			        </li>
			        <!--li><a href="#"><span class="glyphicon glyphicon-file"></span>Reportes PDF</a></li>
			        <li><a href="#"><span class="glyphicon glyphicon-cloud-download"></span>Descargar Manual</a></li-->
			      </ul>
			      <!--form class="navbar-form navbar-left" role="search">
			        <div class="form-group">
			          <input type="text" class="form-control" placeholder="Search">
			        </div>
			        <button type="submit" class="btn btn-default">Submit</button>
			      </form-->
			      <ul class="nav navbar-nav navbar-right">
			        <!--li><a href="#">Link</a></li-->
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			          	<span class="glyphicon glyphicon-user"></span> <?=$_SESSION['usuario']?><span class="caret"></span></a>
			          <ul class="dropdown-menu" role="menu">
			          <!--li><a href="">Cambiar contraseña</a></li-->
			            <li><a href="#" onclick="mostrarModal('Mensaje de Alerta',
			            '¿Esta seguro de Cerrar la sesión?', 'cerrarSesion();')">
			            	<span class="glyphicon glyphicon-log-out"></span> Cerrar Sesión</a></li>
			          </ul> <!--onclick="cerrarSesion();"-->
			        </li>
			      </ul>
			    </div><!-- /.navbar-collapse data-toggle="modal" data-target="#myModal"  " -->
			  </div><!-- /.container-fluid -->
			</nav>
			</div>
		</div>
	</div><!--Fin Menu-->
	<div class="span12">&nbsp;</div>
	<div class="span12">&nbsp;</div>
	<div class="span12">&nbsp;</div>
	<h3 class="text-center text-danger"><strong>Gestión de Movimientos</strong></h3><!--Encabezado-->
	<div class="container-fluid"><!--containerForm-->
		<div class="row">
			<div class="col-lg-4"></div>
			<div class="col-lg-4">
				<div class="panel panel-default">
				  <div class="panel-body">
				    <form role="form">
				    <div class="form-group">
							<label for="descripcion">Imputación Presupuestaria</label>
							<select class="form-control" id="impu_presu" onchange="mostrarDispon();">
							  <option value="0">Seleccione</option>
							</select>
					</div>
					<div class="form-group">
                        <label for="ejemplo_password_1">Monto Disponible</label>
                            <div class='input-group'>
                                <input type='text' id="dispon" readonly='readonly' class="form-control hab" style="text-align: right;"/>
                                <span class="input-group-addon">Bsf</span>
                            </div>
                        </div>
						<!--div class="form-group">
							<label for="">Cuenta</label>
							<input type="text" id="cuenta" class="form-control" readonly>
						</div-->
						<label class="radio-inline">
						  <input type="checkbox" name="traspaso" id="traspaso"> Traspaso de Partida
						</label>
						<div id="selectAfect">
						<div class="form-group">
							<label for="descripcion">Imputación Presupuestaria</label>
							<select class="form-control" id="impu_afect">
							  <option value="0">Seleccione</option>
							</select>
						</div>
					</div>
						<div class="form-group">
                        <label for="ejemplo_password_1">Monto</label>
                            <div class='input-group'>
                                <input type='text' placeholder="Monto del movimiento" onkeypress="return AcceptNum(event,this.id,false,false);" id="monto" class="form-control hab" style="text-align: right;"/>
                                <span class="input-group-addon">Bsf</span>
                            </div>
                        </div>
  						<div class="form-group">
					    	<label for="codigo">Descripcion</label>
					    	<textarea placeholder="Descripción" class="form-control" id="descrip" rows="3"></textarea>
						</div>
						<div class="form-group">
                          <label for="ejemplo_password_1">Fecha</label>
                              <div class='input-group date' id='datetimepicker1'>
                                  <input type='text' readonly="readonly" class="form-control hab" id="fecha" value="<?=date('Y-m-d')?>" />
                                  <span class="input-group-addon"><span class="glyphicon glyphicon-calendar">
                                   </span>
                                  </span>
                              </div>
                        </div>
                        <div class="form-group">
							<label for="descripcion">Banco</label>
							<select class="form-control" id="banco">
							  <option value="0">Seleccione</option>
							</select>
					    </div>
						<label class="radio-inline">
						  <input type="radio" name="inlineRadioOptions" checked="checked" id="inlineRadio1"> Cheque
						</label>
						<label class="radio-inline">
						  <input type="radio" name="inlineRadioOptions" id="inlineRadio2"> Transferencia
						</label>
						<label class="radio-inline">
						  <input type="radio" name="inlineRadioOptions" id="inlineRadio3"> Depósito
						</label>
						  <div class="form-group">
						    <label for="exampleInputEmail1">N° de Cheque, Transferencia o Depósito</label>
						    <input type="text" onkeypress="return soloNum(event);" class="form-control" id="num_refe" placeholder="N° de Cheque o Tranferencia">
						  </div>
						<div class="btn-group">
							<button type="button" onclick="guardarMovimiento();" class="btn btn-default center-block">Guardar
						    <span class="glyphicon glyphicon-floppy-disk"></span></button>
						    <button type='button' class='btn btn-success' onclick='listarMovimientos();' data-toggle="modal" data-target=".bs-example-modal-lg"
						    >Lista Movimientos <span class='glyphicon glyphicon-list'></span></button>
						</div>
				    </form>
				  </div>
               </div>
			</div>
			<div class="col-lg-4"></div>
		</div>
		<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;
        	</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title text-center" id="myModalLabel"></h4>
      </div>
      <div class="modal-body">
        <p align="center" id="mensajeModal"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="btnAccionModal" >Aceptar</button>
      </div>
    </div>
  </div>
</div><!-- Fin Modal 1 -->
<!--COMIENZO DE MODAL MOVI-->
	<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"
	aria-labelledby="myLargeModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title text-center text-danger" id="myModalLabel"><strong>Listado de Movimientos</strong></h4>
		      </div>
		      <div class="modal-body"><!--table-responsive   height:400px; overflow: auto;-->
		      <div style="height:420px; overflow: auto;">
		        <div class="table-responsive">
		        	<table class="table table-hover" id='tbl_movi'>
		        	<thead>
		        		<tr>
		        			<th>#</th>
		        			<th>Descripción del Movimiento</th>
		        			<th>Monto Bs.</th>
		        			<th>Fecha del Movimiento</th>
		        			<th>Modo de Movimiento</th>
		        			<th>Partida</th>
		        			<th>Num. Referencia</th>
		        		</tr>
		        	</thead>
		        	<tbody id='head_movi'>
		        	</tbody>
		        </table>
		       </div>
		      </div>
		      <div class="modal-footer">
		      <form class="form-inline">
		      	<div class="form-group pull-left">
				    <label for="exampleInputEmail1">Monto Final</label>
				    <input type="text" class="form-control" id="mont_final" placeholder="">
				</div>&nbsp;&nbsp;&nbsp;
				<div class="form-group pull-left">
					<label for="exampleInputEmail1">I. Presupuestarial</label>
					<select name="" class="form-control" id="impu_presu_aux">
						<option value="0" checked>Seleccione</option>
					</select>
				</div><br><br>
				<div class="form-group pull-left">
					<label for="ejemplo_password_1">Mes</label>
					<select name="" id="mes" class="form-control">
						<option value="0" checked>Seleccione el mes</option>
						<option value="01">Enero</option>
						<option value="02">Febrero</option>
						<option value="03">Marzo</option>
						<option value="04">Abril</option>
						<option value="05">Mayo</option>
						<option value="06">Junio</option>
						<option value="07">Julio</option>
						<option value="08">Agosto</option>
						<option value="09">Septiembre</option>
						<option value="10">Octubre</option>
						<option value="11">Noviembre</option>
						<option value="12">Diciembre</option>
					</select>
					<input type="hidden" id="cuenta">
				</div>&nbsp;&nbsp;
				<div class="form-group pull-left">
					<label for="ejemplo_password_1">Año</label>
					<input type="text" size="10" class="form-control" id="anio">
				</div>
		      </form>
		      <div class="btn-group">
		      	 <button type="button" onclick="filtrarSumaPartida();" class="btn btn-success"><span class="glyphicon glyphicon-folder-open"></span> Consultar</button>
		        <button type="button" class="btn btn-info" onclick="mostrarReporteMovi();" onclick="">Mostrar Reporte <span class="glyphicon glyphicon-list-alt"></span></button>
		        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		      </div>
		      </div>
	    </div>
	    </div>
	  </div>
   </div>
<!--FIN DE MODAL MOVI-->
</div>
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/jquery-ui-1.10.3.custom.js"></script>
	<script src="bootstrap-3.2.0-dist/js/bootstrap.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src='../controladores/js/presupuesto.js'></script>
	<script src="../js/sesion.js"></script>
	<script src="js/comunes.js"></script>
	<script>
		jQuery(document).ready(function() {
			cargarParti();
			$("div[id^='datetimepicker']").datepicker({
		       	format: 'yyyy-mm-dd',
		       	language: "es",
		        autoclose:true
	        });
		});
	</script>
	<script>
	listarBanco();
		function mostrarModal(titulo, msg, funcion){
			$('#myModalLabel').html(titulo);
			$('#mensajeModal').html(msg);
			$('#btnAccionModal').unbind('click').click({funcion:funcion}, function(eventObject){
				eval(eventObject.data.funcion); //eval('cerrarSesion();');
			});
			$('#myModal').modal('show');
		}
		$("#selectAfect").hide();
		/*if ($("#traspaso").is(':checked')) {
			$("#selectAfect").show();
		}*/
		$("#traspaso").click(function(){
		  if ($("#traspaso").is(':checked')) {
		  	$("#selectAfect").show();
		  	$('#banco').attr("disabled", true);
		  	$('#inlineRadio1').prop("checked", true)
		  	$("input[id^='inlineRadio']").attr("disabled", true);
		  	$('#num_refe').val('');
		  	$('#num_refe').attr("disabled", true);
		  }else{
		  	$("#selectAfect").hide();
		  	$('#banco').removeAttr("disabled");
		  	$("input[id^='inlineRadio']").removeAttr("disabled");
		  	$('#num_refe').removeAttr("disabled");
		  }
		});
		$('#mont_final').attr("disabled", true);

		var f   = new Date();
	    var mes = f.getMonth()+1;
		$("#anio").val(f.getFullYear());
			if (parseInt(mes,10)<10) {
				mes = '0'+mes;
			}
		$("#mes").val(mes);
	//listarMovimientos();
	</script>
</body>
</html>