<?php
session_start();
    include_once ('libs/control_sesion.php');
    
    if ($_SESSION['nivel'] != 1) {
    	?>
    	<script>
    		alert("Usted no tiene acceso a esta parte del sistema");
    		location.href='../';
    	</script>
    	<?php
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Cálculo del Viático</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="stylesheet" href="bootstrap-3.2.0-dist/css/bootstrap.css">
	<link rel="stylesheet" href="bootstrap-3.2.0-dist/css/datepicker.css">
	<link rel="stylesheet" href="bootstrap-3.2.0-dist/css/bootstrap-theme.css">
	<!--link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap-responsive.css"-->
	<!--style>
		/*Escritorio*/
		@media(min-width: 1200px){
			 body{color: blue;}
		}
		/*Escritorio pequeno o tablet*/
		@media(min-width: 768px) and (max-width: 979px){
        	  body{color: green;}
		}/*tablet o smartphone*/
		@media(max-width: 767px){
			  body{color: red;} 
		}/*smartphone*/
		@media(max-width: 480px){
			  body{color: orange;}
		}
	</style-->
</head>
<body>
	<div class="container-fluid"><!--Menu-->
		<div class="row">
			<div class="col-md-12">
			<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			  <div class="container-fluid">
			    <!-- Brand and toggle get grouped for better mobile display -->
			    <div class="navbar-header">
			      <button type="button" class="navbar-toggle" data-toggle="collapse"
			      data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
			      </button>
			    <a class="navbar-brand" href="index.php"><img  src="img/SIVIZE LOGO-menu.png"> SIVIZEES</a>
			    </div>

			    <!-- Collect the nav links, forms, and other content for toggling -->
			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			      <ul class="nav navbar-nav">
			        <!--li><a href="#">Link</a></li>
			        <li><a href="#">Link</a></li-->
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			          <span class="glyphicon glyphicon-user"></span> Usuarios<span class="caret"></span></a>
			          <ul class="dropdown-menu" role="menu">
			            <!--li><a href="form_usu_ac.php"><span class="glyphicon glyphicon-retweet">
			            	</span> Gestión de Usuarios</a></li-->
			            <!--li><a href="#myModal1" data-toggle="modal" onclick="listarUsuarios();"><span class="glyphicon glyphicon-align-justify"></span> Listar Usuarios</a></li-->
			            <li><a href="form_usu_cre.php">
			            	<span class="glyphicon glyphicon-plus-sign"></span> Gestión de Usuario</a></li>
			          </ul>
			        </li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			          <span class="glyphicon glyphicon-briefcase"></span> Empleados<span class="caret"></span></a>
			          <ul class="dropdown-menu" role="menu">
			            <li><a href="form_emp_re.php"><span class="glyphicon glyphicon-pencil"></span> Registro de Empleado</a></li>
			            <li><a href="form_emp_ac.php"><span class="glyphicon glyphicon-refresh"></span> Actualizar datos del empleado</a></li>
			          </ul>
			        </li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			          	<span class='glyphicon glyphicon-usd'></span> Presupuesto<span class="caret"></span></a>
			          <ul class="dropdown-menu" role="menu">
			            <li><a href="form_presu_crear_parti.php"><span class="glyphicon glyphicon-upload"></span> Crear Partida</a></li>
			            <li><a href="form_presu_crear_proyec.php"><span class="glyphicon glyphicon-upload"></span> Crear Proyecto</a></li>
			            <li><a href="form_presu_gestion_movi.php"><span class="glyphicon glyphicon-list-alt"></span> Gestionar Movimiento</a></li>
			            <li><a href="form_via_pre.php"><span class="glyphicon glyphicon-pushpin"></span> Gestión de Precios de Viáticos</a></li>
			          </ul>
			        </li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			          	<span class='glyphicon glyphicon-plane'></span> Viaticos<span class="caret"></span></a>
			          <ul class="dropdown-menu" role="menu">
			          <li><a href="#" onclick="window.open('http://www.me.gob.ve/servicios/recibo/consultacopia.php');"><span class="glyphicon glyphicon-download-alt"></span> Descargar Recibo de Pago</a></li>
			            <li><a href="form_via_soli.php"><span class="glyphicon glyphicon-folder-open"></span> Gestión de Solicitud de Viáticos</a></li>
			             <li><a href="lista_soli.php"><span class="glyphicon glyphicon-align-justify"></span> Gestión de Aprobación de Viáticos</a></li>
			             <li><a href="form_via_cal.php"><span class="glyphicon glyphicon-tasks"></span> Gestión de Cálculo de Viáticos</a></li>
			             <li><a href="lista_calc.php"><span class="glyphicon glyphicon-align-justify"></span> Gestión de Aprobación de Cálculo de Viáticos</a></li>
			             <li><a href="form_pago_via.php"><span class="glyphicon glyphicon-asterisk"></span> Pago de Solicitudes</a></li>
			            <li><a href="consulStatusSol.php"><span class="glyphicon glyphicon-list-alt"></span> Status de la Solicitud</a></li>
			            <li><a href="form_ciu.php"><span class="glyphicon glyphicon-globe"></span> Activar/Desactivar Ciudades</a></li>
			            <!--li><a href="#">Crear Periodo</a></li>
			            <li><a href="#">Crear Mes</a></li-->
			          </ul>
			        </li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			          	<span class='glyphicon glyphicon-list-alt'></span> Reportes<span class="caret"></span></a>
			          <ul class="dropdown-menu" role="menu">
			            <li><a href="reportes/rptListadoZona.php"><span class="glyphicon glyphicon-list-alt"></span> Listado de Solicitudes por Zona</a></li>
			            <li><a href="reportes/rptListadoDispon.php"><span class="glyphicon glyphicon-list-alt"></span> Listado de Disponibilidades por Partida</a></li>
			            <li><a href="reportes/rptListadoMov.php"><span class="glyphicon glyphicon-list-alt"></span> Listados de Movimientos por Partida</a></li>
			            <li><a href="reportes/rptListadoStatus.php"><span class="glyphicon glyphicon-list-alt"></span> Listados de Solicitudes por Status</a></li>
			            <li><a href="reportes/rptSolicitudesEmp.php"><span class="glyphicon glyphicon-list-alt"></span> Listados de Solicitudes por Empleado</a></li>
			            <li><a href="reportes/rptConsolidadoStatus.php"><span class="glyphicon glyphicon-list-alt"></span> Consolidado de Solicitudes por Status</a></li>
			            <li><a href="reportes/bus_avan.php"><span class="glyphicon glyphicon-search"></span> Busqueda Avanzada de Viáticos</a></li>
			          </ul>
			        </li>
			        <!--li><a href="#"><span class="glyphicon glyphicon-file"></span>Reportes PDF</a></li>
			        <li><a href="#"><span class="glyphicon glyphicon-cloud-download"></span>Descargar Manual</a></li-->
			      </ul>
			      <!--form class="navbar-form navbar-left" role="search">
			        <div class="form-group">
			          <input type="text" class="form-control" placeholder="Search">
			        </div>
			        <button type="submit" class="btn btn-default">Submit</button>
			      </form-->
			      <ul class="nav navbar-nav navbar-right">
			        <!--li><a href="#">Link</a></li-->
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			          	<span class="glyphicon glyphicon-user"></span> <?=$_SESSION['usuario']?><span class="caret"></span></a>
			          <ul class="dropdown-menu" role="menu">
			          <!--li><a href="">Cambiar contraseña</a></li-->
			            <li><a href="#" onclick="mostrarModal('Mensaje de Alerta',
			            '¿Esta seguro de Cerrar la sesión?', 'cerrarSesion();')">
			            	<span class="glyphicon glyphicon-log-out"></span> Cerrar Sesión</a></li>
			          </ul> <!--onclick="cerrarSesion();"-->
			        </li>
			      </ul>
			    </div><!-- /.navbar-collapse data-toggle="modal" data-target="#myModal"  " -->
			  </div><!-- /.container-fluid -->
			</nav>
			</div>
		</div>
	</div><!--Fin Menu-->
	<div class="span12">&nbsp;</div>
	<div class="span12">&nbsp;</div>
	<div class="span12">&nbsp;</div>
  <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;
        	</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title text-center" id="myModalLabel"></h4>
      </div>
      <div class="modal-body">
        <p align="center" id="mensajeModal"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="btnAccionModal" >Aceptar</button>
      </div>
    </div>
  </div>
</div><!-- Fin Modal 1 -->
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-2">
			<div class="input-group">
			  <input type="text" id="nSoli" data-placement="bottom" data-toggle="tooltip" data-placement="bottom"
	           title="Usuario de sistema" onkeypress="return soloNum(event);" class="form-control" placeholder='Nro. Solicitud'>
			  <span class="input-group-btn">
			    <button class="btn btn-default" onclick="buscarSolicitud($('#nSoli').val());" type="button"><span class="glyphicon glyphicon-search"></span></button>
			  </span>
            </div><br>
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal22" onclick="listarViaticos();">Listar - Solicitudes
            	<span class="glyphicon glyphicon-th-list"></span>
            </button>
		</div>
		<div class="col-lg-8">
			<h3 class="text-center text-danger"><strong>Gestión de Cálculo de Viáticos</strong></h3>
			<div role="tabpanel">
			  <!-- Nav tabs -->
			  <ul class="nav nav-tabs" role="tablist">
			    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Datos del Solicitante</a></li>
			    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Datos de la Misión</a></li>
			    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Relación de Gastos</a></li>
			  </ul>
			  <!-- Tab panes -->
			  <div class="tab-content">
			    <div role="tabpanel" class="tab-pane active fade in active" id="home">
			    <h4 class="text-danger"><strong>Datos del Solicitante</strong></h4>
					<form class="form-inline">
						<div class="form-group">
						   <strong>Cédula:</strong> &nbsp; <input type="text" class="form-control" id="ced_em" placeholder="Cédula" readonly="readonly" data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom">
						  </div>
						  <div class="form-group">
						   <strong>Nombre:</strong> &nbsp; <input type="text" class="form-control" id="nom_em" placeholder="Nombre" readonly="readonly">
						  </div>
						  <div class="form-group">
						   <strong>Apellido:</strong> &nbsp; <input type="text" class="form-control" id="ape_em" placeholder="Apellido" readonly="readonly"><br><br>
						  </div>
						 <div class="form-group">
						   &nbsp;<strong>Cargo:</strong> &nbsp; <input type="text" class="form-control" id="cargo_em" placeholder="Cargo que ocupa" readonly="readonly">
						 </div>
						 <div class="form-group">
						   <strong>Estado:</strong> &nbsp;&nbsp;&nbsp; <input type="text" class="form-control" id="esta" placeholder="Estado" readonly="readonly">
						 </div>
						 <div class="form-group">
						  <strong> Ciudad:</strong> &nbsp;&nbsp;&nbsp; <input type="text" class="form-control" id="ciu" placeholder="Ciudad" readonly="readonly">
						 </div>
						 <div class="form-group"><br>
						  <strong> Dirección:</strong> &nbsp; <input size="100" type="text" class="form-control" id="direc" placeholder="Dirección" readonly="readonly">
						 </div>
						  <br> <strong>Sexo:</strong>
						  <label class="radio-inline">
						  <input type="radio" disabled="disabled" name="inlineRadioOptions" id="sexoM" value="option1"> M
						  <input type="hidden" id="hidempleado">
						</label>
						<label class="radio-inline">
						  <input type="radio" disabled="disabled" name="inlineRadioOptions" id="sexoF" value="option2"> F
						</label>
					</form>
			    </div>
			    <div role="tabpanel" class="tab-pane fade" id="profile">
			    	<h4 class="text-danger"><strong>Datos de la Misión</strong></h4>
			    	<form class="form-inline">
			    		<!--rgehteth-->
			    		<strong>Estado Origen</strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select class="form-control" id="esta_ori">
	                  																<option checked value="0">Seleccione</option>
	                  				                                            </select>
	                  <strong>Ciudad de Origen</strong> <select class="form-control" id="ciu_ori">
	                  						<option checked value="0">Seleccione</option>
	                  				   </select>
	                  				  <strong>Estado Destino</strong> <select class="form-control" id="esta_des">
	                  						<option checked value="0">Seleccione</option>
	                  				   </select><br><br>
	                  	<strong>Ciudad de Destino</strong> <select class="form-control" id="ciu_des" >
	                  							<option checked value="0">Seleccione</option>
	                  					  </select>
			    			<div class="form-group">
							   <strong>Lugar de Destino</strong> <input type="text" readonly="readonly" class="form-control" size="100" id="lugardes" placeholder="Lugar de Destino"><br><br>
							    <div class="form-group">
			                        <strong>Fecha de la misión: Desde</strong>&nbsp;
			                        <div class='input-group date' id='datetimepicker1'>
			                            <input type='text' readonly="readonly" class="form-control hab" id="fedesde" />
			                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar">
			                            	</span>
			                            </span>
			                        </div>
			                  </div>
			                  <div class="form-group">
				                    &nbsp;&nbsp;<strong>Hasta</strong>&nbsp;
				                    <div class='input-group date' id='datetimepicker1'>
				                        <input type='text' readonly="readonly" class="form-control hab" id="fehasta"/>
				                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar">
				                        	</span>
				                        </span>
				                    </div>
				              </div><br><br>
							  </div>
							  <input type="hidden" id="idHidden">
							   <div class="form-group">
							   	 <strong>Motivo del Viaje</strong> <textarea name="" readonly="readonly" id="motivo" cols="50" rows="2" class="form-control" placeholder='Motivo del viaje'></textarea>
							   </div><br><br>
								<strong>Transporte Utilizado de Ida:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<label class="radio-inline">
								  <input type="radio" disabled="disabled" name="radtrans" checked="checked" id="transa" value="option1"> <strong>Aéreo</strong>
								</label>
								<label class="radio-inline">
								  <input type="radio" disabled="disabled" name="radtrans" id="transt" value="option2"> <strong>Terrestre</strong>
								</label>
								<label class="radio-inline">
								  <input type="radio" disabled="disabled" name="radtrans" id="transo" value="option3"> <strong>Otros</strong>
								</label>
								<div class="col-lg-8">
									&nbsp;
								</div>
								<div class="form-group">
							    	<input type="text" class="form-control" id="otro" placeholder="Otro">
							    </div><br><br>

					    <strong>Transporte Utilizado de Vuelta:</strong>
						<label class="radio-inline">
						  <input type="radio" name="radtransvu" disabled="disabled" checked="checked" id="transavu" value="option1"> <strong>Aéreo</strong>
						</label>
						<label class="radio-inline">
						  <input type="radio" name="radtransvu" disabled="disabled" id="transtvu" value="option2"> <strong>Terrestre</strong>
						</label>
						<label class="radio-inline">
						  <input type="radio" name="radtransvu" disabled="disabled" id="transovu" value="option3"> <strong>Otros</strong>
						</label>
						<div class="form-group">
					    	<input type="text" class="form-control" disabled="disabled" id="otrovu" placeholder="Otro">
					    </div>
			    		<!--vfrdgertrt-->
			    	</form>
			    </div>
			    <div role="tabpanel" class="tab-pane fade" id="messages">
					<h4 class="text-danger"><strong>Relacion de Gastos</strong></h4>
					<form class="form-inline">
					<div class="form-group">
	            	    	<strong>Proyecto:</strong> <select name="" class="form-control" id="proyec_presu" onchange="buscarCuentasProyecto(this.value);">
	            	    				<option value="0">Seleccione</option>
	            	    			  </select>
	            	    </div>
							<div class="form-group">
	            				<strong>Imputación P.</strong> <input type="text" readonly="readonly" class="form-control" id="impu_presu">
	            	        </div>
	            	    <!--div class="form-group">
	            	    	Proyecto: <select name="" class="form-control" id="proyec_presu" onchange="buscarCuentasProyecto(this.value);">
	            	    				<option value="0">Seleccione</option>
	            	    			  </select>
	            	    </div-->
	            	     <div class="col-lg-12">
	            	    	&nbsp;
	            	    </div>
	            	    <div class="form-group">
	            	    	<strong>Cuenta:</strong> <input type="text" readonly="readonly" id="cuenta_presu" class="form-control">
	            	    </div>
	            	</form>
	            	<div class="container-fluid">
	            		<div class="row">
	            			<div class="col-lg-12">
	            				<form id="form_precios_via" class="form-inline" role="form" action="">
						<hr>
						<div class="form-group">
							<strong>Pasaje de Ida</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<select name="selprecio_ida" id="selprecio_ida" class="form-control" onchange="verificarIdaVu(this.id);">
								<option id="preciovia_0_0" value="">Selecione</option>
								<option id="preciovia_1_0" value="0">Aéreo</option>
								<option id="preciovia_5_0" value="0">Terrestre</option>
								<option id="preciovia_6_0" value="0">Otro</option>
							</select>
							<input id="preciovia_ida" size="8" class="form-control" type="text" readonly="readonly">
							<input id="preciovia_0" class="form-control" type="hidden">
						</div>
						<div class="form-group">
							<strong>Monto</strong> <input id="montovia_0" onkeypress="return AcceptNum(event,this.id,false,false);" size="5" type="text" class="form-control mitotal">
						</div>
						<div class="form-group">
							<strong>Cantidad</strong>
							<input id="cantvia_0" size="3" class="form-control mitotal" type="text"  onkeypress="return soloNum(event);" >
						</div>
						<div class="form-group">
							<strong>Total</strong>
							<input id="totalvia_0" size="3" class="form-control" type="text" readonly="">
						</div>
						<hr>
						<div class="form-group">
							<strong>Pasaje de Vuelta</strong>
							<select name="selprecio_vu" id="selprecio_vu" class="form-control" onchange="verificarIdaVu(this.id);">
								<option id="preciovia_0_1" value="">Selecione</option>
								<option id="preciovia_1_1" value="0">Aéreo</option>
								<option id="preciovia_5_1" value="0">Terrestre</option>
								<option id="preciovia_6_1" value="0">Otro</option>
							</select>
							<input id="preciovia_vu" size="8"  class="form-control"  type="text" readonly="readonly">
							<input id="preciovia_1" class="form-control" type="hidden">
						</div>
						<div class="form-group">
							<strong>Monto</strong> <input id="montovia_1" onkeypress="return AcceptNum(event,this.id,false,false);" size="5" type="text" class="form-control mitotal">
						</div>
						<div class="form-group">
							<strong>Cantidad</strong> 
							<input id="cantvia_1" size="3" class="form-control mitotal" type="text" onkeypress="return soloNum(event);" >
						</div>
						<div class="form-group">
							<strong>Total</strong> 
							<input id="totalvia_1" size="3" class="form-control" type="text" readonly="">
						</div>
						<hr>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<div class="form-group">
							<strong>Comida</strong> 
							<input id="preciovia_2" size="8" class="form-control" type="text" readonly="readonly">
						</div>
						<div class="form-group">
							<strong>Monto</strong> <input id="montovia_2" onkeypress="return AcceptNum(event,this.id,false,false);" size="5" type="text" class="form-control mitotal">
						</div>
						<div class="form-group">
							<strong>Cantidad</strong> 
							<input id="cantvia_2" size="3" class="form-control mitotal" type="text"  onkeypress="return soloNum(event);" >
						</div>
						<div class="form-group">
							<strong>Total</strong>
							<input id="totalvia_2" size="3" class="form-control" type="text" readonly="">
						</div>
						<hr>
						<div class="form-group">
							<strong>Hospedaje</strong> 
							<input id="preciovia_3" size="8" class="form-control" type="text" readonly="readonly">
						</div>
						<div class="form-group">
							<strong>Monto</strong> <input id="montovia_3" onkeypress="return AcceptNum(event,this.id,false,false);" size="5" type="text" class="form-control mitotal">
						</div>
						<div class="form-group">
							<strong>Cantidad </strong>
							<input id="cantvia_3" size="3" class="form-control mitotal" type="text" onkeypress="return soloNum(event);">
						</div>
						<div class="form-group">
							<strong>Total</strong> 
							<input id="totalvia_3" size="3" class="form-control" type="text" readonly="">
						</div>
						<div class="col-lg-12"> </div>
						<hr>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<div class="form-group">
							<strong>Taxi</strong> 
							<input id="preciovia_4" size="8" class="form-control" type="text" readonly="readonly">
						</div>
						<div class="form-group">
							<strong>Monto</strong> <input id="montovia_4"  onkeypress="return AcceptNum(event,this.id,false,false);" size="5" type="text" class="form-control mitotal">
						</div>
						<div class="form-group">
							<strong>Cantidad </strong>
							<input id="cantvia_4" size="3" class="form-control mitotal" type="text" onkeypress="return soloNum(event);">
						</div>
						<div class="form-group">
							<strong>Total </strong>
							<input id="totalvia_4" size="3" class="form-control" type="text" readonly="">
                        </div>
	            	</form>
	            			</div>
	            		</div>
	            	</div><br>
	            	<form action="" class="form-inline">
	            		<strong>Total Viáticos</strong> <input type="text" readonly class="form-control" id="preciTotal" placeholder=''>
	            	</form>
	            	<button type="button" onclick="guardarGastos();"
						class="btn btn-primary pull-right">Guardar <span class="glyphicon glyphicon-floppy-disk"></span>
					</button>
	            <div class="col-lg-12">
	            	&nbsp;
	            </div>
	             <div class="col-lg-12">
	            	&nbsp;
	            </div>
					</form>
			    </div>
			  </div>
			</div>
		</div>
		<div class="col-lg-2"></div>
	</div>
</div>

            </div>
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
<div class="col-lg-1"></div>
<!-- /.col-lg-12 -->
   </div-->
   <!--MODAL LISTA_SOLI-->
   	 <div class="modal fade bs-example-modal-lg" id='myModal22' tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-danger" id="myModalLabel"><strong>Listado de Solicitudes</strong></h4>
      </div>
      <div class="modal-body">
	  <div style="height: 200px; overflow: auto;">
	       <div class="table-responsive">
	       		 <table id="tbl-sol" class="table table-bordered table-striped">
	        	<tr>
	        		<td><strong>#</strong></td>
	        		<td><strong>N° Solicitud</strong></td>
	        		<td><strong>Lugar D.</strong></td>
	        		<td><strong>Fecha Desde</strong></td>
	        		<td><strong>Fecha Hasta</strong></td>
	        		<td><strong>Motivo</strong></td>
	        		<td><strong>T. Transporte</strong></td>
	        		<td><strong>Estado S.</strong></td>
	        	</tr>
	        </table>
	       </div>
	    </div>
     </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
   <!--FIN DE MODAL LISTA_SOLI-->
</div>
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/comunes.js"></script>
	<script src="bootstrap-3.2.0-dist/js/bootstrap.js"></script>
	<script src="js/jquery-ui-1.10.3.custom.js"></script>
	<script src="../controladores/js/empleado.js"></script>
	<script src="../controladores/js/viatico.js"></script>
	<script src='../controladores/js/presupuesto.js'></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/comunes.js"></script>
	<script src="../js/sesion.js"></script>

	<script>
		buscarProyectos();
		listarEstados();
		function mostrarModal(titulo, msg, funcion){
			$('#myModalLabel').html(titulo);
			$('#mensajeModal').html(msg);
			$('#btnAccionModal').unbind('click').click({funcion:funcion}, function(eventObject){
				eval(eventObject.data.funcion); //eval('cerrarSesion();');
			});
			$('#myModal').modal('show');
		}
		$('#usuario_aux').tooltip('show');
		$("div[id^='datetimepicker']").datepicker({
       			format: 'yyyy-mm-dd',
       			language: "es",
        		autoclose:true
    	});
		//bloquearBotonesUsu();
		//$('#ced_aux').tooltip('show');
		$("#otro").attr('disabled', true);
		$("#transo").click(function(){
		  $("#otro").removeAttr('disabled');
		});
		//generarInputsPrecios();
		$('#esta_ori').attr("disabled", true);
		$('#esta_des').attr("disabled", true);
		$('#ciu_ori').attr("disabled", true);
		$('#ciu_des').attr("disabled", true);

		$("input.mitotal").blur(function(event) {
			event.preventDefault();
			var pt = this.id.split('_');
			var indice = pt[1];
			var cant = 0;
			var error = false;
			if($("#cantvia_"+indice).val() != ''){
				cant = $("#cantvia_"+indice).val();
			}
			var monto = 0;
			/*if($("#montovia_"+indice).val() != ''){
				monto = $("#montovia_"+indice).val();
			}*/
			var ind = indice;
				if (indice == 0) {
					ind = 'ida';
				}else if(indice == 1){
					ind = 'vu';
				}
			if($.trim($("#preciovia_"+ind).val()) == '' || $.trim($("#preciovia_"+ind).val()) == 0){
				$("#montovia_"+indice).val('');
			}else{
				if($("#montovia_"+indice).val() != ''){
					monto = $("#montovia_"+indice).val();
				}
				
				var pt2 = $("#preciovia_"+ind).val().split('-');
				var min = parseFloat(pt2[0]);
				var max = parseFloat(pt2[1]);
				if (monto < min || monto > max) {
					error = true;
					$("#montovia_"+indice).val('');
				};
			}
			$("#totalvia_"+indice).val(parseFloat(monto)*parseInt(cant,10));
			calcularTotal();

			if (error) {
				//alert("El monto debe estar comprendido entre "+min+" y "+max);

			};
		});

		/*$("input[id^='montovia_']").blur(function(event) {
			var pt = this.id.split('_');
			var indice = pt[1];

			
			}
		});*/

		function calcularTotal(){
			var total = 0;
			$("input[id^='montovia_']").each(function(index, el) {
				var pt = this.id.split('_');
				var indice = pt[1];
				var cant = 0;
				if($("#cantvia_"+indice).val() != ''){
					cant = $("#cantvia_"+indice).val();
				}
				var monto = 0;
				if($("#montovia_"+indice).val() != ''){
					monto = $("#montovia_"+indice).val();
				}
				total = parseFloat(total)+(parseFloat(monto)*parseInt(cant,10));
				$("#totalvia_"+indice).val(parseFloat(monto)*parseInt(cant,10));
			});
			$('#preciTotal').val(total);
		}

		function verificarIdaVu(id){
			var pt = id.split('_');
			var indice = pt[1];
			var ind = 0;
			if (indice == 'vu') {
				ind = 1;
			};
			$('#preciovia_'+indice).val($('#selprecio_'+indice).val());  
			if ($('#selprecio_'+indice).val() == '' || $('#selprecio_'+indice).val() == 0) {
				$("#montovia_"+ind).val('');
			}else{
				
				$('#preciovia_'+ind).val($('#selprecio_'+indice).find('option:selected').attr('id'));
			}
			calcularTotal();
		}
	</script>
	<!--script src='../controladores/js/reportes.js'></script-->
</body>
</html>