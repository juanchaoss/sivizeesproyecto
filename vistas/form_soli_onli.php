<?php
session_start();
    if (!$_SESSION['usuarioOnline'] || !$_SESSION['permitido_online']) {
        header('location: ../login.php');
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Sistema de Gestión de Viaticos onLine</title>
    <link href="css2/css/bootstrap.css" rel="stylesheet">
    <link href="css2/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
    <link href="css2/css/plugins/timeline.css" rel="stylesheet">
    <link href="css2/css/sb-admin-2.css" rel="stylesheet">
    <link href="css2/css/plugins/morris.css" rel="stylesheet">
    <link href="css2/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand text-danger" href="index_Online.php">Sistema de Gestión de Viáticos</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>Salir del Sistema<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> <?=$_SESSION['realname']?></a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="#" onClick='cerrarSessionOnline();'><i class="fa fa-sign-out fa-fw"></i> Cerrar Sesión</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a class="active" href="index_Online.php"><i class="glyphicon glyphicon-home"></i> Inicio</a>
                        </li>
                        <li>
                            <a href="#" class="dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true"><i class="glyphicon glyphicon-tasks"></i> Gestión del Viático<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                 <li>
                                    <a href="#" onclick="window.open('http://www.me.gob.ve/servicios/recibo/consultacopia.php/')"><span class="glyphicon glyphicon-download-alt"></span> Descargar recibo de pago</a>
                                </li>
                                <li>
                                    <a href="form_soli_onli.php"><span class="glyphicon glyphicon-pencil"></span> Generar Solicitud de Viático</a>
                                </li>
                                <li>
                                    <a href="form_consul.php"><span class="glyphicon glyphicon-search"></span> Consultar Viáticos</a>
                                </li>
                                <li>
                                    <a href="form_cal_onli.php"><span class="glyphicon glyphicon-edit"></span> Generar Cálculo del Viatico</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
<!--FIN DEL MENU-->
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header text-danger">SIVIZEES Online - Solicitud de Viáticos</h3>
                </div>
            </div>
            <div class="col-lg-12">
                <!--img src="img/SIVIZE LOGO1.png" width="300px" height="300px" alt="" class="center-block"-->
                <div class="col-lg-3">
                    <div class="input-group pull-right">
                      <input type="text" onkeypress="return soloNum(event);" id="ced_aux" data-placement="bottom" data-toggle="tooltip" data-placement="bottom"
                       title="Usuario de sistema" class="form-control" placeholder='C.I. Empleado'>
                      <span class="input-group-btn">
                        <button class="btn btn-default" onclick="buscarDatosEmpleadoSoli();" type="button"><span class="glyphicon glyphicon-search"></span></button>
                      </span>
                   </div>
                </div>
                <div class="col-lg-12">&nbsp;</div>
                <div role="tabpanel">
                      <!-- Nav tabs -->
                      <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Datos del Solicitante</a></li>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Datos de la Misión</a></li>
                      </ul>

                      <!-- Tab panes -->
                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">
                            <form class="form-inline" role="form">
                                <h4 class="text-danger"><strong>Datos del Solicitante</strong></h4>
                          <div class="form-group">
                           <strong>Cédula:</strong> &nbsp; <input type="text" class="form-control" id="ced_em" placeholder="Cédula" readonly="readonly" data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom">
                          </div>
                          <div class="form-group">
                           <strong>Nombre:</strong> &nbsp; <input type="text" class="form-control" id="nom_em" placeholder="Nombre" readonly="readonly">
                          </div>
                          <div class="form-group">
                           <strong>Apellido:</strong> &nbsp; <input type="text" class="form-control" id="ape_em" placeholder="Apellido" readonly="readonly"><br><br>
                          </div>
                         <div class="form-group">
                           &nbsp; <strong>Cargo:</strong> &nbsp; <input type="text" class="form-control" id="cargo_em" placeholder="Cargo que ocupa" readonly="readonly">
                         </div>
                         <div class="form-group">
                           <strong>Estado:</strong> &nbsp;&nbsp; <input type="text" class="form-control" id="esta" placeholder="Estado" readonly="readonly">
                         </div>
                         <div class="form-group">
                           <strong>Ciudad:</strong> &nbsp;&nbsp;&nbsp; <input type="text" class="form-control" id="ciu" placeholder="Ciudad" readonly="readonly">
                         </div>
                         <div class="form-group"><br>
                           <strong>Dirección:</strong> &nbsp; <input size="100" type="text" class="form-control" id="direc" placeholder="Dirección" readonly="readonly">
                         </div>
                          <br> <strong>Sexo:</strong>
                          <label class="radio-inline">
                          <input type="radio" disabled="disabled" name="inlineRadioOptions" id="sexoM" value="option1"> M
                          <input type="hidden" id="hidempleado">
                        </label>
                        <label class="radio-inline">
                          <input type="radio" disabled="disabled" name="inlineRadioOptions" id="sexoF" value="option2"> F
                        </label>
                    </form>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="profile">
                            <h4 class="text-danger"><strong>Datos de la Misión</strong></h4>
                            <form class="form-inline">
                                <div class="form-group">
                                    <strong>Estado Origen</strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select class="form-control" id="esta_ori" onchange="traerCiudades($('#esta_ori').val(),'ciu_ori');">
                                    <option checked value="0">Seleccione</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                     <strong>Ciudad de Origen</strong> <select class="form-control" id="ciu_ori">
                                            <option checked value="0">Seleccione</option>
                                       </select>
                                </div>
                                <div class="form-group">
                                    <strong>Estado Destino</strong> <select class="form-control" id="esta_des" onchange="traerCiudades($('#esta_des').val(),'ciu_des');">
                                            <option checked value="0">Seleccione</option>
                                       </select>
                                </div><br><br>
                                <div class="form-group">
                                    <strong>Ciudad de Destino</strong>&nbsp;<select class="form-control" id="ciu_des" >
                                                <option checked value="0">Seleccione</option>
                                          </select>
                                </div><br><br>
                                <strong> Lugar de Destino</strong> <input type="text" class="form-control" size="100" id="lugardes" placeholder="Lugar de Destino"><br><br>
                                 <div class="form-group">
                                   <strong> Fecha de la misión: Desde</strong>&nbsp;
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input type='text' readonly='readonly' placeholder='AAAA/MM/DD' class="form-control hab" id="fedesde" />
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar">
                                            </span>
                                        </span>
                                    </div>
                              </div>
                              <div class="form-group">
                                    &nbsp;&nbsp;<strong>Hasta</strong>&nbsp;
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input type='text' readonly='readonly' placeholder='AAAA/MM/DD' onblur="validarFechasDesHas($('#fedesde').val(),$('#fehasta').val());" class="form-control hab" id="fehasta"/>
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar">
                                            </span>
                                        </span>
                                    </div>
                              </div><br><br>
                              <div class="form-group">
                               <strong>Motivo del Viaje</strong> <textarea name="" id="motivo" cols="50" rows="2" class="form-control" placeholder='Motivo del viaje'></textarea>
                               </div><br><br>
                               <div class="form-group">
                                    <strong>Transporte Utilizado de ida:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <label class="radio-inline">
                                      <input type="radio" name="radtrans" checked="checked" id="transa" value="option1"> <strong>Aéreo</strong>
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="radtrans" id="transt" value="option2"> <strong>Terrestre</strong>
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="radtrans" id="transo" value="option3"> <strong>Otros</strong>
                                    </label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="otro" placeholder="Otro">
                                    </div><br><br>

                                    <strong>Transporte Utilizado de Vuelta:</strong>
                                    <label class="radio-inline">
                                      <input type="radio" name="radtransvu" checked="checked" id="transavu" value="option1"> <strong>Aéreo</strong>
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="radtransvu" id="transtvu" value="option2"> <strong>Terrestre</strong>
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="radtransvu" id="transovu" value="option3"> <strong>Otros</strong>
                                    </label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="otrovu" placeholder="Otro">
                                    </div>
                                    <button type="button" onclick="registrarSolicitud();"
                                    class="btn btn-primary">Guardar<span class="glyphicon glyphicon-floppy-disk"></span></button>
                                    <div class="col-lg-12">
                                        &nbsp;
                                    </div>
                            </form>
                        </div>
                      </div>

                    </div>
            </div>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <script src="js/jquery-1.11.2.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery-ui-1.10.3.custom.js"></script>
    <script src="js/plugins/metisMenu/metisMenu.min.js"></script>
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>
    <script src="js/sb-admin-2.js"></script>
    <script src="../controladores/js/empleado.js"></script>
    <script src="../controladores/js/viatico.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    <script src="../js/sesion.js"></script>
    <script src="js/comunes.js"></script>
    <script>
    //listarCiudadesSoli();
     listarEstados();
        function mostrarModal(titulo, msg, funcion){
            $('#myModalLabel').html(titulo);
            $('#mensajeModal').html(msg);
            $('#btnAccionModal').unbind('click').click({funcion:funcion}, function(eventObject){
                eval(eventObject.data.funcion); //eval('cerrarSesion();');
            });
            $('#myModal').modal('show');
        }
        $('#usuario_aux').tooltip('show');
        $("div[id^='datetimepicker']").datepicker({
                format: 'yyyy-mm-dd',
                language: "es",
                autoclose:true
        });
        //bloquearBotonesUsu();
        //$('#ced_aux').tooltip('show');


        $("#otro").attr('disabled', true);
        $("input[name='radtrans']").click(function(){
            if ($(this).attr('id') == 'transo') {
                 $("#otro").removeAttr('disabled');
            }else{
                $("#otro").attr('disabled', true);
                $("#otro").val('');
            }
        });

        $("#otrovu").attr('disabled', true);
        $("input[name='radtransvu']").click(function(){
            if ($(this).attr('id') == 'transovu') {
                 $("#otrovu").removeAttr('disabled');
            }else{
                $("#otrovu").attr('disabled', true);
                $("#otrovu").val('');
            }
        });


        $(document).ready(function(){
            $("#sin_error_ciu").hide();
            $("#error_ciu").hide();
            $("#error_vaci").hide();
        });
</script>
</body>
</html>
