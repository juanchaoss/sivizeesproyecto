<?php
    session_start();
    if ($_SESSION[ 'usuario']) {
        header( 'location: vistas/index.php');
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Inicio de Sesión</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" href="bootstrap-3.2.0-dist/css/bootstrap.css">
    <style>
        body {
            /*background-image: url('img/bg1.jpg');*/
            background-color: #F8F8F8;
        }
    </style>
</head>
<body>
    <!--div class="container">
			<h2 class="text-center text-danger"><strong>Bienvenidos a SIVIZEES.</strong>
				<small>Sistema de Viáticos de la Zona Educativa Estado Sucre.</small></h2>
		</div-->
    <img width="100%" height="55px" src="img/bar_gob1.png">
    <div class="col-lg-12">&nbsp;</div>
    <div class="col-lg-12">&nbsp;</div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 center-block">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             <img src="img/SIVIZE LOGO.png" width="300" height="320" alt="">
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <img src="img/me.png"  width="300" height="320" alt="">
            <div class="col-lg-12">&nbsp;</div>
            <div class="col-lg-12">&nbsp;</div>
            <div class="col-lg-12">&nbsp;</div>
            <center><button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">Iniciar Sesión <span class="glyphicon glyphicon-new-window"></span></button></center>
            </div>
        </div>
    </div>
    <div class="col-md-12">&nbsp;</div>
    <div class="col-md-12">&nbsp;</div>
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title text-center text-primary" id="myModalLabel"><strong>Inicio de Sesión</strong></h4>
                </div>
                <div class="modal-body">
                    <form class="form-inline" role="form">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"><span class='glyphicon glyphicon-user'></span>
                                </div>
                                <input class="form-control" id="caja1" type="text" required placeholder="Nombre de Usuario">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"><span class='glyphicon glyphicon-lock'></span>
                                </div>
                                <input class="form-control" id="caja2" type="password" required placeholder="Contraseña" onkeypress="if (event.keyCode==13){validarCampos();}">
                            </div>
                        </div>
                    </form>
                </div>
                <!--MSJ-->
                <div class="alert alert-info alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;
		            </span><span class="sr-only">Close</span>
                    </button>
                    <strong>¡INFORMACIÓN!</strong> Introduzca su usuario y contraseña para poder acceder al sistema.
                </div>
                <!-- Fin MSJ-->

                <!--MSJ ERROR-->
                <div class="alert alert-danger alert-dismissible" id="error" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;
		  </span><span class="sr-only">Close</span>
                    </button>
                    <strong>¡ERROR!</strong> Existen campos vacios.
                </div>
                <!-- Fin MSJ ERROR-->

                <!--MSJ ERRORUSU-->
                <div class="alert alert-warning alert-dismissible" id="error-usu" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;
		  </span><span class="sr-only">Close</span>
                    </button>
                    <strong>¡ERROR!</strong> El usuario no existe o la contraseña no coincide.
                </div>
                <!-- Fin MSJ ERRORUSU-->
                <div id="prueba"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="limpiarFormModalInicio();" data-dismiss="modal">Cancelar <span class="glyphicon glyphicon-remove"></span></button>
                    <button type="button" class="btn btn-primary" onclick="validarCampos();">Iniciar Sesión <span class="glyphicon glyphicon-log-in"></span></button>
                </div>
            </div>
        </div>
    </div>
    <!-- Fin Modal -->
    <script src="js/jquery-1.11.1.js"></script>
    <script src="bootstrap-3.2.0-dist/js/bootstrap.js"></script>
    <script src="js/sesion.js"></script>
    <script>
        $(document).ready(function () {
            $("#error").hide();
            $("#error-usu").hide();
        });
    </script>
</body>
</html>