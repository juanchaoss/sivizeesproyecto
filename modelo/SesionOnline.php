<?php
include_once("../clases/Fachada.php");

	class inicioSesionOnline{

		public function iniciarSesionOnline($ced, $fecha){
			$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$cedula    = addslashes($ced);
			$fecha_na  = addslashes($fecha);

			$sql       = "SELECT * FROM persona WHERE cedula_per = '$cedula' AND fecha_na_per = '$fecha_na' AND eliminado = 0";			
			$resultado = $bd->consultar($sql, 'ARREGLO');

			if ($resultado){
				session_start();

				$name     = explode(" ", $resultado[0]['nombre_per']);
				$lastname = explode(" ", $resultado[0]['apellido_per']);

				$realname = $name[0].' '.$lastname[0];
				$_SESSION['nombre']    = $resultado[0]['nombre_per'];
				$_SESSION['apellido']  = $resultado[0]['apellido_per'];
				$_SESSION['ced']       = $resultado[0]['cedula_per'];
				$_SESSION['fecha']     = $resultado[0]['fecha_na_per'];
				$_SESSION['usuarioOnline'] = $realname;
				$_SESSION['realname']  = $realname;
				$_SESSION['permitido_online'] = TRUE;
			}
			sleep(2);
			return $resultado;
		}

		public function cerrarSesionOnline(){
			session_start();
			session_destroy();
			session_unset();
			return true;
	    }
	}
?>