<?php
	include_once("clases/Fachada.php");
	require 'clases/PHPMailer/PHPMailerAutoload.php';

	class Viatico{
		public function registrarSolicitud($lugardes,$fedesde,$fehasta,$motivo,$otro,$otrovu,$hidempleado,$trans,$transvu, $ciu_des, $ciu_ori){
			$sql[]="INSERT INTO solicitud_via (lugardes_soli, fechades_soli, fechahas_soli, motivovia_soli, tipotrans_soli,tipotransvu_soli, estado_soli, id_em, otro, otrovu,num_soli, id_ciu_ori, id_ciu_des)
				VALUES('".$lugardes."', '".$fedesde."', '".$fehasta."', '".$motivo."',".$trans.",".$transvu.",0,".$hidempleado.",'".$otro."','".$otrovu."',(SELECT MAX(num_soli)+1 FROM solicitud_via), ".$ciu_ori.",".$ciu_des.")";

			$bd = new Datos(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);
			$bd->conectar();
			$resultado = $bd->consultasMultiples($sql);
			if ($resultado){
				$bd = new Fachada();
				$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);
				$sql = "SELECT CURRVAL('solicitud_via_id_soli_seq') AS id_soli";
				$resultado = $bd->consultar($sql, 'ARREGLO');
				$xy = $resultado[0]['id_soli'];
				return $xy;
			}
			return $resultado;
		}

		public function listarSolicitudes(){
			$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$sql = "SELECT * FROM solicitud_via ORDER BY num_soli";
			$resultado = $bd->consultar($sql, 'ARREGLO');
			return $resultado;
	    }

	    public function listatSolicitudesMesAnio($mes, $anio){
			$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$sql = "SELECT S.id_soli, S.estado_soli, S.num_soli, S.motivovia_soli, S.lugardes_soli, 
			S.fechades_soli, S.fechahas_soli, P.cedula_per, P.nombre_per, P.apellido_per, MAX(ES.nivel) AS nivel
			FROM solicitud_via AS S JOIN empleado AS E ON (S.id_em = E.id_em) 
			JOIN persona AS P ON (E.id_per = P.id_per) LEFT JOIN emp_soli AS ES ON (ES.id_soli = S.id_soli)
			WHERE text(S.fechades_soli) like '$anio-$mes-%' 
			GROUP BY S.id_soli, P.cedula_per, P.nombre_per
			ORDER BY num_soli";

			$resultado = $bd->consultar($sql, 'ARREGLO');
			return $resultado;
	    }

	    public function listatSolicitudesNivelMesAnio($mes, $anio, $gen){
			$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			if ($_SESSION['nivel']<6){
				$status="(".($_SESSION['nivel']-2).")";
				if ($_SESSION['nivel']==2){
					$status="(0,4)";
				}
			}else{
				$status="(5)";
			}

			$AND="AND S.estado_soli=0";
			if ($gen==1){
				$AND="AND S.estado_soli=2";
			}

			$sql = "SELECT S.id_soli, S.estado_soli, S.num_soli, S.motivovia_soli, S.lugardes_soli, 
			S.fechades_soli, S.fechahas_soli, S.in_status, P.cedula_per, P.nombre_per, P.apellido_per
			FROM solicitud_via AS S JOIN empleado AS E ON (S.id_em = E.id_em) 
			JOIN persona AS P ON (E.id_per = P.id_per)
			WHERE text(S.fechades_soli) like '$anio-$mes-%' ".$AND." AND S.in_status IN ".$status."
			ORDER BY num_soli";

			$resultado = $bd->consultar($sql, 'ARREGLO');
			return $resultado;
	    }

	    public function listatSolicitudesGeneradas($mes, $anio){
			$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$sql = "SELECT /*DISTINCT*/ S.id_soli, S.estado_soli, S.num_soli, S.motivovia_soli, S.lugardes_soli, 
			S.fechades_soli, S.fechahas_soli, S.in_status, P.cedula_per, P.nombre_per, P.apellido_per
			FROM solicitud_via AS S JOIN empleado AS E ON (S.id_em = E.id_em) 
			JOIN persona AS P ON (E.id_per = P.id_per) /*JOIN gasto AS G ON (G.id_soli = S.id_soli)*/
			WHERE text(S.fechades_soli) like '$anio-$mes-%' AND S.estado_soli = 3
			ORDER BY num_soli";

			$resultado = $bd->consultar($sql, 'ARREGLO');
			return $resultado;
	    }

	   public function registrarPrecios($via,$monto,$fecha,$montomax){
	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$arreglo = array('id_via'   => $via,
							 'monto_pre'=>$monto,
							 'fecha_pre'=>$fecha,
							 'monto_max'=>$montomax);
			$resultado = $bd->insertar('precio_viatico',$arreglo);
			return $resultado;
	    }

	    public function cargarViatico(){
	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);
			$sql = "SELECT * FROM viatico";
			$resultado = $bd->consultar($sql, 'ARREGLO');
			return $resultado;
	    }

	    public function validarPrecioViatico($via,$fecha){
	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);
			$error = 0;
			$sql = "SELECT * FROM precio_viatico WHERE id_via = $via AND fecha_pre = '$fecha'";
			$resultado = $bd->consultar($sql, 'N_FILAS');

			if ($resultado > 0) {
				$error = 1;
			}

			/*$sql = "SELECT * FROM solicitud_via WHERE fechades_soli > '$fecha' AND  estado_soli = '2'";
			$resultado = $bd->consultar($sql, 'N_FILAS');

			if ($resultado > 0) {
				$error = 2;
			}*/

			$sql = "SELECT * FROM solicitud_via WHERE fechades_soli >= '$fecha' AND estado_soli = '2'";
			$resultado = $bd->consultar($sql, 'N_FILAS');

			if ($resultado > 0) {
				$error = 3;
			}
			return $error;
	    }

	    public function buscarSolicitud($nSoli){
	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$sql = "SELECT A.*, B.*, C.*, G.id_es AS essoliori, 
			I.id_es AS essolides, 
			E.nombre_ciu, D.nombre_es 
			FROM solicitud_via AS A JOIN empleado AS B ON (A.id_em = B.id_em)
			JOIN persona AS C ON (C.id_per = B.id_per) JOIN estado AS D ON (C.id_es = D.id_es) 
			JOIN ciudad AS E ON (C.id_ciu = E.id_ciu) 
			JOIN ciudad AS F ON (A.id_ciu_ori = F.id_ciu)
			JOIN estado AS G ON (F.id_es = G.id_es)
			JOIN ciudad AS H ON (A.id_ciu_des = H.id_ciu)
			JOIN estado AS I ON (H.id_es = I.id_es)
			WHERE A.num_soli = $nSoli";
			$resultado = $bd->consultar($sql, 'ARREGLO');
			if ($resultado){
				if ($resultado[0]['estado_soli']==0)
					return "E001";
				else if ($resultado[0]['estado_soli']==2)
					return "E002";
				else if ($resultado[0]['estado_soli']==3)
					return "E003";
				else if ($resultado[0]['estado_soli']==4)
					return "E004";
			}
			return $resultado;
	    }

	    public function buscarPrecios($fecha){
	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$sql = "SELECT * FROM viatico";
			$resultado = $bd->consultar($sql, 'ARREGLO');

			if ($resultado) {
				for($i = 0; $i<count($resultado); $i++){
					$id_via = $resultado[$i]['id_via'];
					$sqlv = "SELECT V.*, P.monto_pre, P.monto_max,P.fecha_pre FROM viatico AS V JOIN precio_viatico
							AS P ON (P.id_via = V.id_via) WHERE P.fecha_pre = (select MAX(R.fecha_pre)
						FROM precio_viatico AS R JOIN viatico AS Q ON (R.id_via = Q.id_via)
						WHERE R.fecha_pre < '$fecha' AND Q.id_via=$id_via) AND V.id_via=$id_via";
					$res = $bd->consultar($sqlv, 'ARREGLO');
					$precios[] = array('id_via'    => $res[0]['id_via'],
									   'monto_pre' => $res[0]['monto_pre'],
									   'monto_max' => $res[0]['monto_max']);
				}
				return $precios;
			}
			return $resultado;
	    }

	    public function listarViaticos(){
	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$sql = "SELECT * FROM solicitud_via WHERE estado_soli = 1";
			$resultado = $bd->consultar($sql, 'ARREGLO');
			return $resultado;
			return 1;
	    }

	    public function guardarGastos($ids, $montos, $cants, $totals, $idSoli, $idProyecto){
	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$array_ids     = explode(",", $ids);
			//$array_precios = explode(",", $precios);
			$array_montos   = explode(",", $montos);
			$array_cants   = explode(",", $cants);
			$array_totals  = explode(",", $totals);
			$total=0;
			for ($i=0; $i < count($array_ids); $i++) {

				$sql[]="INSERT INTO gasto (id_soli, id_pro, id_via, precio, lapso, total)
				VALUES($idSoli, $idProyecto, ".$array_ids[$i].",".$array_montos[$i].",".$array_cants[$i].",".$array_totals[$i].")";
				$total+=$array_totals[$i];
			}

			$sql[]="UPDATE solicitud_via SET estado_soli=2, monto_soli = ".$total." WHERE id_soli=".$idSoli;

			$bd = new Datos(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);
			$bd->conectar();
			$resultado = $bd->consultasMultiples($sql);
			return $resultado;
	    }

	    public function aprobarViaticos($ids,$status){
	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$ids=explode(',', $ids);
			$status=explode(',', $status);

			for ($i=0; $i < count($ids); $i++) {
				if ($_SESSION['nivel']==2){
					$nstatus=5;
					$nstatus_aux=5;
					if ($status[$i]==0){
						$nstatus=1;
					}
				}else{
					$nstatus=$_SESSION['nivel']-1;
					if ($_SESSION['nivel']==6){
						$nstatus=6;
					}
				}

				$sql[]="INSERT INTO emp_soli (id_em, id_soli, nivel) VALUES(".$_SESSION['empleado'].", ".$ids[$i].",".$_SESSION['nivel'].")";
				$sql[]="UPDATE solicitud_via SET in_status=".$nstatus." WHERE id_soli=".$ids[$i];

				//SI LA ESTA APROBANDO UN NIVEL 3 (DIRECTOR DE ZONA), LA SOLICITUD CAMBIA A 1:"aprobada, no generada"
				if ($_SESSION['nivel']==3){
					$sql[]="UPDATE solicitud_via SET estado_soli=1 WHERE id_soli=".$ids[$i];
					//enviarCorreo($ids[$i],1);
				}

				//SI LA ESTA APROBANDO UN NIVEL 6 (CONTRALORIA INTERNA), LA SOLICITUD CAMBIA A 3:"generada aprobada"
				if ($_SESSION['nivel']==6){
					$sql[]="UPDATE solicitud_via SET estado_soli=3 WHERE id_soli=".$ids[$i];
					//enviarCorreo($ids[$i],3);
				}

			}
			//var_dump($sql);

			$bd = new Datos(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);
			$bd->conectar();
			$resultado = $bd->consultasMultiples($sql);

			return $resultado;
		}

		 public function cancelarViaticos($ids){
	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$ids=explode(',', $ids);

			for ($i=0; $i < count($ids); $i++) {

				$sql[]="INSERT INTO emp_soli (id_em, id_soli, nivel) VALUES(".$_SESSION['empleado'].", ".$ids[$i].",".$_SESSION['nivel'].")";
				$sql[]="UPDATE solicitud_via SET in_status=7 WHERE id_soli=".$ids[$i];

				$sql[]="UPDATE solicitud_via SET estado_soli=5 WHERE id_soli=".$ids[$i];
			}

			$bd = new Datos(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);
			$bd->conectar();
			$resultado = $bd->consultasMultiples($sql);

			return $resultado;
		}

		public function cargarCiudad($ciudad){
			$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$arreglo = array('nombre_ciu_soli' 	=> $ciudad);
			$resultado = $bd->insertar('ciudad_soli',$arreglo);
			return $resultado;
	    }

	    public function listarCiudadesSoli(){
	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$sql = "SELECT id_ciu_soli, nombre_ciu_soli FROM ciudad_soli WHERE status_ciu = 0 ORDER BY nombre_ciu_soli";
			$resultado = $bd->consultar($sql, 'ARREGLO');
			return $resultado;
	    }

	    public function listarEstados(){
	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$sql = "SELECT * FROM estado";
			$resultado = $bd->consultar($sql, 'ARREGLO');
			return $resultado;
	    }

	    public function listarCiudades($estado,$activa){
	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);
			$and = '';
			if ($activa == 1) {
				$and = 'AND status_ciu = 0';
			}
			$sql = "SELECT * FROM ciudad WHERE id_es = $estado ".$and." ORDER BY nombre_ciu";
			$resultado = $bd->consultar($sql, 'ARREGLO');
			return $resultado;
	    }

	    public function pagoDeViaticos($id_soli, $referencia, $modo, $fecha,$banco){
	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$sql1 = "SELECT DISTINCT P.id_partida, S.monto_soli FROM solicitud_via AS S JOIN gasto AS G ON (G.id_soli = S.id_soli)
			 JOIN proyecto AS P ON (G.id_pro = P.id_pro) AND S.id_soli = ".$id_soli;
			$res = $bd->consultar($sql1, 'ARREGLO');

			$id_partida = $res[0]['id_partida'];
			$monto      = $res[0]['monto_soli']*(-1);
			$descrip    = 'Pago de Viáticos';
			//$fecha      = date('Y-m-d');

			$sql[]="INSERT INTO movimiento (descripcion_movi,monto_movi,id_partida,fecha_movi,modo_movi,num_referencia,id_banco)
				VALUES('".$descrip."', ".$monto.", ".$id_partida.",'".$fecha."',".$modo.",'".$referencia."',".$banco.")";
			$sql[]="UPDATE solicitud_via SET estado_soli=4 WHERE id_soli=".$id_soli;

			$bd = new Datos(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);
			$bd->conectar();
			$resultado = $bd->consultasMultiples($sql);

			return true;
	    }

	    public function desactivarCiudades($ids){

	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$ids=explode(',', $ids);

			for ($i=0; $i < count($ids); $i++){
				$sql[]="UPDATE ciudad SET status_ciu=1 WHERE id_ciu=".$ids[$i];
			}

			$bd = new Datos(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);
			$bd->conectar();
			$resultado = $bd->consultasMultiples($sql);

			return $resultado;
	    }

	    public function activarCiudades($ids){

	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$ids=explode(',', $ids);

			for ($i=0; $i < count($ids); $i++){     
				$sql[]="UPDATE ciudad SET status_ciu=0 WHERE id_ciu=".$ids[$i];
			}

			$bd = new Datos(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);
			$bd->conectar();
			$resultado = $bd->consultasMultiples($sql);

			return $resultado;
	    }

	    public function enviarCorreos($ids,$opc){

	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$ids=explode(',', $ids);
			$cent = 0;
			for ($i=0; $i < count($ids); $i++){
				 $this->enviarCorreo($ids[$i],$opc);
			}

			return true;
	    }

		public function enviarCorreo($id,$opc){

	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			//hacer consulta para traer los datos de la solicitud y del empleado que hace la      solicitud

			$sql = "SELECT * FROM solicitud_via AS A JOIN empleado AS B ON (A.id_em = B.id_em)
			JOIN persona AS C ON (C.id_per = B.id_per) JOIN estado AS D ON (C.id_es = D.id_es) JOIN
			ciudad AS E ON (C.id_ciu = E.id_ciu) WHERE A.id_soli = $id";

			$resultado = $bd->consultar($sql, 'ARREGLO');
			//return $resultado;

			$num_soli= $resultado[0]['num_soli'];

			if ($opc == 0) {
				$asunto="Solicitud recibida";
				$mensaje="La solicitud de viáticos número $num_soli fue registrada exitosamente en el sistema. Espere un próximo correo para conocer el status de la solicitud de viáticos.";
			}elseif ($opc == 1) {
				$asunto="Solicitud aprobada, no generada";
				$mensaje="La solicitud de viáticos número $num_soli fue registrada exitosamente en el sistema. Espere un próximo correo para conocer el status de la solicitud de viáticos.";
			}elseif ($opc == 3) {
				$asunto="Solicitud generada, aprobada";
				$mensaje="La solicitud de viáticos número $num_soli fue registrada exitosamente en el sistema. Espere un próximo correo para conocer el status de la solicitud de viáticos.";
			}
			elseif ($opc == 4) {
				$asunto="Solicitud Pagada";
				$mensaje="La solicitud de viáticos número $num_soli Ha sido Pagada. Diríjase a la oficina de la División de Administración y Servicios para mayor información.";
			}
			elseif ($opc == 5) {
				$asunto="Solicitud Cancelada";
				$mensaje="La solicitud de viáticos número $num_soli fue cancelada. Diríjase a la oficina de la División de Administración y Servicios para mayor información.";
			}

			$mail = new PHPMailer;
			//$mail->SMTPDebug = 3;                               // Enable verbose debug output
			$email_aux = $resultado[0]['correo_per'];
			$mail->isSMTP();                                      // Set mailer to use SMTP
			$mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
			$mail->SMTPAuth = true;                               // Enable SMTP authentication
			$mail->Username = 'sivizees.zona.edu@gmail.com';                 // SMTP username
			$mail->Password = 'sivizees2015';                           // SMTP password
			$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
			$mail->Port = 587;                                    // TCP port to connect to

			$mail->From = 'sivizees.zona.edu@gmail.com';
			$mail->FromName = 'SIVIZEES';
			$mail->addAddress($email_aux);               // Name is optional
			$mail->isHTML(true);                                  // Set email format to HTML
			//if($status==1){
			$id_con = $resultado[0]['id_soli'];

			$mail->Subject = $asunto;     
			$mail->Body    = $mensaje;

			if(!$mail->send()) {
			    return false;
			} else {
			    return true;
			}

	    }

	    public function busquedaAvanzasaVia($mes,$anio,$filtro,$cedula_em,$fedesde,$fehasta,$esta_ori,$ciu_ori,$esta_des,$ciu_des){
	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);
			//$condicion;
			$AND="";
			if ($filtro!="-") {
				$AND.=" AND A.estado_soli = ".$filtro;
			}

			if ($cedula_em!="") {
				$AND.=" AND C.cedula_per = '".$cedula_em."'";
			}

			if ($fedesde!="") {
				$AND.=" AND A.fechades_soli = '".$fedesde."'";
			}

			if ($fehasta!="") {
				$AND.=" AND A.fechahas_soli = '".$fehasta."'";
			}

			if ($esta_ori!=0) {
				$AND.=" AND D.id_es = '".$esta_ori."'";
			}

			if ($ciu_ori!=0) {
				$AND.=" AND E.id_ciu = '".$ciu_ori."'";
			}

			if ($esta_des!=0) {
				$AND.=" AND D.id_es = '".$esta_des."'";
			}

			if ($ciu_des!=0) {
				$AND.=" AND E.id_ciu = '".$ciu_des."'";
			}


			$sql = "SELECT * FROM solicitud_via AS A JOIN empleado AS B ON (A.id_em = B.id_em)
			JOIN persona AS C ON (C.id_per = B.id_per) JOIN estado AS D ON (C.id_es = D.id_es) JOIN
			ciudad AS E ON (C.id_ciu = E.id_ciu) WHERE text(A.fe_creado) like '$anio-$mes-%' ".$AND."
			ORDER BY A.num_soli";
			$resultado = $bd->consultar($sql, 'ARREGLO');
			return $resultado;
	    }

	     public function busquedaZona($filtro,$fedesde,$fehasta,$esta_des,$ciu_des, $cons){
	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);
			//$condicion;
			$AND="";
			if ($filtro!="-") {
				$AND.=" AND S.estado_soli = ".$filtro;
			}

			if ($fedesde!="") {
				$AND.=" AND CAST(S.fechades_soli AS DATE) >= '".$fedesde."'";
			}

			if ($fehasta!="") {
				$AND.=" AND CAST(S.fechahas_soli AS DATE) <= '".$fehasta."'";
			}

			if ($esta_des!=0) {
				$AND.=" AND ED.id_es = '".$esta_des."'";
			}

			$GO = "";
			$SELECT = "";


			if ($cons==0){//no es consolidado
				$SELECT=",CD.*";
				$GO=",CD.id_ciu";

				if ($ciu_des!=0) {
					$AND.=" AND CD.id_ciu = '".$ciu_des."'";
				}
			}


			$sql = "SELECT COUNT(S.id_soli) AS cont,
					(
						SELECT COUNT(S.id_soli) AS cont
						FROM solicitud_via AS S JOIN empleado AS E ON (S.id_em = E.id_em)
									JOIN persona AS P ON (P.id_per = E.id_per) 
									JOIN ciudad AS CD ON (S.id_ciu_des = CD.id_ciu)
									RIGHT JOIN estado AS ED ON (CD.id_es = ED.id_es)
									WHERE 1=1 $AND
					) AS total, ED.id_es AS id_edo, ED.nombre_es AS nombre_edo $SELECT
					FROM solicitud_via AS S JOIN empleado AS E ON (S.id_em = E.id_em)
								JOIN persona AS P ON (P.id_per = E.id_per) 
								JOIN ciudad AS CD ON (S.id_ciu_des = CD.id_ciu)
								RIGHT JOIN estado AS ED ON (CD.id_es = ED.id_es)
								WHERE 1=1 $AND
								GROUP BY ED.id_es $GO
								ORDER BY ED.id_es $GO";

			$resultado = $bd->consultar($sql, 'ARREGLO');
			return $resultado;
	    }

	    public function historialDeViaticos($tipo,$cedu,$cod){
	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			if ($tipo == 1) {

				$sql = "SELECT * FROM solicitud_via AS s JOIN empleado AS E ON (E.id_em = S.id_em) 
					JOIN persona AS P ON (E.id_per = P.id_per)
                     WHERE P.cedula_per = '$cedu' ORDER BY fe_creado";

            }elseif ($tipo == 2) {

            	$sql = "SELECT * FROM solicitud_via AS S JOIN empleado AS E ON (E.id_em = S.id_em) 
					JOIN persona AS p ON (E.id_per = P.id_per)
                     WHERE S.num_soli = '$cod' AND P.cedula_per = '$cedu' ORDER BY fe_creado";
            }

            $resultado = $bd->consultar($sql, 'ARREGLO');
			return $resultado;
	    }

	    public function listarSoliStatus($filtro,$fedesde,$fehasta){

	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$and = '';

			if ($filtro != '-') {
				$and = "AND estado_soli = $filtro";
			}

			if ($fedesde != '') {
				$and = "AND fechades_soli >= '$fedesde'";
			}

			if ($fehasta != '') {
				$and = "AND fechahas_soli <= '$$fehasta'";
			}


			$sql = "SELECT * FROM solicitud_via AS S JOIN empleado AS E ON (E.id_em = S.id_em)
					JOIN persona AS P ON (P.id_per = E.id_per)
						WHERE 1=1 $and ORDER BY fe_creado DESC";

			$resultado = $bd->consultar($sql, 'ARREGLO');
			return $resultado;
	    }

	    public function ConsolidadoSoliPerso($filtro,$fedesde,$fehasta){
	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$and = '';

			if ($filtro != '-') {
				$and = "AND estado_soli = $filtro";
			}

			if ($fedesde != '') {
				$and = "AND fechades_soli >= '$fedesde'";
			}

			if ($fehasta != '') {
				$and = "AND fechahas_soli <= '$$fehasta'";
			}

			$sql = "SELECT P.nombre_per, P.apellido_per,P.cedula_per, COUNT (S.id_soli) AS cont 
					FROM solicitud_via AS S JOIN empleado AS E ON (E.id_em = S.id_em) 
					join persona as P on (E.id_per = P.id_per) where 1=1 $and
					/*fechades_soli >= '2014-10-16' AND fechahas_soli <= '2015-03-11' AND estado_soli = 0*/
					GROUP BY P.id_per ORDER BY P.id_per";

		    $resultado = $bd->consultar($sql, 'ARREGLO');
			return $resultado;
	    }

	    public function consolidadoSoliStatus($filtro,$fedesde,$fehasta){
	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$and = '';

			if ($filtro != '-') {
				$and = "AND estado_soli = $filtro";
			}

			if ($fedesde != '') {
				$and = "AND fechades_soli >= '$fedesde'";
			}

			if ($fehasta != '') {
				$and = "AND fechahas_soli <= '$$fehasta'";
			}

			$sql = "SELECT COUNT(id_soli) AS cont, estado_soli FROM solicitud_via WHERE 1=1 $and --estado_soli = 0
					/*and fechades_soli >= '2014-10-16' AND fechahas_soli <= '2015-03-12' */
					GROUP BY estado_soli order BY estado_soli";

		    $resultado = $bd->consultar($sql, 'ARREGLO');
			return $resultado;
	    }

	    public function buscarStatusSoli($soli){
	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$sql = "SELECT S.in_status, S.estado_soli, S.num_soli FROM solicitud_via AS S WHERE
					num_soli = $soli";
		    $resultado = $bd->consultar($sql, 'ARREGLO');
			return $resultado;
	    }
	}
?>
