<?php
	include_once("clases/Fachada.php");

	class Empleado{
		public function cargarEstados(){
			$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);
			$sql = "SELECT * FROM estado";
			$resultado = $bd->consultar($sql, 'ARREGLO');
			return $resultado;
		}

		public function cargarMunicipios($id_es){
			$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);
			$sql = "SELECT * FROM municipio WHERE id_es=".$id_es;
			$resultado = $bd->consultar($sql, 'ARREGLO');
			return $resultado;
		}

		public function cargarParroquias($id_mun){
			$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);
			$sql = "SELECT * FROM parroquia WHERE id_mun=".$id_mun;
			$resultado = $bd->consultar($sql, 'ARREGLO');
			return $resultado;
		}

		public function cargarCiudades($id_es){
			$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);
			$sql = "SELECT * FROM ciudad WHERE id_es=".$id_es;
			$resultado = $bd->consultar($sql, 'ARREGLO');
			return $resultado;
		}
		public function registrarEmpleado($cedula, $nombres,$apellidos, $fecha_na,$correo, $direccion, $celular,
				$estado, $sexo, $numero, $municipio,$parroquia, $ciudad,$fecha_in,$condicion, $tipo_em,$cargo){
			$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$sql[]="INSERT INTO persona (cedula_per,nombre_per,apellido_per,fecha_na_per,correo_per,direccion_per, celular_per,
				id_es,sexo_per,telefono_per,id_mun,id_parro,id_ciu) VALUES('$cedula', '$nombres','$apellidos', '$fecha_na','$correo', '$direccion', '$celular',
				$estado,'$sexo',$numero,$municipio,$parroquia, $ciudad)";

			$sql[]="INSERT INTO empleado (fecha_ingre,condi_lab, tipo, cargo, id_per)
				VALUES('$fecha_in', $condicion,$tipo_em,'$cargo',(SELECT last_value FROM persona_id_per_seq))";

			$bd = new Datos(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);
			$bd->conectar();
			$resultado = $bd->consultasMultiples($sql);
			return $resultado;

			/*$arreglo = array('cedula_per'  => $cedula,
							'nombre_per'   => $nombres,
							'apellido_per' => $apellidos,
							'fecha_na_per' => $fecha_na,
							'correo_per'   => $correo,
							'direccion_per'=> $direccion,
							'id_es' 	   => $estado,
							'id_mun'       => $municipio,
							'id_parro'     => $parroquia,
							'id_ciu'       => $ciudad
							);

			$resultado = $bd->insertar('persona',$arreglo);

			if($resultado){

				$sql_ci = "SELECT id_per FROM persona WHERE cedula_per = '$cedula' AND eliminado=0";
				$res_ci = $bd->consultar($sql_ci, 'ARREGLO');
				$arreglo2 = array('fecha_ingre' => $fecha_in,
								'condi_lab'     =>$condicion,
								'tipo'          =>$tipo_em,
								'cargo'         => $cargo,
								'id_per'    	=>$res_ci[0]['id_per']
								);
				$resultado = $bd->insertar('empleado',$arreglo2);
			}

			return $resultado;*/
		}

		public function verificarEmpleado($cedula){
			$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);
			$sql = "SELECT id_per FROM persona WHERE cedula_per = '$cedula' AND eliminado=0";
			//var_dump($sql);
			$resultado = $bd->consultar($sql, 'ARREGLO');
			if($resultado){
			  $resultado=false;
			}else{
				$resultado=true;
			}
			return $resultado;
		}
		public function buscarEmpleado($cedula){
			$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$sql = "SELECT * FROM empleado AS E JOIN persona AS P ON (E.id_per = P.id_per)
			JOIN estado AS D ON (P.id_es = D.id_es) JOIN ciudad AS C ON (P.id_ciu = C.id_ciu)
					WHERE P.cedula_per = '$cedula' AND P.eliminado = 0 AND E.eliminado = 0";
			$resultado = $bd->consultar($sql, 'ARREGLO');
			return $resultado;
		}

		public function actualizarDatosEmpleado($cedula, $nombres,$apellidos, $fecha_na,$correo, $direccion, $celular,
				$estado, $sexo, $numero, $municipio,$parroquia, $ciudad,$fecha_in,$condicion, $tipo_em,$cargo, $hidempleado, $hidpersona){
			$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$sql[]="UPDATE persona SET cedula_per='$cedula', nombre_per='$nombres', apellido_per='$apellidos',
					fecha_na_per='$fecha_na', correo_per='$correo', direccion_per='$direccion', celular_per='$celular',
					id_es=$estado, sexo_per='$sexo', telefono_per='$numero', id_mun=$municipio, id_parro=$parroquia, id_ciu=$ciudad WHERE id_per=$hidpersona";

			$sql[]="UPDATE empleado SET fecha_ingre='$fecha_in', condi_lab=$condicion, tipo=$tipo_em, cargo='$cargo'
					WHERE id_em=$hidempleado";
			$bd = new Datos(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);
			$bd->conectar();
			$resultado = $bd->consultasMultiples($sql);

			return $resultado;
		}

		public function eliminarDatosEmpleado($hidempleado, $hidpersona){
			$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$sql[]="UPDATE persona SET eliminado=1 WHERE id_per=$hidpersona";

			$sql[]="UPDATE empleado SET eliminado=1 WHERE id_em=$hidempleado";
			$bd = new Datos(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);
			$bd->conectar();
			$resultado = $bd->consultasMultiples($sql);
			return $resultado;
		}
	}
?>