<?php
	include_once("clases/Fachada.php");

	class Presupuesto{
		public function cargarPartida($codigo,$descripcion){
			$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$arreglo = array('cuenta_par' 	    => $codigo,
							 'descripcion_par' 	=> $descripcion
							);
			$resultado = $bd->insertar('partida',$arreglo);
			//sleep(2);
			return $resultado;
	    }
	    public function cargarParti($except){
	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$where="";
			if($except && $except!=null){
				$where="WHERE P.id_partida!=".$except;
			}

			$sql = "SELECT P.id_partida,P.cuenta_par, P.descripcion_par, CASE WHEN sum(M.monto_movi) IS 
					NULL THEN 0 ELSE sum(M.monto_movi) END 
					AS dispon FROM partida AS P LEFT JOIN movimiento As M ON 
					(M.id_partida = P.id_partida) ".$where." GROUP BY P.id_partida";
			$resultado = $bd->consultar($sql, 'ARREGLO');
			return $resultado;
	    }
	    public function buscarCuentasProyecto($id_pro){
	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);
			$sql = "SELECT * FROM proyecto AS A JOIN partida AS B ON (A.id_partida = B.id_partida AND A.id_pro = $id_pro)";
			$resultado = $bd->consultar($sql, 'ARREGLO');
			return $resultado;
	    }
	    public function traerCuenta($id_partida){
	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);
			$sql = "SELECT * FROM partida WHERE id_partida=".$id_partida;
			$resultado = $bd->consultar($sql, 'ARREGLO');
			return $resultado;
	    }

	    public function registrarProyecto($impu,$descrip){
			$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$arreglo = array('id_partida' 	=> $impu,
							 'nombre_pro' 	=> $descrip
							);
			$resultado = $bd->insertar('proyecto',$arreglo);
			return $resultado;
	    }

	    public function guardarMovimiento($impu,$descrip, $num_refe,$monto,$fecha,$modo,$traspa,$banco){
			$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			if ($traspa == 0) {
				$traspa = 'default';
				//registramos el movimiento POSITIVO a la partida seleccionada
				$sql[]="INSERT INTO movimiento (id_partida, descripcion_movi, monto_movi, fecha_movi ,modo_movi, num_referencia, id_traspa, id_banco) 
				VALUES($impu, '".pg_escape_string($descrip)."',$monto, '$fecha',$modo, '$num_refe', $traspa, $banco)";
			}else{
				$modo = 3;
				$banco = 'default';
				$num_refe = '';
				//registramos el movimiento NEGATIVO a la partida seleccionada (desde)
				$sql[]="INSERT INTO movimiento (id_partida, descripcion_movi, monto_movi, fecha_movi ,modo_movi, num_referencia, id_traspa, id_banco) 
				VALUES($impu, '".pg_escape_string($descrip)."',(-1)*$monto, '$fecha',$modo, '$num_refe', $traspa, $banco)";
				//registramos el movimiento POSITIVO a la partida seleccionada (hacia)
				$sql[]="INSERT INTO movimiento (id_partida, descripcion_movi, monto_movi, fecha_movi ,modo_movi, num_referencia, id_traspa, id_banco) 
				VALUES($traspa, '".pg_escape_string($descrip)."',$monto, '$fecha',$modo, '$num_refe', $impu, $banco)";
			}

			$bd = new Datos(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);
			$bd->conectar();
			$resultado = $bd->consultasMultiples($sql);
			return $resultado;
	    }

	    public function buscarProyectos(){
	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$sql = "SELECT * FROM proyecto";
			$resultado = $bd->consultar($sql, 'ARREGLO');
			return $resultado;
	    }

	    public function listarMovimientos(){
	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$sql = "SELECT * FROM movimiento AS M JOIN partida AS P ON (M.id_partida=P.id_partida) 
			ORDER BY M.fecha_movi, M.id_movi";
			$resultado = $bd->consultar($sql, 'ARREGLO');
			return $resultado;
	    }

	    public function listarBanco(){
	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$sql = "SELECT * FROM banco";
			$resultado = $bd->consultar($sql, 'ARREGLO');
			return $resultado;
	    }

	    public function filtrarSumaPartida($mes,$anio,$partida){
	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$id_parti = explode('&@', $partida);

			$sql = "SELECT * FROM movimiento AS M JOIN partida AS P ON (M.id_partida=P.id_partida)
			WHERE P.id_partida = '$id_parti[0]' AND text(M.fecha_movi) like '$anio-$mes-%' ORDER BY M.fecha_movi, M.id_movi";

			$resultado = $bd->consultar($sql, 'ARREGLO');
			return $resultado;
	    }

	    public function listarProyectos(){
	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);
			$sql = "SELECT * FROM proyecto as A JOIN partida AS B ON (A.id_partida = B.id_partida)";
			$resultado = $bd->consultar($sql, 'ARREGLO');
			return $resultado;
	    }

	     public function buscarPrecioFecha($mes,$anio){
	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);
			$sql = "SELECT * FROM precio_viatico AS P JOIN viatico AS V ON (P.id_via = V.id_via) 
			WHERE text(P.fecha_pre) like '$anio-$mes-%' ORDER BY P.fecha_pre";
			$resultado = $bd->consultar($sql, 'ARREGLO');
			return $resultado;
	    }

	    public function  listarDispon($partida){
	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$where='';

			if ($partida != 0) {
				$where = "WHERE P.id_partida = $partida";
			}

			$sql = "SELECT P.cuenta_par, P.descripcion_par, CASE WHEN SUM(M.monto_movi) 
					IS NOT NULL THEN SUM(M.monto_movi) ELSE 0 END AS disponible 
          			FROM partida AS P LEFT JOIN movimiento AS M ON (P.id_partida = M.id_partida) 
					$where
					GROUP BY P.id_partida
					ORDER BY P.cuenta_par";

			$resultado = $bd->consultar($sql, 'ARREGLO');
			return $resultado;
	    }

	    public function  listarMovimientoPartida($partida,$fechades,$fechahas){
	    	$bd = new Fachada();
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$where='';

			if ($fechades != '') {
				$fechades = "AND fecha_movi >= '$fechades'";
			}

			if ($fechahas != '') {
				$fechahas = "AND fecha_movi <= '$fechahas'";
			}

			$sql = "SELECT * FROM movimiento AS M join partida AS P ON (P.id_partida = M.id_partida) 
					WHERE M.id_partida = $partida $fechades $fechahas ORDER BY fecha_movi";

			$resultado = $bd->consultar($sql, 'ARREGLO');
			return $resultado;
	    }
	}
 ?>