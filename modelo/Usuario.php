<?php
	include_once("clases/Fachada.php");

class Usuario{

	public function verificarUsuario($cedula, $usuario, $hidusu){
		$bd = new Fachada();
		$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

		$sql = "SELECT cedula_per FROM persona WHERE cedula_per = '$cedula' AND eliminado=0";
		$resultado = $bd->consultar($sql,'ARREGLO');
		$res=1;
		if ($resultado) {
			$sql1 = "SELECT usuario FROM usuario WHERE usuario = '$usuario' AND id_usu<>$hidusu AND eliminado=0";
			$resultado1 = $bd->consultar($sql1, 'ARREGLO');
			if ($resultado1) {
				return 'El usuario introducido ya existe en el sistema, debe ingresar otro nombre de usuario';
			}
		}else{
			return 'Debe estar registrado como empleado en el sistema para poder ser un usuario del sistema.';
		}
		return $res;
	}

	public function empleadoExis($ced){
		$bd = new Fachada();
		$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

		$sql = "SELECT cedula_per, nombre_per, apellido_per FROM persona WHERE cedula_per = '$ced' AND eliminado = 0";
		$resultado = $bd->consultar($sql, 'ARREGLO');
		return $resultado;
	}

	public function registrarUsuario($cedula,$usuario, $clave, $tipo, $aprobador){
		$bd = new Fachada();
		$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

		$sql = "SELECT E.id_em FROM empleado AS E JOIN persona AS P ON(E.id_per=P.id_per)
					WHERE P.cedula_per = '$cedula' AND P.eliminado=0";
		$resultado = $bd->consultar($sql, 'ARREGLO');
		$id_em=$resultado[0]['id_em'];

		$arreglo = array('usuario' 	=> $usuario,
						'clave' 	=> md5($clave),
						'tipo'	 	=> $tipo,
						'aprob' 	=> $aprobador,
						'id_em' 	=> $id_em
						);
		$resultado = $bd->insertar('usuario',$arreglo);
		return $resultado;

	}

	public function cambiarClave($usu,$passnue,$passvie){
		$bd = new Fachada();
		$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

		$sql = "SELECT * FROM usuario WHERE usuario = '$usu' AND clave = md5('$passvie')";
		//var_dump($sql);
		$resultado = $bd->consultar($sql, 'ARREGLO');

		if ($resultado) {

			$arreglo    = array('clave' => md5($passnue));
			$condicion  = "usuario='$usu'";
			$resultado1 = $bd->modificar('usuario',$arreglo, $condicion);
			return $resultado1;
		}
		return $resultado;
	}

	public function buscarUsuario($usu_aux){
		$bd = new Fachada();
		$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

		$sql = "SELECT * FROM usuario AS u JOIN empleado
				as e ON(u.id_em=e.id_em AND u.usuario='$usu_aux' AND u.eliminado = 0) JOIN persona as p ON(e.id_per=p.id_per) ";
		$resultado = $bd->consultar($sql, 'ARREGLO');
		return $resultado;
	}

	public function listarUsuarios(){
		$bd = new Fachada();
		$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

		$sql = "SELECT * FROM usuario WHERE eliminado = 0 ORDER BY usuario";
		$resultado = $bd->consultar($sql, 'ARREGLO');
		return $resultado;
	}

	public function actualizarDatosUsuario($usuario,$tipo, $aprobador, $hidusu){
		$bd = new Fachada();
		$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

		$arreglo = array('usuario' => $usuario,
						 'tipo'	   => $tipo,
						 'aprob'   => $aprobador
						);
		$condicion = "id_usu=$hidusu";

		$resultado = $bd->modificar('usuario',$arreglo, $condicion);
		if ($resultado) {
			if ($_SESSION['id_usu'] == $hidusu) {
				$_SESSION['usuario'] = $usuario;
			    $_SESSION['tipo']    = $tipo;
			    $_SESSION['nivel']    = $aprobador;
			    return 'u001'; //usuario cambiado es el el que inicio sesion
			}
			return 'u002';//el usuario cambiado no es el que inicio sesion
		}

		return $resultado;
	}
	public function eliminarUsuario($hidusu){
		$bd = new Fachada();
		$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);
		$eli = 1;
		$arreglo = array('eliminado' => $eli);
		$condicion = "id_usu=$hidusu";
		$resultado = $bd->modificar('usuario',$arreglo, $condicion);
		return $resultado;
	}
}
?>